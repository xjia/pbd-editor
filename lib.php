<?php
function authenticate()
{
  if (strlen(trim($_COOKIE['user'])) === 0) {
    header('Location: login.php', true);
    exit;
  }
}

function database_connection()
{
  $db = new PDO('sqlite:db.sqlite3');
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  return $db;
}