(function($) {
$.fn.editorize = function(options) {

var $theEditor = $(this);

var editor = $.extend({
  // defaults:
  content: 'Hello',
  cursor: 0,
  selection: { begin: -1, end: -1 },
  capslock: 'off',
  clipboard: ''
}, options);

function get_clipboard() {
  return editor.clipboard;
}

function set_clipboard(t) {
  editor.clipboard = t;
}

function is_selecting() {
  return editor.selection.begin >= 0 && editor.selection.end >= 0;
}

function cancel_selection() {
  editor.selection.begin = editor.selection.end = -1;
}

function move_left() {
  if (is_selecting()) {
    editor.cursor = Math.min(editor.selection.begin, editor.selection.end);
    cancel_selection();
  }
  else if (editor.cursor > 0) {
    editor.cursor--;
  }
}

function move_right() {
  if (is_selecting()) {
    editor.cursor = Math.max(editor.selection.begin, editor.selection.end);
    cancel_selection();
  }
  else if (editor.cursor < editor.content.length) {
    editor.cursor++;
  }
}

function move_up() {
  if (is_selecting()) {
    editor.cursor = Math.min(editor.selection.begin, editor.selection.end);
    cancel_selection();
  }
  var coord = coordinate_of(editor.cursor);
  if (coord.row > 1) {
    coord.row--;
    editor.cursor = position_of(coord);
  }
}

function move_down() {
  if (is_selecting()) {
    editor.cursor = Math.max(editor.selection.begin, editor.selection.end);
    cancel_selection();
  }
  var coord = coordinate_of(editor.cursor);
  coord.row++;
  editor.cursor = position_of(coord);
}

function validate_selection() {
  if (editor.selection.begin === editor.selection.end) {
    editor.cursor = editor.selection.begin;
    cancel_selection();
  }
}

function select_left() {
  if (is_selecting()) {
    if (editor.selection.end > 0) {
      editor.selection.end--;
    }
  }
  else if (editor.cursor > 0) {
    editor.selection.begin = editor.cursor;
    editor.selection.end = editor.cursor - 1;
  }
  validate_selection();
}

function select_right() {
  if (is_selecting()) {
    if (editor.selection.end < editor.content.length) {
      editor.selection.end++;
    }
  }
  else if (editor.cursor < editor.content.length) {
    editor.selection.begin = editor.cursor;
    editor.selection.end = editor.cursor + 1;
  }
  validate_selection();
}

function select_home() {
  if (!is_selecting()) {
    editor.selection.begin = editor.cursor;
  }
  var coord = coordinate_of(editor.cursor);
  coord.column = 0;
  editor.selection.end = position_of(coord);
}

function select_end() {
  if (!is_selecting()) {
    editor.selection.begin = editor.cursor;
  }
  var coord = coordinate_of(editor.cursor);
  coord.column = editor.content.length;
  editor.selection.end = position_of(coord);
}

function select_up() {
  if (is_selecting()) {
    var coord = coordinate_of(editor.selection.end);
    if (coord.row === 1) {
      editor.selection.end = 0;
    }
    else {
      coord.row--;
      editor.selection.end = position_of(coord);
    }
  }
  else {
    editor.selection.begin = editor.cursor;
    var coord = coordinate_of(editor.cursor);
    if (coord.row === 1) {
      editor.selection.end = 0;
    }
    else {
      coord.row--;
      editor.selection.end = position_of(coord);
    }
  }
  validate_selection();
}

function select_down() {
  if (is_selecting()) {
    var coord = coordinate_of(editor.selection.end);
    coord.row++;
    editor.selection.end = position_of(coord);
  }
  else {
    editor.selection.begin = editor.cursor;
    var coord = coordinate_of(editor.cursor);
    coord.row++;
    editor.selection.end = position_of(coord);
  }
  validate_selection();
}

function remove_selection() {
  var left = Math.min(editor.selection.begin, editor.selection.end);
  var right = Math.max(editor.selection.begin, editor.selection.end);
  editor.content = editor.content.slice(0, left) + editor.content.slice(right);
  cancel_selection();
  editor.cursor = left;
}

function get_selection() {
  var left = Math.min(editor.selection.begin, editor.selection.end);
  var right = Math.max(editor.selection.begin, editor.selection.end);
  return editor.content.slice(left, right);
}

function remove_char_at(x) {
  editor.content = editor.content.slice(0, x) + editor.content.slice(x + 1);
}

function remove_left() {
  if (is_selecting()) {
    remove_selection();
  }
  else if (editor.cursor > 0) {
    remove_char_at(editor.cursor - 1);
    editor.cursor--;
  }
}

function remove_right() {
  if (is_selecting()) {
    remove_selection();
  }
  else if (editor.cursor < editor.content.length) {
    remove_char_at(editor.cursor);
  }
}

function insert_char(c) {
  if (is_selecting()) {
    remove_selection();
  }
  editor.content = editor.content.slice(0, editor.cursor) + c + editor.content.slice(editor.cursor);
  editor.cursor++;
}

function select_all() {
  editor.selection.begin = 0;
  editor.selection.end = editor.content.length;
}

function cut() {
  if (!is_selecting()) return;
  copy();
  remove_selection();
}

function copy() {
  if (!is_selecting()) return;
  set_clipboard(get_selection());
}

function paste() {
  if (is_selecting()) {
    remove_selection();
  }
  var t = get_clipboard();
  editor.content = editor.content.slice(0, editor.cursor) + t + editor.content.slice(editor.cursor);
  editor.cursor += t.length;
}

function is_selected(x, begin, end) {
  if (begin < end) {
    return begin <= x && x < end;
  }
  return end <= x && x < begin;
}

function draw() {
  var i, parts = [];
  for (i = 0; i < editor.content.length; i++) {
    if (editor.content[i] === "\n") {
      if (is_selecting() && is_selected(i, editor.selection.begin, editor.selection.end)) {
        parts.push('<span class="char linebreak selected" data-index="' + i + '">↩</span>');
      }
      else if (i === editor.cursor) {
        parts.push('<span class="char linebreak cursor" data-index="' + i + '">↩</span>');
      }
      else {
        parts.push('<span class="char linebreak" data-index="' + i + '">↩</span>');
      }
      parts.push('<div class="clearfix"></div>');
    }
    else {
      if (is_selecting() && is_selected(i, editor.selection.begin, editor.selection.end)) {
        parts.push('<span class="char selected" data-index="' + i + '">' + editor.content[i] + '</span>');
      }
      else if (i === editor.cursor) {
        parts.push('<span class="char cursor" data-index="' + i + '">' + editor.content[i] + '</span>');
      }
      else {
        parts.push('<span class="char" data-index="' + i + '">' + editor.content[i] + '</span>');
      }
    }
  }
  if (editor.cursor === editor.content.length) {
    parts.push('<span class="char eof cursor" data-index="' + editor.content.length + '"></span>');
  }
  else {
    parts.push('<span class="char eof" data-index="' + editor.content.length + '"></span>');
  }
  parts.push('<div class="clearfix"></div>');
  return parts.join('');
}

function coordinate_of(position) {
  var coord = { row: 1, column: 0 }, i;
  for (i = 0; i < position; i++) {
    if (editor.content[i] === "\n") {
      coord.row++;
      coord.column = 0;
    }
    else {
      coord.column++;
    }
  }
  return coord;
}

function position_of(coordinate) {
  var row = 1, column, row_size = [0, 0], i, pos = 0;
  for (i = 0; i < editor.content.length; i++) {
    if (editor.content[i] === "\n") {
      row++;
      row_size[row] = 0;
    }
    else {
      row_size[row]++;
    }
  }
  row = Math.min(coordinate.row, row);
  column = Math.min(coordinate.column, row_size[row]);
  for (i = 1; i < row; i++) {
    pos += row_size[i] + 1;
  }
  pos += column;
  return pos;
}

function redraw() {
  $theEditor.html(draw());
  var coord = coordinate_of(editor.cursor);
  var pos = position_of(coord);
}

function deep_copy(x) {
  return JSON.parse(JSON.stringify(x));
}

function emit() {
  if (editor.target && editor.submitUrl) {
    editor.events = editor.events || [];
    editor.events.push(deep_copy({
      content: editor.content,
      cursor: editor.cursor,
      selection: editor.selection,
      capslock: editor.capslock,
      clipboard: editor.clipboard
    }));
    if (editor.content === editor.target) {
      $.blockUI();
      $.post(editor.submitUrl, { events: editor.events }, function(){
        window.location.reload();
      }, 'json');
    }
  }
}

function on(f) {
  return function(){
    f();
    redraw();
    return false;
  }
}

var contextMenu = ui.menu();
contextMenu.add('Cut', on(cut));
contextMenu.add('Copy', on(copy));
contextMenu.add('Paste', on(paste));

var _to_ascii = {
  '188': '44',
  '109': '45',
  '190': '46',
  '191': '47',
  '192': '96',
  '220': '92',
  '222': '39',
  '221': '93',
  '219': '91',
  '173': '45',
  '187': '61', // IE key codes
  '186': '59', // IE key codes
  '189': '45'  // IE key codes
};

var shiftUps = {
  '96': '~',
  '49': '!',
  '50': '@',
  '51': '#',
  '52': '$',
  '53': '%',
  '54': '^',
  '55': '&',
  '56': '*',
  '57': '(',
  '48': ')',
  '45': '_',
  '61': '+',
  '91': '{',
  '93': '}',
  '92': '|',
  '59': ':',
  '39': '"',
  '44': '<',
  '46': '>',
  '47': '?'
};

var allowedCharacters = "`1234567890-=~!@#$%^&*()_+qwertyuiop[]\\QWERTYUIOP{}|asdfghjkl;'ASDFGHJKL:\"zxcvbnm,./ZXCVBNM<>? ";

function isMetaKey(e) {
  e = e.originalEvent || e;
  return e.metaKey || e.keyIdentifier === "Meta";
}

function isMac() {
  return navigator.platform.toUpperCase().indexOf('MAC') !== -1;
}

$(document).keydown(function(e){
  var c = e.which;
  // normalize keyCode 
  if (_to_ascii.hasOwnProperty(c)) {
    c = _to_ascii[c];
  }
  if (!e.shiftKey && (c >= 65 && c <= 90)) {
    // FIXME it seems that c is always in uppercase @mac
    c = String.fromCharCode(c + 32);
  }
  else if (e.shiftKey && shiftUps.hasOwnProperty(c)) {
    // get shifted keyCode value
    c = shiftUps[c];
  }
  else {
    c = String.fromCharCode(c);
  }
  
  if (jwerty.is('left', e)) {
    move_left();
    emit();
  }
  else if (jwerty.is('right', e)) {
    move_right();
    emit();
  }
  else if (jwerty.is('up', e)) {
    move_up();
    emit();
  }
  else if (jwerty.is('down', e)) {
    move_down();
    emit();
  }
  else if (jwerty.is('esc/pgup/pgdown', e)) {
    cancel_selection();
    emit();
  }
  else if (jwerty.is('shift+left', e)) {
    select_left();
    emit();
  }
  else if (jwerty.is('shift+right', e)) {
    select_right();
    emit();
  }
  else if (jwerty.is('shift+up', e)) {
    select_up();
    emit();
  }
  else if (jwerty.is('shift+down', e)) {
    select_down();
    emit();
  }
  else if (jwerty.is('backspace', e)) {
    remove_left();
    emit();
  }
  else if (jwerty.is('delete', e)) {
    remove_right();
    emit();
  }
  else if (jwerty.is('ctrl+a/cmd+a', e)) {
    select_all();
    emit();
  }
  else if (jwerty.is('ctrl+x/cmd+x/shift+delete', e)) {
    cut();
    emit();
  }
  else if (jwerty.is('ctrl+c/cmd+c/ctrl+insert', e)) {
    copy();
    emit();
  }
  else if (jwerty.is('ctrl+v/cmd+v/shift+insert', e)) {
    paste();
    emit();
  }
  else if (jwerty.is('home', e)) {
    cancel_selection();
    var coord = coordinate_of(editor.cursor);
    coord.column = 0;
    editor.cursor = position_of(coord);
    emit();
  }
  else if (jwerty.is('end', e)) {
    cancel_selection();
    var coord = coordinate_of(editor.cursor);
    coord.column = editor.content.length;
    editor.cursor = position_of(coord);
    emit();
  }
  else if (jwerty.is('shift+home', e)) {
    select_home();
    emit();
  }
  else if (jwerty.is('shift+end', e)) {
    select_end();
    emit();
  }
  else if (jwerty.is('caps', e)) {
    if (isMac()) {
      editor.capslock = 'on';
    }
    else {
      // flip capslock
      if (editor.capslock === 'on') {
        editor.capslock = 'off';
      }
      else {
        editor.capslock = 'on';
      }
    }
    emit();
  }
  else if (jwerty.is('meta', e) || isMetaKey(e)) {
    // ignore
  }
  else if (jwerty.is('enter', e)) {
    insert_char("\n");
    emit();
  }
  else if (allowedCharacters.indexOf(c) !== -1) {
    if (editor.capslock === 'on') {
      if ('a' <= c && c <= 'z') {
        c = c.toUpperCase();
      }
      else if ('A' <= c && c <= 'Z') {
        c = c.toLowerCase();
      }
    }
    insert_char(c);
    emit();
  }
  redraw();
  return false; // prevent page refresh or history navigation
}).keyup(function(e){
  if (jwerty.is('caps', e)) {
    if (isMac()) {
      editor.capslock = 'off';
      emit();
    }
    else {
      // ignore
    }
  }
}).mousedown(function(e){
  if (e.button === 0) { // left
    var $target = $(e.target);
    if ($target.hasClass('char')) {
      cancel_selection();
      contextMenu.hide();
      var targetIndex = $target.data('index');
      if (e.pageX - $target.offset().left < $target.width() / 2) {
        editor.cursor = targetIndex;
      }
      else if (targetIndex < editor.content.length) {
        editor.cursor = targetIndex + 1;
      }
      editor.dragObject = $target.parent();
      redraw();
      emit();
    }
  }
  else if (e.button === 1) { // middle
    // ignore
  }
  else if (e.button === 2) { // right
    // leave it to contextmenu event
  }
}).contextmenu(function(e){
  var $target = $(e.target);
  if ($target.hasClass('char')) {
    e.preventDefault();
    contextMenu.moveTo(e.pageX, e.pageY).show();
  }
}).mousemove(function(e){
  if (editor.dragObject) {
    var $target = $(e.target);
    if ($target.hasClass('char')) {
      var targetIndex = $target.data('index');
      if (!is_selecting()) {
        editor.selection.begin = editor.cursor;
      }
      if (e.pageX - $target.offset().left < $target.width() / 2) {
        editor.selection.end = targetIndex;
      }
      else if (targetIndex < editor.content.length) {
        editor.selection.end = targetIndex + 1;
      }
      redraw();
      emit();
    }
  }
}).mouseup(function(){
  editor.dragObject = null;
});

redraw();

setInterval(function(){
  $('.cursor').toggleClass('blink');
}, 500);

return this;
};
})(jQuery);
