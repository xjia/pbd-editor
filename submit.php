<?php
require_once 'lib.php';
authenticate();
$dbh = database_connection();
$sth = $dbh->prepare('INSERT INTO traces (example_id,user,trace) VALUES (:id,:user,:trace)');
if (!$sth) {
  echo "\nPDO::errorInfo():\n";
  print_r($dbh->errorInfo());
}
$sth->execute(array(
  ':id' => $_GET['id'],
  ':user' => $_COOKIE['user'],
  ':trace' => json_encode($_POST['events'])
));
?>{ "status": "ok" }