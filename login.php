<?php
$allowed_users = array('Kailang', 'Yuanfei', 'Kangqi', 'Youer', 'Jack', 'Xiao', 'Zheng', 'Wei', 'Tianwan', 'Jacky', 'Adapter');

$error_msg = '';
if (isset($_POST['user'])) {
  if (in_array($_POST['user'], $allowed_users)) {
    setcookie('user', $_POST['user'], time()+60*60*24*30, '/');
    header('Location: index.php', true);
    exit;
  }
  $error_msg = 'User not allowed.';
}
?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Login</title>
</head>
<body>
  <h1>Login</h1>
  <form action="login.php" method="POST">
    <label>
      User:
      <input type="text" name="user" value="">
    </label>
    <p><?php echo $error_msg; ?></p>
    <input type="submit">
  </form>
</body>
</html>