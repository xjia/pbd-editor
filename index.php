<?php
require_once 'lib.php';
authenticate();
$dbh = database_connection();
$sth = $dbh->prepare('SELECT id, input, output, (SELECT count(1) FROM traces WHERE example_id = e.id) AS c '.
                     'FROM examples e '.
                     'WHERE id NOT IN (SELECT example_id FROM traces WHERE user = :user) '.
                     'ORDER BY c, random() LIMIT 1');
$sth->execute(array(':user' => $_COOKIE['user']));
$row = $sth->fetch(PDO::FETCH_ASSOC);
?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>String Transformation</title>
  <link rel="stylesheet" href="ui.css">
  <link rel="stylesheet" href="editor.css">
  <script src="jquery.min.js"></script>
  <script src="lib.js"></script>
  <script src="editor.js"></script>
  <script>
  $(function(){
    $('#theEditor').editorize({
      content: $('#givenContent').text(),
      cursor: 0,
      target: $('#targetContent').text(),
      submitUrl: 'submit.php?id=<?php echo $row['id']; ?>'
    });
  });
  </script>
  <style>
  * {
    -moz-user-select: -moz-none;
    -khtml-user-select: none;
    -webkit-user-select: none;
    -o-user-select: none;
    user-select: none;
    cursor: default;
  }
  a { cursor: pointer; }
  #theEditor * { cursor: text; }
  #targetContent {
    width: 100%;
    min-height: 50px;
    margin: 0;
    padding: 0 0 0 5px;
    border: 1px solid black;
    font-family: monospace;
    font-size: 40px;
    line-height: 50px;
    letter-spacing: 8px;
    word-wrap: break-word;
  }
  #givenContent { display: none; }
  </style>
</head>
<body>
  <h1>String Transformation</h1>
  <p>
    Hi <?php echo $_COOKIE['user']; ?>, thank you very much for doing this test!
    (Not <?php echo $_COOKIE['user']; ?>? <a href="logout.php">Click here to change user</a>)
  </p>
  <?php if ($row === false): ?>
    <p>No more data for you.</p>
  <?php else: ?>
    <h2>Target:</h2>
    <pre id="targetContent"><?php echo htmlspecialchars($row['output']); ?></pre>
    <p>Please use your mouse and keyboard to edit the given text below in order to make it exactly the same as the target shown above.</p>
    <h2>Given:</h2>
    <pre id="givenContent"><?php echo htmlspecialchars($row['input']); ?></pre>
    <div id="theEditor" class="editor"></div>
    <p>Once you finished editing, your actions will be automatically submitted and this page will be refreshed.</p>
  <?php endif; ?>
  <h2>Statistics</h2>
  <table border="1" cellpadding="10" cellspacing="0" style="border-collapse:collapse">
    <tr>
      <th>Who</th>
      <th>How many</th>
    </tr>
    <?php foreach ($dbh->query('SELECT user, count(1) AS c FROM traces GROUP BY user ORDER BY c DESC, user ASC') as $row): ?>
      <tr>
        <td><?php echo $row['user']; ?></td>
        <td><?php echo $row['c']; ?></td>
      </tr>
    <?php endforeach; ?>
  </table>
</body>
</html>