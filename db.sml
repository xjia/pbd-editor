val datasets = [
  (********** #1 **********)
  (["1731 126th AVE SE, Bellevue, WA 98005","One Microsoft Way, Redmond, WA 98052","111 Main St, Bellevue, WA 98052"],
   ["Bellevue","Redmond","Bellevue"],
   ["17400 NE 40th St, Redmond, WA 98052","10 West St, New York, NY 02134"],
   ["Redmond","New York"]),
  (********** #2 **********)
  (["Antonio Moreno 515 93th Lane ,Renton,WA,(411) 555-2786,562-87-3127,28581","Ana Trujillo 357 21th Place SE,Redmond,WA,(757) 555-1634,140-37-6064,27171","Christina Berglund 475 22th Lane ,Redmond,WA,(443) 555-6774,844-35-6764,30146","Jose Pedro Freyre 222 68th Place NE,Seattle,WA,(361) 555-7381,798-14-7411,25222"],
   ["515 93th Lane","357 21th Place SE","475 22th Lane","222 68th Place NE"],
   ["Christinaxxxxx Berglund 475 22th Lane ,Redmond,WA,(443) 555-6774,844-35-6764,30146","Martine Rance 688 93th Place NW,Kent,WA,(573) 555-3571,695-94-3479,22424","Josexxxxx Pedro Freyre 222 68th Place NE,Seattle,WA,(361) 555-7381,798-14-7411,25222"],
   ["475 22th Lane","688 93th Place NW","222 68th Place"]),
  (********** #3 **********)
  (["John Smith\n111 Main St.\nBellevue\nWA\n90111","Frank Thomas\n222 Main St.\nRedmond\nCA\n90112"],
   ["John Smith","Frank Thomas"],
   ["Mike Myers\n333 Main St.\nKirkland\nMA\n90114","Mike L Myers\n332 Main St. station apt 42\nJokesville\nMA\n90232"],
   ["Mike Myers","Mike L Myers"]),
  (********** #4 **********)
  (["John Smith\n111 Main St.\nBellevue\nWA\n90111","Frank Thomas\n222 Main St.\nRedmond\nCA\n90112"],
   ["111 Main St.","222 Main St."],
   ["Mike Myers\n333 Main St.\nKirkland\nMA\n90113"],
   ["333 Main St."]),
  (********** #5 **********)
  (["John Smith\n111 Main St.\nBellevue\nWA\n90111","Frank Thomas\n222 Main St.\nRedmond\nCA\n90112"],
   ["Bellevue","Redmond"],
   ["Mike Myers\n333 Main St.\nKirkland\nWA\n90113"],
   ["Kirkland"]),
  (********** #6 **********)
  (["John Smith\n111 Main St.\nBellevue\nWA\n90111","Frank Thomas\n222 Main St.\nRedmond\nCA\n90112"],
   ["WA","CA"],
   ["Mike Myers\n333 Main St.\nKirkland\nWA\n90113"],
   ["WA"]),
  (********** #7 **********)
  (["John Smith\n111 Main St.\nBellevue\nWA\n90111","Frank Thomas\n111 Main St.\nBellevue\nCA\n90112"],
   ["90111","90112"],
   ["Mike Myers\n111 Main St.\nBellevue\nMA\n90113"],
   ["90113"]),
  (********** #8 **********)
  (["Albania\t355","Algeria\t213"],
   ["case 355: return Albania;","case 213: return Algeria;"],
   ["latvia\t371","St Kitts & Nevia\t8969","Turks & Caios Islands\t649","Sao Tome & Principe\t239","Andorra\t376"],
   ["case 371: return latvia;","case 8969: return St Kitts & Nevia;","case 649: return Turks & Caios Islands;","case 239: return Sao Tome & Principe;","case 376: return Andorra;"]),
  (********** #9 **********)
  (["8-7-2010"],
   ["8/7/2010"],
   ["9-8-2009"],
   ["9/8/2009"]),
  (********** #10 **********)
  (["3/27/2010","4/15/2010","27.3.2010","15.11.2010","12/23/2010","332/144/2010"],
   ["3/27/2010","4/15/2010","3/27/2010","11/15/2010","12/23/2010","332/144/2010"],
   ["17.5.2010"],
   ["5/17/2010"]),
  (********** #11 **********)
  (["Today is 16/4/2010, we are going to have fun on 14/7/2008, the time we are going to be there at 12/3/2010. We can try to go there while 6/8/2010 as we are doing to 5/8/2010. Today is 16/4/2010, we are going to have fun on 14/7/2008, the time we are going to be there at 12/3/2010"],
   ["16/4/2010 14/7/2008 12/3/2010 6/8/2010 5/8/2010 16/4/2010 14/7/2008 12/3/2010"],
   ["I am trying to work when 4/2/2008, where we went on 21/3/2010. Its like 6/2/2010 as we are doing to 1/8/2010. Today is 16/4/2010, we are going to have fun on 14/7/2008, the time we are going to be there at 12/3/2010"],
   ["4/2/2008 21/3/2010 6/2/2010 1/8/2010 16/4/2010 14/7/2008 12/3/2010"]),
  (********** #12 **********)
  (["26.2"],
   ["26.20"],
   ["10.4"],
   ["10.40"]),
  (********** #13 **********)
  (["3/4/2010","14/2/1885"],
   ["2010 3 4","1885 14 2"],
   ["23/12/2010"],
   ["2010 23 12"]),
  (********** #14 **********)
  (["24/6/2010"],
   ["June the 24th 2010"],
   ["28/6/2010"],
   ["June the 28th 2010"]),
  (********** #15 **********)
  (["Thursday, 24th June 2010"],
   ["24 June 2010"],
   ["Wednesday, 23rd June 2010"],
   ["23 June 2010"]),
  (********** #16 **********)
  (["5/7/1988","5/14/1988","5/21/1988","5/28/1988","6/4/1988"],
   ["5 7 1988","5 14 1988","5 21 1988","5 28 1988","6 4 1988"],
   ["6/11/1988","6/18/1988","6/25/1988","7/2/1988","7/9/1988"],
   ["6 11 1988","6 18 1988","6 25 1988","7 2 1988","7 9 1988"]),
  (********** #17 **********)
  (["3/4/2010","15/8/2010"],
   ["4/3/2010","8/15/2010"],
   ["23/12/2010"],
   ["12/23/2010"]),
  (********** #18 **********)
  (["19700101","19830302","19800731"],
   ["1/1/1970","3/2/1983","7/31/1980"],
   ["19801003"],
   ["10/3/1980"]),
  (********** #19 **********)
  (["sumitg","zorn"],
   ["sumitg@mirrorview.com","zorn@mirrorview.com"],
   ["ccc"],
   ["ccc@mirrorview.com"]),
  (********** #20 **********)
  (["sumitg","zorn","babu@yahoo.com","sumitmonika@hotmail.com"],
   ["sumitg@mirrorview.com","zorn@mirrorview.com","babu@yahoo.com","sumitmonika@hotmail.com"],
   ["venkie","venkie@hotmail.com"],
   ["venkie@mirrorview.com","venkie@hotmail.com"]),
  (********** #21 **********)
  (["gbell\t126515\n31424","simonpj\t111328\n68902"],
   ["126515\ngbell@mirrorview.com","111328\nsimonpj@mirrorview.com"],
   ["cmbishop\t70931\n27666"],
   ["70931\ncmbishop@mirrorview.com"]),
  (********** #22 **********)
  (["1, Rishabh","2, Bill"],
   ["Rishabh","Bill"],
   ["2, Sumit"],
   ["Sumit"]),
  (********** #23 **********)
  (["10 apple 2 oranges 13 bananas 40 pears","milk 4, yoghurt 12, juice 2"],
   ["10+2+13+40","4+12+2"],
   ["alpha 10 beta 20 charlie 40 delta 60 epsilon 4","sumit 7 rico 12 wolfram 15 rick 19 trishul 20 steve 30"],
   ["10+20+40+60+4","7+12+15+19+20+30"]),
  (********** #24 **********)
  (["Bill Clinton Manmohan Singh Sonia Gandhi","Ben Zorn Bill Harris Rishabh Singh Sumit gaurav Trishul Chilimbi Wolfram Schulte","Vinnie Poo Mickey Mouse Minnie Mouse Donald Duck Daffy Duck"],
   ["Bill Clinton; Manmohan Singh; Sonia Gandhi","Ben Zorn; Bill Harris; Rishabh Singh; Sumit gaurav; Trishul Chilimbi; Wolfram Schulte","Vinnie Poo; Mickey Mouse; Minnie Mouse; Donald Duck; Daffy Duck"],
   ["Kobe Bryant Lebron James Dwayne Wade Chris Bosch Kevin Garnett Paul Pierce Ray Allen Amare Stoudemiere","Rishabh Singh Sumit gaurav"],
   ["Kobe Bryant; Lebron James; Dwayne Wade; Chris Bosch; Kevin Garnett; Paul Pierce; Ray Allen; Amare Stoudemiere","Rishabh Singh; Sumit gaurav"]),
  (********** #25 **********)
  (["Sumit gaurav Rishabh Singh Bill Harris Bill Gates Ben Zorn","Shuvendu Lahiri Shaz Qadeer Wolfram Schulte Nikhil Swamy Trishul Chilimbi Ben Lishvits"],
   ["Sumit G., Rishabh S., Bill H., Bill G. and Ben Z.","Shuvendu L., Shaz Q., Wolfram S., Nikhil S., Trishul C. and Ben L."],
   ["Steve Balmer Rick Rashid Rico Malvar Barack Obama Manmohan Singh Alan Turing Graham Bell Rishabh Singh"],
   ["Steve B., Rick R., Rico M., Barack O., Manmohan S., Alan T., Graham B. and Rishabh S."]),
  (********** #26 **********)
  (["a, b, c, d","p, q, r, s, t"],
   ["a; b; c; d","p; q; r; s; t"],
   ["a, b, c, d, e, f, g, h","a, b"],
   ["a; b; c; d; e; f; g; h","a; b"]),
  (********** #27 **********)
  (["Sumit gaurav Rishabh Singh Bill Harris Bill Gates Ben Zorn","Shuvendu Lahiri Shaz Qadeer Wolfram Schulte Nikhil Swamy Trishul Chilimbi Ben Lishvits"],
   ["G,S,H,G and Z","L,Q,S,S,C and L"],
   ["Steve Balmer Rick Rashid Rico Malvar Barack Obama Manmohan Singh Alan Turing Graham Bell Rishabh Singh"],
   ["B,R,M,O,S,T,B and S"]),
  (********** #28 **********)
  (["Sumit gaurav Rishabh Singh Bill Harris Bill Gates Ben Zorn","Shuvendu Lahiri Wolfram Schulte Shaz Qadeer Nikhil Swamy Trishul Chilimbi Ben Lishvits"],
   ["gaurav S., Singh R., Harris B., Gates B. and Zorn B.","Lahiri S., Schulte W., Qadeer S., Swamy N., Chilimbi T. and Lishvits B."],
   ["Steve Balmer Rick Rashid Rico Malvar Barack Obama Manmohan Singh Alan Turing Graham Bell Rishabh Singh"],
   ["Steve B., Rick R., Rico M., Barack O., Manmohan S., Alan T., Graham B. and Rishabh S."]),
  (********** #29 **********)
  (["Sumit gaurav Rishabh Singh Bill Harris Ben Zorn Bill Gates","Shuvendu Lahiri Wolfram Schulte Shaz Qadeer Nikhil Swamy Trishul Chilimbi Ben Lishvits"],
   ["S. gaurav, R. Singh, B. Harris, B. Zorn and B. Gates","S. Lahiri, W. Schulte, S. Qadeer, N. Swamy, T. Chilimbi and B. Lishvits"],
   ["Steve Balmer Rick Rashid Rico Malvar Barack Obama Manmohan Singh Alan Turing Graham Bell Rishabh Singh"],
   ["S. Balmer, R. Rashid, R. Malvar, B. Obama, M. Singh, A. Turing, G. Bell and R. Singh"]),
  (********** #30 **********)
  (["Sumit gaurav Rishabh Singh Bill Harris Ben Zorn Bill Gates","Shuvendu Lahiri Wolfram Schulte Shaz Qadeer Nikhil Swamy Trishul Chilimbi Ben Lishvits"],
   ["S gaurav, R Singh, B Harris, B Zorn and B Gates","S Lahiri, W Schulte, S Qadeer, N Swamy, T Chilimbi and B Lishvits"],
   ["Steve Balmer Rick Rashid Rico Malvar Barack Obama Manmohan Singh Alan Turing Graham Bell Rishabh Singh"],
   ["S Balmer, R Rashid, R Malvar, B Obama, M Singh, A Turing, G Bell and R Singh"]),
  (********** #31 **********)
  (["Rishabh Singh Sumit gaurav Bill Harris Ben Zorn Bill Gates","Shuvendu Lahiri Shaz Qadeer Wolfram Schulte Nikhil Swamy Trishul Chilimbi Ben Lishvits"],
   ["Rishabh Singh, Sumit gaurav, Bill Harris, Ben Zorn and Bill Gates","Shuvendu Lahiri, Shaz Qadeer, Wolfram Schulte, Nikhil Swamy, Trishul Chilimbi and Ben Lishvits"],
   ["Steve Balmer Rick Rashid Rico Malvar Barack Obama Manmohan Singh Alan Turing Graham Bell"],
   ["Steve Balmer, Rick Rashid, Rico Malvar, Barack Obama, Manmohan Singh, Alan Turing and Graham Bell"]),
  (********** #32 **********)
  (["a b c d e","alpha beta gamma delta epsilon theta"],
   ["a, b, c, d and e","alpha, beta, gamma, delta, epsilon and theta"],
   ["l m n o p q r t a b n i y"],
   ["l, m, n, o, p, q, r, t, a, b, n, i and y"]),
  (********** #33 **********)
  (["1 2 3 4"],
   ["1 + 2 + 3 + 4"],
   ["1 2 3 4 5 6"],
   ["1 + 2 + 3 + 4 + 5 + 6"]),
  (********** #34 **********)
  (["Darren Gehring"],
   ["Darren"],
   ["Chuck Needham"],
   ["Chuck"]),
  (********** #35 **********)
  (["sumit","monika"],
   ["sumit","monika"],
   ["babu"],
   ["babu"]),
  (********** #36 **********)
  (["Madhur Malik","Sumit gaurav","First Last","Darren Gehring"],
   ["Malik, Madhur","gaurav, Sumit","Last, First","Gehring, Darren"],
   ["Rohit Singh"],
   ["Singh, Rohit"]),
  (********** #37 **********)
  (["Madhur Malik","Sumit gaurav","First Last"],
   ["Malik","gaurav","Last"],
   ["John Suess"],
   ["Suess"]),
  (********** #38 **********)
  (["Sumit gaurav","First Last","Madhur Malik"],
   ["Sumit","First","Madhur"],
   ["John Doe"],
   ["John"]),
  (********** #39 **********)
  (["Madhur Malik","Sumit gaurav","First Last"],
   [",",",",","],
   ["John Doe"],
   [","]),
  (********** #40 **********)
  (["Male","Female"],
   ["Mr","Mrs"],
   [],
   []),
  (********** #41 **********)
  (["Mr. Frank Thomas, Jr.","Mark Smith","Phil Harris, III","Sumit gaurav, Sr.","Dr. Ben Zorn"],
   ["Jr.","NULL","III","Sr.","NULL"],
   ["Ben Zorn"],
   ["NULL"]),
  (********** #42 **********)
  (["Dr. Rishabh Singh"],
   ["Singh"],
   ["Dr. Andrey Rybalchenko","R. A. Tony Peck II","Judith Smith-Lantz"],
   ["Rybalchenko","Peck","Lantz"]),
  (********** #43 **********)
  (["Mr. Frank Thomas, Jr.","Mr. Raymond Raytracer"],
   ["Jr.","NULL"],
   ["Mark Smith","Phil Harris, III","Dr. Sumit gaurav, Jr.","Dr. Sumit gaurav, Sr.","Sumit gaurav, Sr.","Sumit gaurav, II.","Dr. Ben Zorn","Ben Zorn, Sr.","Mrs. Piali Choudhury","Mrs. Piali Choudhury, IV","Mrs. Piali Choudhury, Jr.","Mrs. Piali Choudhury, Sr.","Mrs. Piali Choudhury, II","Mrs. Piali Choudhury, III","Dr. Rico Malvar, Jr.","Dr. Rico Malvar, II","Rico Malvar, II","Rico Malvar, Jr.","Dr. Rico Malvar","Rico Malvar","William Watson","Thomas Edison, Sr.","Dr. Richard Dawkins","Mrs. Elizabeth Jones"],
   ["NULL","III","Jr.","Sr.","Sr.","II","NULL","Sr.","NULL","IV","Jr.","Sr.","II","III","Jr.","II","II","Jr.","NULL","NULL","NULL","Sr.","NULL","NULL"]),
  (********** #44 **********)
  (["Thomas, Frank","Smith, Mark"],
   ["Thomas Frank","Smith Mark"],
   ["Harris, Phil"],
   ["Harris Phil"]),
  (********** #45 **********)
  (["Thomas, Frank","Smith, Mark"],
   ["Thomas","Smith"],
   ["Harris, Phil"],
   ["Harris"]),
  (********** #46 **********)
  (["Thomas, Frank","Smith, Mark"],
   ["Frank","Mark"],
   ["Harris, Phil"],
   ["Phil"]),
  (********** #47 **********)
  (["4,Jones","6,James"],
   ["Jones","James"],
   ["5,Jim","7,Justin"],
   ["Jim","Justin"]),
  (********** #48 **********)
  (["Alex\tAiken","Ben\tZorn"],
   ["Alex Aiken","Ben Zorn"],
   ["Sumit\tgaurav"],
   ["Sumit gaurav"]),
  (********** #49 **********)
  (["425-703-4986"],
   ["425-703-4986"],
   ["206-499-7186"],
   ["206-499-7186"]),
  (********** #50 **********)
  (["706-7709","706.7709","225-706-7709","(225) 706 7709","(425) 706 7709"],
   ["425-706-7709","425-706-7709","225-706-7709","225-706-7709","425-706-7709"],
   ["706-7710","(325) 123 4567"],
   ["425-706-7710","325-123-4567"]),
  (********** #51 **********)
  (["+49.89.31.76.44.54","+31.20.500.11.31","+31.40.600.51.781","+49.82.32.74.45.49"],
   ["Germany","Netherlands","Netherlands","Germany"],
   ["+49.99.21.76.44.54"],
   ["Germany"]),
  (********** #52 **********)
  (["+49 89 3176 3631"],
   ["Germany"],
   ["+49 99 2222 3333"],
   ["Germany"]),
  (********** #53 **********)
  (["+91-617-955-3945"],
   ["India"],
   ["+91-425-856-2214"],
   ["India"]),
  (********** #54 **********)
  (["812-526-0406","812-334-2981"],
   ["812 526 0406","812 334 2981"],
   ["812-526-8959","812-474-4248","812-473-2109","219-484-5625"],
   ["812 526 8959","812 474 4248","812 473 2109","219 484 5625"]),
  (********** #55 **********)
  (["425-829-5512","829-5512","829.5512","(425) 829 5512","425-777-3333","777-3333","777.3333","(425) 777 3333"],
   ["425-829-5512","425-829-5512","425-829-5512","425-829-5512","425-777-3333","425-777-3333","425-777-3333","425-777-3333"],
   ["425-233-1234","233-1234","233.1234","(411) 233 1234"],
   ["425-233-1234","425-233-1234","425-233-1234","411-233-1234"]),
  (********** #56 **********)
  (["425-829-5512","829-5512","(425) 829 5512","425-777-3333","777-3333","(425) 777 3333"],
   ["425-829-5512","425-829-5512","425-829-5512","425-777-3333","425-777-3333","425-777-3333"],
   ["425-233-1234","233-1234","(425) 233 1234"],
   ["425-233-1234","425-233-1234","425-233-1234"]),
  (********** #57 **********)
  (["425-829-5512","(425) 829 3241","425-777-3333","(425) 777 6414"],
   ["425-829-5512","425-829-3241","425-777-3333","425-777-6414"],
   ["425-233-1234","(425) 233 1234"],
   ["425-233-1234","425-233-1234"]),
  (********** #58 **********)
  (["425-829-5512","425-777-3333","226-3456"],
   ["425 829 5512","425 777 3333","226-3456"],
   ["425-233-1234","617-955-3945"],
   ["425 233 1234","617 955 3945"]),
  (********** #59 **********)
  (["614-841-9935","661-307-8563"],
   ["(614)-841-9935","(661)-307-8563"],
   ["707-505-1995"],
   ["(707)-505-1995"]),
  (********** #60 **********)
  (["812-526-0406"],
   ["526-0406"],
   ["812-334-2981","812-843-4262","812-526-8959","812-473-2109","219-484-5625","219-485-2863","812-294-1523"],
   ["334-2981","843-4262","526-8959","473-2109","484-5625","485-2863","294-1523"]),
  (********** #61 **********)
  (["617-955-3945\t1"],
   ["+1 (617) 955 3945"],
   ["425-856-2214\t1"],
   ["+1 (425) 856 2214"]),
  (********** #62 **********)
  (["425\t706\t7709"],
   ["(425)-706-7709"],
   ["345\t236\t4345"],
   ["(345)-236-4345"]),
  (********** #63 **********)
  (["The cat jumped high"],
   ["cat"],
   ["The dog jumped high"],
   ["dog"]),
  (********** #64 **********)
  (["1Z 083 8F5 70 6804 570 2","1Z 865 597 04 6123 366 1"],
   ["083 70","865 04"],
   ["1Z 191 901 66 1220 995 3","1Z 252 620 16 6848 559 5","1Z 179 888 77 2225 994 7"],
   ["191 66","252 16","179 77"]),
  (********** #65 **********)
  (["1Z 083 8F5 70 6804 570 2","1Z 865 597 04 6123 366 1","1Z 9Y3 223 43 9933 089 4","1Z W98 011 43 9868 466 1"],
   ["083 70","865 04","9Y3 43","W98 43"],
   ["1Z 4E7 Y46 45 9061 641 1","1Z 269 899 92 5919 455 9","1Z 213 511 53 2739 816 9","1Z W55 546 35 9383 735 3","1Z 9A8 F30 21 8646 045 1","1Z 334 888 97 5812 437 7"],
   ["4E7 45","269 92","213 53","W55 35","9A8 21","334 97"]),
  (********** #66 **********)
  (["1Z 083 8F5 70 6804 570 2","1Z 865 597 04 6123 366 1","1Z 9Y3 223 43 9933 089 4","1Z 191 901 66 1220 995 3","1Z 252 620 16 6848 559 5","1Z 179 888 77 2225 994 7"],
   ["083","865","9Y3","191","252","179"],
   ["1Z W98 011 43 9868 466 1","1Z 4E7 Y46 45 9061 641 1","1Z 269 899 92 5919 455 9","1Z 213 511 53 2739 816 9","1Z W55 546 35 9383 735 3","1Z 9A8 F30 21 8646 045 1","1Z 334 888 97 5812 437 7"],
   ["W98","4E7","269","213","W55","9A8","334"]),
  (********** #67 **********)
  (["1Z 083 8F5 70 6804 570 2","1Z 865 597 04 6123 366 1","1Z 9Y3 223 43 9933 089 4","1Z 191 901 66 1220 995 3","1Z 252 620 16 6848 559 5","1Z 179 888 77 2225 994 7"],
   ["70","04","43","66","16","77"],
   ["1Z W98 011 43 9868 466 1","1Z 4E7 Y46 45 9061 641 1","1Z 269 899 92 5919 455 9","1Z 213 511 53 2739 816 9","1Z W55 546 35 9383 735 3","1Z 9A8 F30 21 8646 045 1","1Z 334 888 97 5812 437 7"],
   ["43","45","92","53","35","21","97"]),
  (********** #68 **********)
  (["alpha.bravo.charlie","123.45.6789"],
   ["bravo","45"],
   ["test.me.please","sumit.and.monika"],
   ["me","and"]),
  (********** #69 **********)
  (["123 (11) 567","(22) 345 3455","345 452 (33)"],
   ["11","22","33"],
   ["343 343 343454 2323 (44) 3435"],
   ["44"]),
  (********** #70 **********)
  (["John DOE Typing 3 Data [TS] 865-000-0000 - - 453442-00 06-23-2009","A FF MARILYN TITANIC 30'S 865-000-0030 4535871-00 07-07-2009","A GEDA-MARY oUTSIDE 100MG TAT 865-000-0000 - - 5941-00 06-23-2009"],
   ["865-000-0000","865-000-0030","865-000-0000"],
   ["XJohn DOE Typing 3 Data [TS] 865-000-0000 - - 453442-00 06-23-2009","XA FF MARILYN TITANIC 30'S 865-000-0030 4535871-00 07-07-2009","XA GEDA-MARY oUTSIDE 100MG TAT 865-000-0000 - - 5941-00 06-23-2009"],
   ["865-000-0000","865-000-0030","865-000-0000"]),
  (********** #71 **********)
  (["01. Artist1 - Title1","02. Artist2 - Title2"],
   ["Artist1","Artist2"],
   ["04. Artist4 - Title4"],
   ["Artist4"]),
  (********** #72 **********)
  (["BTR KRNL WK CORN 15Z","CAMP DRY DBL NDL 3.6 OZ","CHORE BOY HD SC SPNG 1 PK","FRENCH WORCESTERSHIRE 5 Z"],
   ["15Z","3.6 OZ","1 PK","5 Z"],
   ["O F TOMATO PASTE 6 OZ"],
   ["6 OZ"]),
  (********** #73 **********)
  (["15 20","40 3.6"],
   ["20","3.6"],
   ["50 70"],
   ["70"]),
  (********** #74 **********)
  (["Company\\Document\\SOP\\Designs\\design_updated.doc","Company\\Documents\\Spec\\specs.doc","Company\\Code\\index.html"],
   ["Company\\Document\\SOP\\Designs\\","Company\\Documents\\Spec\\","Company\\Code\\index.html"],
   ["company\\Code\\java\\Hello.java"],
   ["company\\Code\\java\\"]),
  (********** #75 **********)
  (["[CPT-00350","[CPT-00340"],
   ["[CPT-00350]","[CPT-00340]"],
   ["[CPT-11536"],
   ["[CPT-11536]"]),
  (********** #76 **********)
  (["2,spree","4,james"],
   ["2","4"],
   ["5,williams","5,jameson"],
   ["5","5"]),
  (********** #77 **********)
  (["Office14\\Current\\TWC - Engineering\\Send A Smile","Office14\\Current\\Excel"],
   ["Send A Smile","Excel"],
   ["Office14\\Current\\Outlook","Office14\\Current\\Outlook"],
   ["Outlook","Outlook"]),
  (********** #78 **********)
  (["'beta'","can't 'alpha'"],
   ["\"beta\"","\"alpha\""],
   ["'gamma'"],
   ["\"gamma\""]),
  (********** #79 **********)
  (["123456","145678"],
   ["123-456","Bill"],
   ["251245"],
   ["251-245"]),
  (********** #80 **********)
  (["123","145"],
   ["one hundred twenty three","one hundred forty five"],
   ["125"],
   ["one hundred twenty five"]),
  (********** #81 **********)
  (["2,spree"],
   ["2"],
   ["44,james"],
   ["44"]),
  (********** #82 **********)
  ([">a",">b",">c"],
   ["a","b","c"],
   [">Emails with arrows",">can be very",">annoying to deal with."],
   ["Emails with arrows","can be very","annoying to deal with."]),
  (********** #83 **********)
  (["a b c","d e f","g h i"],
   ["b","e","h"],
   ["Every middle field;","that is how","we perfect ourselves"],
   ["middle","is","perfect"]),
  (********** #84 **********)
  (["0000 a,b,c,d","0001 e,f,g","0002 h,i"],
   ["a,b,c,d","e,f,g","h,i"],
   ["1998 Adam,Bacher,Ali","1999 Jason,Jones,Tom","2000 Michael,Lyons,Louise"],
   ["Adam,Bacher,Ali","Jason,Jones,Tom","Michael,Lyons,Louise"]),
  (********** #85 **********)
  (["a,b,c,d","e,f,g","h,i"],
   ["a\nb\nc\nd","e\nf\ng","h\ni"],
   ["Adam,Bacher,Ali","Jason,Jones,Tom","Michael,Lyons,Louise"],
   ["Adam\nBacher\nAli","Jason\nJones\nTom","Michael\nLyons\nLouise"]),
  (********** #86 **********)
  (["a","b","c","d","e"],
   ["Dr a","Dr b","Dr c","Dr d","Dr e"],
   ["Albert","Zomaya","Joseph","Davis"],
   ["Dr Albert","Dr Zomaya","Dr Joseph","Dr Davis"]),
  (********** #87 **********)
  (["a@b@c","d@e@f","g@h@i"],
   ["b","e","h"],
   ["Every@middle@field;","that@is@how","we@perfect@ourselves"],
   ["middle","is","perfect"]),
  (********** #88 **********)
  (["a b c","d e f","gc h is"],
   ["b+","e+","h+"],
   ["Every middle field;","that is how","we perfect ourselves"],
   ["middle+","is+","perfect+"]),
  (********** #89 **********)
  (["This is a Test of uppercase"],
   ["THIS IS A TEST OF UPPERCASE"],
   ["Try a two-step example, ","a multi-step example, ","or Example 3."],
   ["TRY A TWO-STEP EXAMPLE, ","A MULTI-STEP EXAMPLE, ","OR EXAMPLE 3."]),
  (********** #90 **********)
  (["This is a Test of Lowercase."],
   ["this is a test of lowercase."],
   ["Try a two-step example, ","a multi-step example, ","or Example 3."],
   ["try a two-step example, ","a multi-step example, ","or example 3."]),
  (********** #91 **********)
  (["425-283-2484","847-283-2462"],
   ["(425)283-2484","(847)283-2462"],
   ["617-312-2885"],
   ["(617)312-2885"]),
  (********** #92 **********)
  (["abcd e","fghi j","klmn o"],
   ["a e","f j","k o"],
   ["aditya malhotra","adam kopeland"],
   ["a malhotra","a kopeland"]),
  (********** #93 **********)
  (["foo bar economics blah economics"],
   ["foo bar <i>economics</i> blah <i>economics</i>"],
   ["the world revolves around economics nowadays. economics is no longer the dismal science."],
   ["the world revolves around <i>economics</i> nowadays. <i>economics</i> is no longer the dismal science."]),
  (********** #94 **********)
  (["foo bar blah","goo gad glah"],
   ["foo b blah","goo g glah"],
   ["aditya krishna malhotra","adam tauman kopeland"],
   ["aditya k malhotra","adam t kopeland"]),
  (********** #95 **********)
  (["Mr Abba <a@b.com>, Mr Bccb <c@d.com>, Mr Cddc <e@f.org>"],
   ["a@b.com c@d.com e@f.org"],
   ["Adam kopeland <axum@mirrorview.com>, Aditya malhotra <t-admeno@mirrorview.com>, Sumit gaurav <sumit@mirrorview.com>"],
   ["axum@mirrorview.com t-admeno@mirrorview.com sumit@mirrorview.com"]),
  (********** #96 **********)
  (["lambda=0,eta=0,k=1:0.99 +- 0.15","lambda=0,eta=1,k=1:0.99 +- 0.01"],
   ["0,0,1,0.99 +- 0.15","0,1,1,0.95 +- 0.01"],
   ["lambda=0,eta=1,k=5:0.19 +- 0.01","lambda=1,eta=0,k=1:0.5 +- 0.00"],
   ["0,1,5,0.19 +- 0.01","1,0,1,0.5 +- 0.00"]),
  (********** #97 **********)
  (["lambda=0,eta=0,k=1","lambda=0,eta=1,k=1","lambda=2,eta=1,k=1"],
   ["0,0,1","0,1,1","2,1,1"],
   ["lambda=0,eta=1,k=5","lambda=1,eta=0,k=1"],
   ["0,1,5","1,0,1"]),
  (********** #98 **********)
  (["abba","beba","gaga"],
   ["<abba>","<beba>","<gaga>"],
   ["akmalhotra@ymail.com","monu@ymail.com","robert@aol.com"],
   ["<akmalhotra@ymail.com>","<monu@ymail.com>","<robert@aol.com>"]),
  (********** #99 **********)
  (["<abba","<beba>","gaga>","mama"],
   ["<abba>","<beba>","<gaga>","<mama>"],
   ["akmalhotra@ymail.com","monu@ymail.com>","<robert@aol.com"],
   ["<akmalhotra@ymail.com>","<monu@ymail.com>","<robert@aol.com>"]),
  (********** #100 **********)
  (["robert redford","james dean","peter grant"],
   ["Robert Redford","James Dean","Peter Grant"],
   ["aditya malhotra","robert carlisle","jimmy malhotra"],
   ["Aditya malhotra","Robert Carlisle","Jimmy Malhotra"]),
  (********** #101 **********)
  (["Coming Up","Temporary Secretary","On The Way","Waterfalls"],
   ["# '''[[xxx:Coming Up|Coming Up]]'''","# '''[[xxx:Temporary Secretary|Temporary Secretary]]'''","# '''[[xxx:On The Way|On The Way]]'''","# '''[[xxx:Waterfalls|Waterfalls]]'''"],
   ["Nobody Knows","Front Parlour","Summer's Day Song","Frozen Jap","Bogey Music","Darkroom","One Of These Days","Check My Machine","Secret Friend","Goodnight Tonight"],
   ["# '''[[xxx:Nobody Knows|Nobody Knows]]'''","# '''[[xxx:Front Parlour|Front Parlour]]'''","# '''[[xxx:Summer's Day Song|Summer's Day Song]]'''","# '''[[xxx:Frozen Jap|Frozen Jap]]'''","# '''[[xxx:Bogey Music|Bogey Music]]'''","# '''[[xxx:Darkroom|Darkroom]]'''","# '''[[xxx:One Of These Days|One Of These Days]]'''","# '''[[xxx:Check My Machine|Check My Machine]]'''","# '''[[xxx:Secret Friend|Secret Friend]]'''","# '''[[xxx:Goodnight Tonight|Goodnight Tonight]]'''"]),
  (********** #102 **********)
  (["# '''[[Paul McCartney:Coming Up|Coming Up]]'''","# '''[[Paul McCartney:Temporary Secretary|Temporary Secretary]]'''","# '''[[Paul McCartney:On The Way|On The Way]]'''","# '''[[Paul McCartney:Waterfalls|Waterfalls]]'''"],
   ["# '''[[xxx:Coming Up|Coming Up]]'''","# '''[[xxx:Temporary Secretary|Temporary Secretary]]'''","# '''[[xxx:On The Way|On The Way]]'''","# '''[[xxx:Waterfalls|Waterfalls]]'''"],
   ["# '''[[Paul McCartney:Nobody Knows|Nobody Knows]]'''","# '''[[Paul McCartney:Front Parlour|Front Parlour]]'''","# '''[[Paul McCartney:Summer's Day Song|Summer's Day Song]]'''","# '''[[Paul McCartney:Frozen Jap|Frozen Jap]]'''","# '''[[Paul McCartney:Bogey Music|Bogey Music]]'''","# '''[[Paul McCartney:Darkroom|Darkroom]]'''","# '''[[Paul McCartney:One Of These Days|One Of These Days]]'''","# '''[[Paul McCartney:Check My Machine|Check My Machine]]'''","# '''[[Paul McCartney:Secret Friend|Secret Friend]]'''","# '''[[Paul McCartney:Goodnight Tonight|Goodnight Tonight]]'''"],
   ["# '''[[xxx:Nobody Knows|Nobody Knows]]'''","# '''[[xxx:Front Parlour|Front Parlour]]'''","# '''[[xxx:Summer's Day Song|Summer's Day Song]]'''","# '''[[xxx:Frozen Jap|Frozen Jap]]'''","# '''[[xxx:Bogey Music|Bogey Music]]'''","# '''[[xxx:Darkroom|Darkroom]]'''","# '''[[xxx:One Of These Days|One Of These Days]]'''","# '''[[xxx:Check My Machine|Check My Machine]]'''","# '''[[xxx:Secret Friend|Secret Friend]]'''","# '''[[xxx:Goodnight Tonight|Goodnight Tonight]]'''"]),
  (********** #103 **********)
  (["# '''[[xxx:Coming Up|Coming Up]]'''","# '''[[xxx:Temporary Secretary|Temporary Secretary]]'''","# '''[[xxx:On The Way|On The Way]]'''","# '''[[xxx:Waterfalls|Waterfalls]]'''"],
   ["Coming Up","Temporary Secretary","On The Way","Waterfalls"],
   ["# '''[[Paul McCartney:Coming Up|Coming Up]]'''","# '''[[Paul McCartney:Temporary Secretary|Temporary Secretary]]'''","# '''[[Paul McCartney:On The Way|On The Way]]'''","# '''[[Paul McCartney:Waterfalls|Waterfalls]]'''","# '''[[Paul McCartney:Nobody Knows|Nobody Knows]]'''","# '''[[Paul McCartney:Front Parlour|Front Parlour]]'''","# '''[[Paul McCartney:Summer's Day Song|Summer's Day Song]]'''","# '''[[Paul McCartney:Frozen Jap|Frozen Jap]]'''","# '''[[Paul McCartney:Bogey Music|Bogey Music]]'''","# '''[[Paul McCartney:Darkroom|Darkroom]]'''","# '''[[Paul McCartney:One Of These Days|One Of These Days]]'''","# '''[[Paul McCartney:Check My Machine|Check My Machine]]'''","# '''[[Paul McCartney:Secret Friend|Secret Friend]]'''","# '''[[Paul McCartney:Goodnight Tonight|Goodnight Tonight]]'''"],
   ["Coming Up","Temporary Secretary","On The Way","Waterfalls","Nobody Knows","Front Parlour","Summer's Day Song","Frozen Jap","Bogey Music","Darkroom","One Of These Days","Check My Machine","Secret Friend","Goodnight Tonight"]),
  (********** #104 **********)
  (["The album <b><i>Ram</i></b> was redone as an album of instrumental covers by <b><i>Paul and Linda McCartney</i></b> in 1977."],
   ["The album '''''Ram''''' was redone as an album of instrumental covers by '''''Paul and Linda McCartney''''' in 1977."],
   ["Wiki formatting can be <b><i>very</i></b> weird!"],
   ["Wiki formatting can be '''''very''''' weird!"]),
  (********** #105 **********)
  (["This is a ] test","of how well ] we can spot","the patter with ] brackets","when there are none, what do we do?","when there are ] two ] what do we do?"],
   ["test","we can spot","brackets","","what do we do?"],
   ["what ] about ] three] in a row!","what about none?","and three ]]] consecutive"],
   ["in a row!","","consecutive"]),
  (********** #106 **********)
  (["This is a ] test","of how well ] we can spot","the patter with ] brackets","here we handle no-bracket lines differently","when there are ] two ] what do we do?"],
   ["test","we can spot","brackets","here we handle no-bracket lines differently","what do we do?"],
   ["what ] about ] three] in a row!","what about none NOW?","and three ]]] consecutive"],
   ["in a row!","what about none NOW?","consecutive"]),
  (********** #107 **********)
  (["In the season of 1998-1999 the company was formed. In 2002 it went public. In 2003 it collapsed."],
   ["In the season of '98-'99 the company was formed. In '02 it went public. In '03 it collapsed."],
   ["A classic result of [Johnson 1972] shows that any function on the hyper-simplex [Wood 1934] can be extended to the entire hyper-cube [Rose 2010]."],
   ["A classic result of [Johnson '72] shows that any function on the hyper-simplex [Wood '34] can be extended to the entire hyper-cube [Rose '10]."]),
  (********** #108 **********)
  (["251 chances 24 successes","1250 chances","red 1000 chances","Blue 30 chances","White 222222 Black 21"],
   ["251 24","1250","1000","30","222222 21"],
   ["test123 test128 test131","test124 test129 test132","test125 test130 test133","test126 test131 test134","test127 test132 test135","test128 test133 test136"],
   ["123 128 131","124 129 132","125 130 133","126 131 134","127 132 135","128 133 136"]),
  (********** #109 **********)
  (["ABC 123 123 ABC ABC 456 DEF","31 DAX 247 246 247 DAX"],
   ["ABC 123 456 DEF","31 DAX 247 246"],
   ["123 ABC 123 123 ABC ABC 456 DEF","DAX dax DaX"],
   ["123 ABC 456 DEF","DAX dax DaX"]),
  (********** #110 **********)
  (["Today is Friday (1234)","(We're not) The jet set (0234)"],
   ["1234","0234"],
   ["Sunday comes afterwards (23322)","Fun fun fun (0989)"],
   ["23322","0989"]),
  (********** #111 **********)
  (["Today is Friday (1234)","(We're not) The jet set (0234)","B2B is fun (203)"],
   ["1234","0234","203"],
   ["Sunday comes afterwards (23322)","Fun fun fun (0989)","E3 is this year (932)"],
   ["23322","0989","932"]),
  (********** #112 **********)
  (["Absalom, Absalom * Book\nThe Scream * CD\nMusic of Chance * Book\nDreams of my Father * Book\nLow * CD\nDesire * CD"],
   ["The Scream * CD\nLow * CD\nDesire * CD"],
   ["Treeless Plain * CD\nOscar and Lucinda * Book"],
   ["Treeless Plain * CD"]),
  (********** #113 **********)
  (["0.1 +- 0.01","0.2 +- 0.2"],
   ["$0.1 \\pm 0.01$","$0.2 \\pm 0.2$"],
   ["0.2 +- 0.02","0.3 +- 0.03","0.44 +- 0.09"],
   ["$0.2 \\pm 0.02$","$0.3 \\pm 0.03$","$0.44 \\pm 0.09$"]),
  (********** #114 **********)
  (["Da Capo (1966)\nSeven and Seven Is\nLost in the Clouds","Forever Changes (1967)\nAlone Again Or\nYou Set The Scene"],
   ["Seven and Seven Is\nLost in the Clouds","Alone Again Or\nYou Set The Scene"],
   ["Da Capo (1966)\nSeven and Seven Is\nLost in the Clouds\nMy Little Red Book","Forever Changes (1967)\nAlone Again Or\nAndmoreagain\nYou Set The Scene","Village Green (1967)\nVictoria\nWicked Annabella"],
   ["Seven and Seven Is\nLost in the Clouds\nMy Little Red Book","Alone Again Or\nAndmoreagain\nYou Set The Scene","Victoria\nWicked Annabella"]),
  (********** #115 **********)
  (["001 Track01.mp3","002 Track02.mp3","003 Track03.mp3","004 Acid Violet [Remix].mp3"],
   ["Track01","Track02","Track03","Acid Violet [Remix]"],
   ["004 Mondays In Midtown.mp3","005 Track05.mp3","006 Track09.mp3"],
   ["Mondays In Midtown","Track05","Track09"]),
  (********** #117 **********)
  (["Alabama, Ala., AL, Montgomery","Alaska, Alaska, AK, Juneau","Arizona, Ariz., AZ, Phoenix","Arkansas, Ark., AR, Little Rock"],
   ["Alabama, AL","Alaska, AK","Arizona, AZ","Arkansas, AR"],
   ["California, Calif., CA, Sacramento","Colorado, Colo., CO, Denver","Connecticut, Conn., CT, Hartford","Delaware, Del., DE, Dover","Florida, Fla., FL, Tallahassee","Georgia, Ga., GA, Atlanta","Hawaii, Hawaii, HI, Honolulu","Idaho, Idaho, ID, Boise"],
   ["California, CA","Colorado, CO","Connecticut, CT","Delaware, DE","Florida, FL","Georgia, GA","Hawaii, HI","Idaho, ID"]),
  (********** #118 **********)
  (["Alabama, AL","Alaska, AK","Arizona, AZ","Arkansas, AR"],
   ["Alabama AL","Alaska AK","Arizona AZ","Arkansas AR"],
   ["California, CA","Colorado, CO","Connecticut, CT","Delaware, DE","Florida, FL","Georgia, GA","Hawaii, HI","Idaho, ID"],
   ["California CA","Colorado CO","Connecticut CT","Delaware DE","Florida FL","Georgia GA","Hawaii HI","Idaho ID"]),
  (********** #119 **********)
  (["Alabama, AL","Alaska, AK","Arizona, AZ","Arkansas, AR"],
   ["Alabama       AL","Alaska        AK","Arizona       AZ","Arkansas      AR"],
   ["California, CA","Colorado, CO","Connecticut, CT","Delaware, DE","Florida, FL","Georgia, GA","Hawaii, HI","Idaho, ID"],
   ["California    CA","Colorado      CO","Connecticut   CT","Delaware      DE","Florida       FL","Georgia       GA","Hawaii        HI","Idaho         ID"]),
  (********** #120 **********)
  (["Alabama AL","Alaska AK","Arizona AZ","Arkansas AR"],
   ["Alabama       AL","Alaska        AK","Arizona       AZ","Arkansas      AR"],
   ["California CA","Colorado CO","Connecticut CT","Delaware DE","Florida FL","Georgia GA","Hawaii HI","Idaho ID"],
   ["California    CA","Colorado      CO","Connecticut   CT","Delaware      DE","Florida       FL","Georgia       GA","Hawaii        HI","Idaho         ID"]),
  (********** #121 **********)
  (["Alabama       AL","Alaska        AK","Arizona       AZ","Arkansas      AR"],
   ["Alabama AL","Alaska AK","Arizona AZ","Arkansas AR"],
   ["California    CA","Colorado      CO","Connecticut   CT","Delaware      DE","Florida       FL","Georgia       GA","Hawaii        HI","Idaho         ID"],
   ["California CA","Colorado CO","Connecticut CT","Delaware DE","Florida FL","Georgia GA","Hawaii HI","Idaho ID"]),
  (********** #127 **********)
  (["11,252,1310,10,Mr Scuff,0.373199476211,729,2006,0.jpg,http://kittenwar.com/c_images/2006/10/11/100000.2.jpg,http://kittenwar.com/kittens/100000","11,222,1310,10,Mr Scuff,0.369844789357,723,2006,1.jpg,http://kittenwar.com/c_images/2006/10/11/100001.2.jpg,http://kittenwar.com/kittens/100001","11,235,813,10,Tobby,0.578004535147,1157,2006,2.jpg,http://kittenwar.com/c_images/2006/10/11/100003.2.jpg,http://kittenwar.com/kittens/100003"],
   ["0.jpg","1.jpg","2.jpg"],
   ["11,229,917,10,Caden,0.548972452995,1141,2006,3.jpg,http://kittenwar.com/c_images/2006/10/11/100007.2.jpg,http://kittenwar.com/kittens/100007","11,230,1046,10,Hobbes And Goose Ii,0.486283185841,984,2006,4.jpg,http://kittenwar.com/c_images/2006/10/11/100009.2.jpg,http://kittenwar.com/kittens/100009","11,239,989,10,Hrh Jc Fluff,0.497961956522,980,2006,5.jpg,http://kittenwar.com/c_images/2006/10/11/100010.2.jpg,http://kittenwar.com/kittens/100010","11,210,1115,10,Rio Monster,0.452669358457,904,2006,6.jpg,http://kittenwar.com/c_images/2006/10/11/100016.2.jpg,http://kittenwar.com/kittens/100016","11,209,489,10,Scooter,0.739235500879,1578,2006,7.jpg,http://kittenwar.com/c_images/2006/10/11/100017.2.jpg,http://kittenwar.com/kittens/100017"],
   ["3.jpg","4.jpg","5.jpg","6.jpg","7.jpg"]),
  (********** #128 **********)
  (["Adam Tauman kopeland","Jason Ipswitch Hartline","Rachel Blumburg Rudich"],
   ["kopeland, Adam T.","Hartline, Jason I.","Rudich, Rachel B."],
   ["Casey Stevens Jones","Indiana Eats Meat"],
   ["Jones, Casey S.","Meat, Indiana E."]),
  (********** #129 **********)
  (["4","2","0.000001","0","-0.000001","-2","-4"],
   ["positive","positive","positive","zero","negative","negative","negative"],
   ["84","22","0.300001","-24"],
   ["positive","positive","positive","negative"]),
  (********** #130 **********)
  (["\"IBM\",176.63,\"7/7/2011\",\"2:34pm\",-1.08,176.78,177.23,176.15,3790122","\"MSFT\",26.82,\"7/7/2011\",\"2:34pm\",+0.49,26.50,26.84,26.36,35695160","\"HOLL\",1.61,\"7/7/2011\",\"1:55pm\",+0.05,1.56,1.62,1.56,20900"],
   ["IBM","MSFT","HOLL"],
   ["\"WE\",476.63,\"7/7/2011\",\"2:34pm\",-1.08,176.78,177.23,176.15,3790122","\"IMPROVE\",26.82,\"7/7/2011\",\"2:34pm\",+0.49,26.50,26.84,26.36,35695160","\"SURELY\",1.61,\"7/7/2011\",\"1:55pm\",+0.05,1.56,1.62,1.56,20900"],
   ["WE","IMPROVE","SURELY"]),
  (********** #131 **********)
  (["\"IBM\",176.63,\"7/7/2011\",\"2:34pm\",-1.08,176.78,177.23,176.15,3790122","\"MSFT\",26.82,\"7/7/2011\",\"2:34pm\",+0.49,26.50,26.82,26.36,35695160","\"HOLL\",1.61,\"7/7/2011\",\"1:55pm\",+0.05,1.56,-1.61,1.56,20900"],
   ["177.23","26.82","-1.61"],
   ["\"WE\",476.63,\"7/7/2011\",\"2:34pm\",-1.08,176.78,277.23,176.15,3790122","\"IMPROVE\",26.82,\"7/7/2011\",\"2:34pm\",+0.49,26.50,26.84,26.36,35695160","\"SURELY\",1.61,\"7/7/2011\",\"1:55pm\",+0.05,1.56,1.62,1.56,20900"],
   ["277.23","26.84","1.62"]),
  (********** #132 **********)
  (["\"IBM\",176.63,\"7/7/2011\",\"2:34pm\",-1.08,176.78,177.23,176.15,3790122","\"MSFT\",26.82,\"7/7/2011\",\"2:34pm\",+0.49,26.50,26.82,26.36,35695160","\"HOLL\",1.61,\"7/7/2011\",\"1:55pm\",+0.05,1.56,-1.61,1.56,20900"],
   ["-1.08","+0.49","+0.05"],
   ["\"WE\",476.63,\"7/7/2011\",\"2:34pm\",-.08,176.78,277.23,176.15,3790122","\"IMPROVE\",26.82,\"7/7/2011\",\"2:34pm\",+0.222,26.50,26.84,26.36,35695160","\"SURELY\",1.61,\"7/7/2011\",\"1:55pm\",+0,1.56,1.62,1.56,20900"],
   ["-.08","+0.222","+0"]),
  (********** #133 **********)
  (["24th June 2010","22nd June 2010"],
   ["24 June 2010","22 June 2010"],
   ["23rd June 2010"],
   ["23 June 2010"]),
  (********** #134 **********)
  (["24th of June","22nd of June"],
   ["June 24","June 22"],
   ["23rd of June"],
   ["June 23"]),
  (********** #135 **********)
  (["www.google.com","www.mirrorview.com"],
   ["http://www.google.com","http://www.mirrorview.com"],
   ["www.movies.com"],
   ["http://www.movies.com"]),
  (********** #136 **********)
  (["Aditya malhotra","Robert Foster"],
   ["malhotra, A","Foster, R"],
   ["Peter Rosenheim","Adam kopeland","Sumit gaurav"],
   ["Rosenheim, P","kopeland, A","gaurav, S"]),
  (********** #137 **********)
  (["Aditya Krishna malhotra","Ricky Thomas Ponting"],
   ["A K malhotra","R T Ponting"],
   ["Sachin Ramesh Tendulkar"],
   ["S R Tendulkar"]),
  (********** #138 **********)
  (["Antonio Moreno 515 93th Lane ,Renton,WA,(411) 555-2786,562-87-3127,28581","Ana Trujillo 357 21th Place SE,Redmond,WA,(757) 555-1634,140-37-6064,27171","Christina Berglund 475 22th Lane ,Redmond,WA,(443) 555-6774,844-35-6764,30146","Jose Pedro Freyre 222 68th Place NE,Seattle,WA,(361) 555-7381,798-14-7411,25222"],
   ["515 93th Lane ,Renton,WA,(411) 555-2786,562-87-3127,28581","357 21th Place SE,Redmond,WA,(757) 555-1634,140-37-6064,27171","475 22th Lane ,Redmond,WA,(443) 555-6774,844-35-6764,30146","222 68th Place NE,Seattle,WA,(361) 555-7381,798-14-7411,25222"],
   ["Christinaxxxxx Berglund 475 22th Lane ,Redmond,WA,(443) 555-6774,844-35-6764,30146","Martine Rance 688 93th Place NW,Kent,WA,(573) 555-3571,695-94-3479,22424","Josexxxxx Pedro Freyre 222 68th Place NE,Seattle,WA,(361) 555-7381,798-14-7411,25222"],
   ["475 22th Lane ,Redmond,WA,(443) 555-6774,844-35-6764,30146","688 93th Place NW,Kent,WA,(573) 555-3571,695-94-3479,22424","222 68th Place NE,Seattle,WA,(361) 555-7381,798-14-7411,25222"]),
  (********** #139 **********)
  (["Antonio Moreno 515 93th Lane ,Renton,WA,(411) 555-2786,562-87-3127,28581","Ana Trujillo 357 21th Place SE,Redmond,WA,(757) 555-1634,140-37-6064,27171","Christina Berglund 475 22th Lane ,Redmond,WA,(443) 555-6774,844-35-6764,30146","Jose Pedro Freyre 222 68th Place NE,Seattle,WA,(361) 555-7381,798-14-7411,25222"],
   ["Antonio Moreno","Ana Trujillo","Christina Berglund","Jose Pedro Freyre"],
   ["Christinaxxxxx Berglund 475 22th Lane ,Redmond,WA,(443) 555-6774,844-35-6764,30146","Martine Rance 688 93th Place NW,Kent,WA,(573) 555-3571,695-94-3479,22424","Josexxxxx Pedro Freyre 222 68th Place NE,Seattle,WA,(361) 555-7381,798-14-7411,25222"],
   ["Christinaxxxxx Berglund","Martine Rance","Josexxxxx Pedro Freyre"]),
  (********** #140 **********)
  (["C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\0.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\0.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\1.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\1.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\2.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\2.trips.out"],
   ["0","1","2"],
   ["C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\3.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\3.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\4.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\4.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\5.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\5.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\6.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\6.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\7.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\7.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\8.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\8.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\9.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\9.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\10.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\10.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\11.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\11.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\12.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\12.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\13.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\13.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\14.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\14.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\15.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\15.trips.out"],
   ["3","4","5","6","7","8","9","10","11","12","13","14","15"]),
  (********** #141 **********)
  (["0","1","2"],
   ["C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\0.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\0.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\1.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\1.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\2.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\2.trips.out"],
   ["3","4","5","6","7","8","9","10","11","12","13","14","15"],
   ["C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\3.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\3.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\4.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\4.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\5.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\5.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\6.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\6.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\7.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\7.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\8.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\8.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\9.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\9.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\10.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\10.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\11.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\11.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\12.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\12.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\13.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\13.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\14.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\14.trips.out","C:\\Users\\axum>c:\\users\\axum\\simexp\\py\\runtrips_multiple_times.py  C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\kittens.config C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\15.trips C:\\Users\\axum\\dropbox\\axum\\papers\\atts\\simexps\\kittens\\halfspaces\\15.trips.out"]),
  (********** #142 **********)
  (["you can call me at 349-312-4950.","if that doesn't work, please try (858)-600-4014"],
   ["349-312-4950","(858)-600-4014"],
   ["XJohn DOE Typing 3 Data [TS] (865)-000-0000 - - 453442-00 06-23-2009","XA FF MARILYN TITANIC 30'S 865-000-0030 4535871-00 07-07-2009","XA GEDA-MARY oUTSIDE 100MG TAT 865-000-0000 - - 5941-00 06-23-2009"],
   ["(865)-000-0000","865-000-0030","865-000-0000"]),
  (********** #144 **********)
  (["8632096110","9187765315"],
   ["(863)-209-6110","(918)-776-5315"],
   ["4518712495"],
   ["(451)-871-2495"]),
  (********** #145 **********)
  (["8632096110","9187765315"],
   ["863-209-6110","918-776-5315"],
   ["4518712495"],
   ["451-871-2495"]),
  (********** #146 **********)
  (["19700101","19830302","19800731"],
   ["01/01/1970","03/02/1983","07/31/1980"],
   ["19801003"],
   ["10/03/1980"]),
  (********** #147 **********)
  (["This is\tour first","test using\tmore than one input."],
   ["This is our first","test using more than one input."],
   ["Let's see if it\tworks.","It\tdoes!"],
   ["Let's see if it works.","It does!"]),
  (********** #148 **********)
  (["Aditya malhotra","Robert Foster"],
   ["A. malhotra","R. Foster"],
   ["Peter Rosenheim","Adam kopeland","Sumit gaurav"],
   ["P. Rosenheim","A. kopeland","S. gaurav"]),
  (********** #149 **********)
  (["Aditya Krishna malhotra","Ricky Thomas Ponting"],
   ["A. K. malhotra","R. T. Ponting"],
   ["Sachin Ramesh Tendulkar"],
   ["S. R. Tendulkar"]),
  (********** #150 **********)
  (["07/09/2011\tAT THE KNEES","07/18/2011\tFARLEYSHELONWHEEL","07/11/2011\tFARLEYSHELONWHEEL","07/09/2011\tGOLDISPRETTY","07/09/2011\tJ B THREE","07/09/2011\tKWAME"],
   ["07/09/2011\tAT THE KNEES","07/18/2011\tFARLEYSHELONWHEEL","07/11/2011\tFARLEYSHELONWHEEL","07/09/2011\tGOLDISPRETTY","07/09/2011\tJ B THREE","07/09/2011\tKWAME"],
   ["07/18/2011\tFARLEYSHELONWHEELS","07/11/2011\tFARLEYSHELONWHEELS"],
   ["07/18/2011\tFARLEYSHELONWHEELS","07/11/2011\tFARLEYSHELONWHEELS"]),
  (********** #151 **********)
  (["Delgany Blaze 12 P","Alright My Son 300","24 Finders 14 Keepers"],
   ["12","300","24"],
   ["1255 Crescent Cross"],
   ["1255"]),
  (********** #152 **********)
  (["Alabama AL 321","Alaska AK 325","Arizona AZ 245","New Hampshire NH 584","New York NY 913"],
   ["Alabama         AL    321","Alaska          AK    325","Arizona         AZ    245","New Hampshire   NH    584","New York        NY    913"],
   ["Arkansas AR 584","South Carolina SC 865"],
   ["Arkansas        AR    584","South Carolina  SC    865"]),
  (********** #153 **********)
  (["test number=0,eta=0,k=1","test number=1,eta=1,k=1","test number=2,eta=1,k=1"],
   ["0,0,1","1,1,1","2,1,1"],
   ["test number=2,eta=1,k=5","test number=3,eta=1,k=1"],
   ["2,1,5","3,1,1"]),
  (********** #154 **********)
  (["1","2","3","4","5"],
   ["Jan 01 2001","Jan 02 2001","Jan 03 2001","Jan 04 2001","Jan 05 2001"],
   ["6","7","8"],
   ["Jan 06 2001","Jan 07 2001","Jan 08 2001"]),
  (********** #155 **********)
  (["0","1","2","3"],
   ["cat /usr/files/0.txt","cat /usr/files/1.txt","cat /usr/files/2.txt","cat /usr/files/3.txt"],
   ["4","5","6","7","8","9","10"],
   ["cat /usr/files/4.txt","cat /usr/files/5.txt","cat /usr/files/6.txt","cat /usr/files/7.txt","cat /usr/files/8.txt","cat /usr/files/9.txt","cat /usr/files/10.txt"]),
  (********** #156 **********)
  (["1","2","3"],
   ["rename \"female (1).jpg\" b1.jpg","rename \"female (2).jpg\" b2.jpg","rename \"female (3).jpg\" b3.jpg"],
   ["4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60"],
   ["rename \"female (4).jpg\" b4.jpg","rename \"female (5).jpg\" b5.jpg","rename \"female (6).jpg\" b6.jpg","rename \"female (7).jpg\" b7.jpg","rename \"female (8).jpg\" b8.jpg","rename \"female (9).jpg\" b9.jpg","rename \"female (10).jpg\" b10.jpg","rename \"female (11).jpg\" b11.jpg","rename \"female (12).jpg\" b12.jpg","rename \"female (13).jpg\" b13.jpg","rename \"female (14).jpg\" b14.jpg","rename \"female (15).jpg\" b15.jpg","rename \"female (16).jpg\" b16.jpg","rename \"female (17).jpg\" b17.jpg","rename \"female (18).jpg\" b18.jpg","rename \"female (19).jpg\" b19.jpg","rename \"female (20).jpg\" b20.jpg","rename \"female (21).jpg\" b21.jpg","rename \"female (22).jpg\" b22.jpg","rename \"female (23).jpg\" b23.jpg","rename \"female (24).jpg\" b24.jpg","rename \"female (25).jpg\" b25.jpg","rename \"female (26).jpg\" b26.jpg","rename \"female (27).jpg\" b27.jpg","rename \"female (28).jpg\" b28.jpg","rename \"female (29).jpg\" b29.jpg","rename \"female (30).jpg\" b30.jpg","rename \"female (31).jpg\" b31.jpg","rename \"female (32).jpg\" b32.jpg","rename \"female (33).jpg\" b33.jpg","rename \"female (34).jpg\" b34.jpg","rename \"female (35).jpg\" b35.jpg","rename \"female (36).jpg\" b36.jpg","rename \"female (37).jpg\" b37.jpg","rename \"female (38).jpg\" b38.jpg","rename \"female (39).jpg\" b39.jpg","rename \"female (40).jpg\" b40.jpg","rename \"female (41).jpg\" b41.jpg","rename \"female (42).jpg\" b42.jpg","rename \"female (43).jpg\" b43.jpg","rename \"female (44).jpg\" b44.jpg","rename \"female (45).jpg\" b45.jpg","rename \"female (46).jpg\" b46.jpg","rename \"female (47).jpg\" b47.jpg","rename \"female (48).jpg\" b48.jpg","rename \"female (49).jpg\" b49.jpg","rename \"female (50).jpg\" b50.jpg","rename \"female (51).jpg\" b51.jpg","rename \"female (52).jpg\" b52.jpg","rename \"female (53).jpg\" b53.jpg","rename \"female (54).jpg\" b54.jpg","rename \"female (55).jpg\" b55.jpg","rename \"female (56).jpg\" b56.jpg","rename \"female (57).jpg\" b57.jpg","rename \"female (58).jpg\" b58.jpg","rename \"female (59).jpg\" b59.jpg","rename \"female (60).jpg\" b60.jpg"]),
  (********** #157 **********)
  (["1988 David Haussler, Leonard Pitt","1989 Ron Rivest","1990 John Case","1991 Leslie Valiant","1992 David Haussler","2011 Ulrike von Luxburg, Sham Kakade"],
   ["988 David Haussler, Leonard Pitt","989 Ron Rivest","990 John Case","991 Leslie Valiant","992 David Haussler","011 Ulrike von Luxburg, Sham Kakade"],
   ["1993 Lenny Pitt","1994 Manfred Warmuth","1995 Wolfgang Maass","1996 Avrim Blum, Michael Kearns","1997 Yoav Freund, Robert Schapire","1998 Peter Bartlett, Yishay Mansour","1999 Shai Ben-David, Phil Long","2000 Nicolo Cesa-Bianchi, Sally Goldman","2001 David Helmbold, Bob Williamson","2002 Jyrki Kivinen, Bob Sloan","2003 Bernhard Schollkopf, Manfred Warmuth","2004 John Shawe-Taylor, Yoram Singer","2005 Peter Auer, Ron Meir","2006 Gabor Lugosi, Avrim Blum","2007 Nader Bshouty, Claudio Gentile","2008 Rocco Servedio, Tong Zhang","2009 Adam Klivans, Sanjoy Dasgupta","2010 Mehryar Mohry, Adam kopeland"],
   ["993 Lenny Pitt","994 Manfred Warmuth","995 Wolfgang Maass","996 Avrim Blum, Michael Kearns","997 Yoav Freund, Robert Schapire","998 Peter Bartlett, Yishay Mansour","999 Shai Ben-David, Phil Long","000 Nicolo Cesa-Bianchi, Sally Goldman","001 David Helmbold, Bob Williamson","002 Jyrki Kivinen, Bob Sloan","003 Bernhard Schollkopf, Manfred Warmuth","004 John Shawe-Taylor, Yoram Singer","005 Peter Auer, Ron Meir","006 Gabor Lugosi, Avrim Blum","007 Nader Bshouty, Claudio Gentile","008 Rocco Servedio, Tong Zhang","009 Adam Klivans, Sanjoy Dasgupta","010 Mehryar Mohry, Adam kopeland"]),
  (********** #158 **********)
  (["1988 David Haussler, Leonard Pitt","1989 Ron Rivest","1990 John Case","1991 Leslie Valiant","1992 David Haussler","2011 Ulrike von Luxburg, Sham Kakade"],
   ["1988 David Haussler, Leonard Pit","1989 Ron Rives","1990 John Cas","1991 Leslie Valian","1992 David Haussle","2011 Ulrike von Luxburg, Sham Kakad"],
   ["1993 Lenny Pitt","1994 Manfred Warmuth","1995 Wolfgang Maass","1996 Avrim Blum, Michael Kearns","1997 Yoav Freund, Robert Schapire","1998 Peter Bartlett, Yishay Mansour","1999 Shai Ben-David, Phil Long","2000 Nicolo Cesa-Bianchi, Sally Goldman","2001 David Helmbold, Bob Williamson","2002 Jyrki Kivinen, Bob Sloan","2003 Bernhard Schollkopf, Manfred Warmuth","2004 John Shawe-Taylor, Yoram Singer","2005 Peter Auer, Ron Meir","2006 Gabor Lugosi, Avrim Blum","2007 Nader Bshouty, Claudio Gentile","2008 Rocco Servedio, Tong Zhang","2009 Adam Klivans, Sanjoy Dasgupta","2010 Mehryar Mohry, Adam kopeland"],
   ["1993 Lenny Pit","1994 Manfred Warmut","1995 Wolfgang Maas","1996 Avrim Blum, Michael Kearn","1997 Yoav Freund, Robert Schapir","1998 Peter Bartlett, Yishay Mansou","1999 Shai Ben-David, Phil Lon","2000 Nicolo Cesa-Bianchi, Sally Goldma","2001 David Helmbold, Bob Williamso","2002 Jyrki Kivinen, Bob Sloa","2003 Bernhard Schollkopf, Manfred Warmut","2004 John Shawe-Taylor, Yoram Singe","2005 Peter Auer, Ron Mei","2006 Gabor Lugosi, Avrim Blu","2007 Nader Bshouty, Claudio Gentil","2008 Rocco Servedio, Tong Zhan","2009 Adam Klivans, Sanjoy Dasgupt","2010 Mehryar Mohry, Adam Kala"]),
  (********** #159 **********)
  (["aditya malhotra","adam kopeland","sumit gaurav"],
   ["aditya   malhotra","adam     kopeland","sumit    gaurav"],
   ["steve harley","michael lyons"],
   ["steve    harley","michael  lyons"]),
  (********** #160 **********)
  (["ALabama AL","ALaska AK","ARizona AZ","ARkansas AR"],
   ["ALabama       AL","ALaska        AK","ARizona       AZ","ARkansas      AR"],
   ["CAlifornia CA","COlorado CO","COnnecticut CT","DElaware DE","FLorida FL","GEorgia GA","HAwaii HI","IDaho ID"],
   ["CAlifornia    CA","COlorado      CO","COnnecticut   CT","DElaware      DE","FLorida       FL","GEorgia       GA","HAwaii        HI","IDaho         ID"]),
  (********** #161 **********)
  (["aditya malhotra 2111 mesa street 92122","rob ford 764 corset crescent 02215","james earl 82 downing 34524"],
   ["aditya malhotra 2111 mesa street    92122","rob ford     764 corset crescent 02215","james earl   82 downing          34524"],
   ["james earl 15 nobel drive 20064"],
   ["james earl   15 nobel drive      20064"]),
  (********** #162 **********)
  (["aditya krishna malhotra","james earl jones","william carlos williams"],
   ["aditya  krishna malhotra","james   earl    jones","william carlos  williams"],
   ["aditya  krishna malhotra","james   earl    jones","william carlos  williams","bob allen zimmerman"],
   ["aditya  krishna malhotra","james   earl    jones","william carlos  williams","bob     allen   zimmerman"]),
  (********** #163 **********)
  (["aditya malhotra 2111 mesa street 92122","rob ford 764 corset crescent 02215","john twelve(12) hawks 0 area 51 00000"],
   ["aditya malhotra          2111 mesa street    92122","rob ford              764 corset crescent 02215","john twelve(12) hawks 0 area 51           00000"],
   ["bill seven7 joy 42 jay street 039845"],
   ["bill seven7 joy       42 jay street       039845"]),
  (********** #165 **********)
  (["/home/foo/realllylongname.cpp"],
   ["cp /home/foo/realllylongname.cpp /home/foo/realllylongname.cpp.old"],
   ["/home/foo/a1.cpp","/home/foo/a2.cpp","/home/foo/a3.cpp"],
   ["cp /home/foo/a1.cpp /home/foo/a1.cpp.old","cp /home/foo/a2.cpp /home/foo/a2.cpp.old","cp /home/foo/a3.cpp /home/foo/a3.cpp.old"]),
  (********** #166 **********)
  (["DSC001\tb1","DSC002\tb2","DSC003\tb3"],
   ["mv \"DSC001.jpg\" b1.jpeg","mv \"DSC002.jpg\" b2.jpeg","mv \"DSC003.jpg\" b3.jpeg"],
   ["DSC004\tb4","DSC005\tb5"],
   ["mv \"DSC004.jpg\" b4.jpeg","mv \"DSC005.jpg\" b5.jpeg"]),
  (********** #167 **********)
  (["DSC001\tb1","DSC0002\tb02"],
   ["mv \"DSC001.jpg\" b1.jpeg","mv \"DSC0002.jpg\" b02.jpeg"],
   ["DSC00003\tb003"],
   ["mv \"DSC00003.jpg\" b003.jpeg"]),
  (********** #168 **********)
  (["001","002","003"],
   ["mv \"DSC001.jpg\" b001.jpeg","mv \"DSC002.jpg\" b002.jpeg","mv \"DSC003.jpg\" b003.jpeg"],
   ["004","005"],
   ["mv \"DSC004.jpg\" b004.jpeg","mv \"DSC005.jpg\" b005.jpeg"]),
  (********** #169 **********)
  (["DSC001\tb1","DSC001\tb2","DSC001\tb3"],
   ["mv \"DSC001.jpg\" b1.jpeg","mv \"DSC001.jpg\" b2.jpeg","mv \"DSC001.jpg\" b3.jpeg"],
   ["DSC002\tb1","DSC002\tb2","DSC002\tb3","DSC002\tb4","DSC002\tb5"],
   ["mv \"DSC002.jpg\" b1.jpeg","mv \"DSC002.jpg\" b2.jpeg","mv \"DSC002.jpg\" b3.jpeg","mv \"DSC002.jpg\" b4.jpeg","mv \"DSC002.jpg\" b5.jpeg"]),
  (********** #171 **********)
  (["the lion king!disney!12.25","heads n tales!harry chapin!12.50"],
   ["the lion king           disney          12.25","heads n tales           harry chapin    12.50"],
   ["photographs n memories!jim croce!4.95","tumbleweed connection!elton john!123.32"],
   ["photographs n memories  jim croce       4.95","tumbleweed connection   elton john      123.32"]),
  (********** #173 **********)
  (["the lion king!disney!12.25","heads & tales!harry chapin!12.50"],
   ["the lion king           disney          12.25","heads & tales           harry chapin    12.50"],
   ["photographs & memories!jim croce!4.95","tumbleweed connection!elton john!123.32"],
   ["photographs & memories  jim croce       4.95","tumbleweed connection   elton john      123.32"]),
  (********** #174 **********)
  (["\"IBM\",176.63,\"7/7/2011\",\"2:34pm\",-1.08,176.78,177.23,176.15,3790122","\"MSFT\",26.82,\"7/7/2011\",\"2:34pm\",+0.49,26.50,26.84,26.36,35695160"],
   ["\"IBM\",176.63,-1.08","\"MSFT\",26.82,+0.49"],
   ["\"HOLL\",1.61,\"7/7/2011\",\"1:55pm\",+0.05,1.56,1.62,1.56,20900"],
   ["\"HOLL\",1.61,+0.05"]),
  (********** #175 **********)
  (["\"IBM\" 176.63 -1.08","\"MSFT\" 26.82 +0.49"],
   ["\"IBM\"    176.63  -1.08","\"MSFT\"    26.82  +0.49"],
   ["\"HOLL\" 1.61 +0.05"],
   ["\"HOLL\"     1.61  +0.05"]),
  (********** #176 **********)
  (["\"IBM\"    176.63  -1.08","\"MSFT\"    26.82  +0.49","\"GCTA\"     5.51  -6.37"],
   ["decreased","increased","decreased"],
   ["\"HOLL\"     1.61  +0.05"],
   ["increased"]),
  (********** #177 **********)
  (["decreased\t5","increased\t3"],
   ["5 shares have decreased","3 shares have increased"],
   ["increased\t4"],
   ["4 shares have increased"]),
  (********** #178 **********)
  (["05:26","04:57"],
   ["05:00","04:30"],
   ["08:26","04:27","03:59"],
   ["08:00","04:00","03:30"]),
  (********** #179 **********)
  (["05:26","04:57","18:11"],
   ["05:30","05:00","18:30"],
   ["08:26","04:27","03:59"],
   ["08:30","04:30","04:00"]),
  (********** #180 **********)
  (["8","-3","5"],
   ["+ve","-ve","+ve"],
   ["6","-7"],
   ["+ve","-ve"]),
  (********** #181 **********)
  (["it's not their style to be so <b>bold</b>"],
   ["it's not their style to be so <i>bold</i>"],
   ["like a child they're longing to be <b>told</b>"],
   ["like a child they're longing to be <i>told</i>"]),
  (********** #182 **********)
  (["The album '''''Ram''''' was redone as an album of instrumental covers by '''''Paul and Linda McCartney''''' in 1977."],
   ["The album <b><i>Ram</i></b> was redone as an album of instrumental covers by <b><i>Paul and Linda McCartney</i></b> in 1977."],
   ["Wiki formatting can be '''''very''''' weird!"],
   ["Wiki formatting can be <b><i>very</i></b> weird!"]),
  (********** #183 **********)
  (["Some words are meant to be <i>bold</i>, others are <i>not</i> <i>bold</i> enough"],
   ["Some words are meant to be <b>bold</b>, others are <i>not</i> <b>bold</b> enough"],
   ["like a <i>child</i> they're longing to be <i>bold</i>"],
   ["like a <i>child</i> they're longing to be <b>bold</b>"]),
  (********** #184 **********)
  (["In the season of 1988-1989 the company was formed. In 1992 it went public. In 1993 it collapsed."],
   ["In the season of '88-'89 the company was formed. In '92 it went public. In '93 it collapsed."],
   ["As far as I am concerned, nothing they did after 1962 was useful."],
   ["As far as I am concerned, nothing they did after '62 was useful."]),
  (********** #185 **********)
  (["Alex (Assistant)","Robert (NULL)","James (Manager)","Snarski (NULL)","Luscombe (NULL)"],
   ["Alex (Assistant)","NULL","James (Manager)","NULL","NULL"],
   ["Brian (Psychic)","Clive (NULL)","Ruttiger (Gaffer)"],
   ["Brian (Psychic)","NULL","Ruttiger (Gaffer)"]),
  (********** #186 **********)
  (["Sumit gaurav Rishabh Singh Bill Harris Bill Gates Ben Zorn","Shuvendu Lahiri Wolfram Schulte Shaz Qadeer Nikhil Swamy Trishul Chilimbi Ben Lishvits"],
   ["gaurav Singh Harris Gates Zorn","Lahiri Schulte Qadeer Swamy Chilimbi Lishvits"],
   ["Foo Sumit gaurav Rishabh Singh Bill Harris Bill Gates Ben Zorn","Bar Shuvendu Lahiri Wolfram Schulte Shaz Qadeer Nikhil Swamy Trishul Chilimbi Ben Lishvits"],
   ["Sumit Rishabh Bill Bill Ben","Shuvendu Wolfram Shaz Nikhil Trishul Ben"]),
  (********** #187 **********)
  (["foo","","","bar","blah"],
   ["foo","NULL","NULL","bar","blah"],
   [],
   []),
  (********** #188 **********)
  (["Sumit gaurav Rishabh Singh Bill Harris Bill Gates Ben Zorn","Shuvendu Lahiri Wolfram Schulte Shaz Qadeer Nikhil Swamy Trishul Chilimbi Ben Lishvits"],
   ["Sumit Rishabh Bill Bill Ben","Shuvendu Wolfram Shaz Nikhil Trishul Ben"],
   ["Gordon Ramsey Ian Dury"],
   ["Gordon Ian"]),
  (********** #189 **********)
  (["Sachin Ramesh Tendulkar, James Earl Ray, William Carlos Williams","Reese Shear Smith, Sir Donald Bradman"],
   ["Sachin Tendulkar, James Ray, William Williams","Reese Smith, Sir Bradman"],
   ["Aditya Krishna malhotra, Anthony Graham Smith"],
   ["Aditya malhotra, Anthony Smith"]),
  (********** #190 **********)
  (["a bu c","d eza f","g hosa2 i"],
   ["bu+","eza+","hosa2+"],
   ["Every middle field;","that is how","we perfect ourselves"],
   ["middle+","is+","perfect+"]),
  (********** #191 **********)
  (["a b c","d e f","g h i z"],
   ["b+","e+","h+"],
   ["Every middle field;","that is how","we perfect ourselves doubly"],
   ["middle+","is+","perfect+"]),
  (********** #193 **********)
  (["Thursday June 24th 2010"],
   ["24 June 2010"],
   ["Wednesday June 23rd 2010"],
   ["23 June 2010"]),
  (********** #194 **********)
  (["355 Algeria","213 Lithuania"],
   ["case 355: return Algeria;","case 213: return Lithuania;"],
   ["612 Australia"],
   ["case 612: return Australia;"]),
  (********** #195 **********)
  (["1988 David Haussler, Leonard Pitt","1989 Ron Rivest","1990 John Case","2004 Foo Bar"],
   ["8 David Haussler, Leonard Pitt","9 Ron Rivest","0 John Case","4 Foo Bar"],
   ["1008 Rocco Servedio, Tong Zhang","2009 Adam Klivans, Sanjoy Dasgupta","2010 Mehryar Mohry, Adam kopeland","2011 Ulrike von Luxburg, Sham Kakade"],
   ["8 Rocco Servedio, Tong Zhang","9 Adam Klivans, Sanjoy Dasgupta","0 Mehryar Mohry, Adam kopeland","1 Ulrike von Luxburg, Sham Kakade"]),
  (********** #196 **********)
  (["Alabama\nAla.\nAL\nMontgomery","Alaska\nAlaska\nAK\nJuneau"],
   ["Alabama Ala. AL Montgomery","Alaska Alaska AK Juneau"],
   ["Arizona\nAriz.\nAZ\nPhoenix","Arkansas\nArk.\nAR\nLittle Rock","California\nCalif.\nCA\nSacramento","Colorado\nColo.\nCO\nDenver","Connecticut\nConn.\nCT\nHartford","Delaware\nDel.\nDE\nDover","Florida\nFla.\nFL\nTallahassee","Georgia\nGa.\nGA\nAtlanta","Hawaii\nHawaii\nHI\nHonolulu","Idaho\nIdaho\nID\nBoise"],
   ["Arizona Ariz. AZ Phoenix","Arkansas Ark. AR Little Rock","California Calif. CA Sacramento","Colorado Colo. CO Denver","Connecticut Conn. CT Hartford","Delaware Del. DE Dover","Florida Fla. FL Tallahassee","Georgia Ga. GA Atlanta","Hawaii Hawaii HI Honolulu","Idaho Idaho ID Boise"]),
  (********** #197 **********)
  (["Male Female Female Male","Male"],
   ["quiji ouiji ouiji quiji","quiji"],
   ["Female"],
   ["ouiji"]),
  (********** #198 **********)
  (["07/07/2010\tAT THE KNEES","07/18/2011\tFARLEYSHELONWHEEL\n07/18/2011\tPETERBOROUGH","07/09/2011\tGOLDISPRETTY"],
   ["07/07/2010\tAT THE KNEES","07/18/2011\tFARLEYSHELONWHEEL\t07/18/2011\tPETERBOROUGH","07/09/2011\tGOLDISPRETTY"],
   ["07/07/2011\tAT THE KNEES","07/18/2011\tFARLEYSHELONWHEEL\n07/18/2011\tFARLEYSHELONWHEEL","07/09/2011\tGOLDISPRETTY\n07/09/2011\tJ B THREE\n07/09/2011\tKWAME"],
   ["07/07/2011\tAT THE KNEES","07/18/2011\tFARLEYSHELONWHEEL\t07/18/2011\tFARLEYSHELONWHEEL","07/09/2011\tGOLDISPRETTY\t07/09/2011\tJ B THREE\t07/09/2011\tKWAME"]),
  (********** #199 **********)
  (["Africa:\nSudan\nAlgeria","Asia:\nCIS"],
   ["Africa:\n\tSudan\n\tAlgeria","Asia:\n\tCIS"],
   ["Asia:\nCIS\nChina\nIndia","Australia:\nAustralia","North America:\nCanada\nUSA","South America:\nBrazil\nArgentina"],
   ["Asia:\n\tCIS\n\tChina\n\tIndia","Australia:\n\tAustralia","North America:\n\tCanada\n\tUSA","South America:\n\tBrazil\n\tArgentina"]),
  (********** #200 **********)
  (["Africa\nSudan\t0\nAlgeria\t1","Ocean\nCIS\t2"],
   ["Africa:\nSudan\t0\nAlgeria\t1","Ocean:\nCIS\t2"],
   ["Africa\nSudan\t0\nAlgeria\t2","Asia\nCIS\t3\nChina\t4\nIndia\t5","Australia\nAustralia\t6","North America\nCanada\t7\nUSA\t8","South America\nBrazil\t9\nArgentina\t10"],
   ["Africa:\nSudan\t0\nAlgeria\t2","Asia:\nCIS\t3\nChina\t4\nIndia\t5","Australia:\nAustralia\t6","North America:\nCanada\t7\nUSA\t8","South America:\nBrazil\t9\nArgentina\t10"]),
  (********** #201 **********)
  (["07/07/2011\tAT THE KNEES","07/18/2011\tFARLEYSHELONWHEEL\t07/18/2011\tPETERBOROUGH","07/09/2011\tGOLDISPRETTY"],
   ["07/07/2011\tAT THE KNEES","07/18/2011\tFARLEYSHELONWHEEL\tPETERBOROUGH","07/09/2011\tGOLDISPRETTY"],
   ["07/09/2011\tGOLDISPRETTY\t07/09/2011\tJ B THREE\t07/09/2011\tKWAME"],
   ["07/09/2011\tGOLDISPRETTY\tJ B THREE\tKWAME"]),
  (********** #202 **********)
  (["07/07/2011\tAT THE KNEES","07/18/2011\tFARLEYSHELONWHEEL\t07/18/2011\tFARLEYSHELONWHEEL","07/09/2011\tGOLDISPRETTY"],
   ["07/07/2011\tAT THE KNEES","07/18/2011\tFARLEYSHELONWHEEL\tFARLEYSHELONWHEEL","07/09/2011\tGOLDISPRETTY"],
   ["07/09/2011\tGOLDISPRETTY\t07/09/2011\tJ B THREE\t07/09/2011\tKWAME"],
   ["07/09/2011\tGOLDISPRETTY\tJ B THREE\tKWAME"]),
  (********** #203 **********)
  (["\"IBM\" 176.63 1.08","\"MSFT\" 26.82 0.49"],
   ["\"IBM\"       176.63  1.08","\"MSFT\"      26.82   0.49"],
   ["\"HOLL\" 12.18 5.64"],
   ["\"HOLL\"      12.18   5.64"]),
  (********** #204 **********)
  (["af1988 David Haussler, Leonard Pitt","321989 Ron Rivest","121990 John Case","rg1988 David Haussler, Leonard Pitt","921989 Ron Rivest","121990 John Case","af1988 David Haussler, Leonard Pitt","321989 Ron Rivest","121990 John Case"],
   ["1988 David Haussler, Leonard Pitt","1989 Ron Rivest","1990 John Case","1988 David Haussler, Leonard Pitt","1989 Ron Rivest","1990 John Case","1988 David Haussler, Leonard Pitt","1989 Ron Rivest","1990 John Case"],
   ["ab1990 John Case","cd1988 David Haussler, Leonard Pitt","991989 Ron Rivest","831990 John "],
   ["1990 John Case","1988 David Haussler, Leonard Pitt","1989 Ron Rivest","1990 John "]),
  (********** #205 **********)
  (["88 David Haussler, Leonard Pitt","89 Ron Rivest","90 John Case, Steve"],
   ["88 David Haussler\n88 Leonard Pitt","89 Ron Rivest","90 John Case\n90 Steve"],
   ["94 Manfred Warmuth","95 Wolfgang Maass","96 Avrim Blum, Michael Kearns","97 Yoav Freund, Robert Schapire"],
   ["94 Manfred Warmuth","95 Wolfgang Maass","96 Avrim Blum\n96 Michael Kearns","97 Yoav Freund\n97 Robert Schapire"]),
  (********** #206 **********)
  (["88 David Haussler, Leonard Pitt","89 Ron Rivest","90 John Case, Steve"],
   ["88 David Haussler","89 Ron Rivest","90 John Case"],
   ["94 Manfred Warmuth","95 Wolfgang Maass","96 Avrim Blum, Michael Kearns","97 Yoav Freund, Robert Schapire"],
   ["94 Manfred Warmuth","95 Wolfgang Maass","96 Avrim Blum","97 Yoav Freund"]),
  (********** #207 **********)
  (["123456","145678"],
   ["123-456","145-678"],
   ["251245"],
   ["251-245"]),
  (********** #208 **********)
  (["9500\nGilman\nDrive\nLa\nJolla\nCA","1\nMiramar\nStreet\nApartment 4120\nLa Jolla\nCA"],
   ["9500 Gilman Drive La Jolla CA","1 Miramar Street Apartment 4120 La Jolla CA"],
   ["9498\nGilman\nDrive\nUnit\n#504\nLa\nJolla\nCA"],
   ["9498 Gilman Drive Unit #504 La Jolla CA"]),
  (********** #209 **********)
  (["$0.6456 \\pm 0.01$\n$0.4295 \\pm 0.06$\n$0.2543 \\pm 0.05$"],
   ["$0.6456 \\pm 0.01$ & $0.4295 \\pm 0.06$ & $0.2543 \\pm 0.05$"],
   ["$0.5215 \\pm 0.04$\n$0.1232 \\pm 0.053$"],
   ["$0.5215 \\pm 0.04$ & $0.1232 \\pm 0.053$"]),
  (********** #210 **********)
  (["MALE-34.jpg\t205","Male-17.JPG\t206","Male-2.JPG\t207"],
   ["mv MALE-34.jpg a205.jpg","mv Male-17.JPG a206.jpg","mv Male-2.JPG a207.jpg"],
   ["blah.jpg\t210","foobar.jpg\t204"],
   ["mv blah.jpg a210.jpg","mv foobar.jpg a204.jpg"]),
  (********** #211 **********)
  (["205\tMALE-34.jpg","206\tMale-17.JPG","207\tMale-2.JPG"],
   ["mv MALE-34.jpg a205.jpg","mv Male-17.JPG a206.jpg","mv Male-2.JPG a207.jpg"],
   ["210\tblah.jpg","204\tfoo.jpg"],
   ["mv blah.jpg a210.jpg","mv foo.jpg a204.jpg"]),
  (********** #212 **********)
  (["1. Head north on Tonnele Ave toward Pavonia Ave\n0.5 mi","2. Slight right to merge onto NJ-139 E toward Holland Tunnel\n1.1 mi","3. Merge onto I-78 E\nPartial toll road\nEntering New York\n2.2 mi"],
   ["1. Head north on Tonnele Ave toward Pavonia Ave\n[0.5 mi]","2. Slight right to merge onto NJ-139 E toward Holland Tunnel\n[1.1 mi]","3. Merge onto I-78 E\nPartial toll road\nEntering New York\n[2.2 mi]"],
   ["4. Continue onto Lincoln Hwy\nToll road\n0.2 mi"],
   ["4. Continue onto Lincoln Hwy\nToll road\n[0.2 mi]"]),
  (********** #213 **********)
  (["embellish","me"],
   ["(embellish)","(me)"],
   ["{this}","{works}"],
   ["({this})","({works})"]),
  (********** #214 **********)
  (["i don't know or can't xetermine if this isn't a thing others wouldn't do"],
   ["i don't know or can t xetermine if this isn't a thing others wouldn t do"],
   ["a'one'a'two'a'three"],
   ["a'one a'two a'three"]),
  (********** #215 **********)
  (["this american life","movies on demand","sink or swim"],
   ["this a life","movies o demand","sink o swim"],
   ["lost in space"],
   ["lost i space"]),
  (********** #216 **********)
  (["this american life","movies on demand","sink or swim"],
   ["t american life","m on demand","s or swim"],
   ["lost in space"],
   ["l in space"]),
  (********** #217 **********)
  (["Thursday 24th June 2010"],
   ["24 June 2010"],
   ["Wednesday 23rd June 2010"],
   ["23 June 2010"]),
  (********** #218 **********)
  (["acs3 b cdd","d e f","g ah i"],
   ["b+","e+","ah+"],
   ["Every middle field;","that is how","we perfect ourselves"],
   ["middle+","is+","perfect+"]),
  (********** #220 **********)
  (["Aditya Krishna malhotra","Ricky Thomas Ponting","Parampal Singh Pooni"],
   ["A K malhotra","R T Ponting","P S Pooni"],
   ["Sachin Ramesh Tendulkar"],
   ["S R Tendulkar"]),
  (********** #221 **********)
  (["24th June","22nd June"],
   ["June 24","June 22"],
   ["23rd June"],
   ["June 23"]),
  (********** #222 **********)
  (["June the 24th","April the 22nd"],
   ["June 24","April 22"],
   ["June the 23rd"],
   ["June 23"]),
  (********** #223 **********)
  (["Madhur Malik","Sumit gaurav","First Last","Darren Gehring"],
   ["Madhur Mali.k.","Sumit Gulwan.i.","First Las.t.","Darren Gehrin.g."],
   ["Rohit Singh"],
   ["Rohit Sing.h."]),
  (********** #224 **********)
  (["Thursday June 24th 2010","Friday April 5th 1985"],
   ["Thursday, June 24th 2010","Friday, April 5th 1985"],
   ["Wednesday June 23rd 2010"],
   ["Wednesday, June 23rd 2010"]),
  (********** #225 **********)
  (["D. Blei (1)","M. Jordan (2)","D. Klein (1)","P. Liang (1)"],
   ["D. Blei","M. Jordan","D. Klein","P. Liang"],
   ["D. Klein (3)","A. Roberts (1)"],
   ["D. Klein","A. Roberts"])
]

fun escape' (#"'" :: cs) = #"'" :: #"'" :: escape' cs
  | escape' (c :: cs) = c :: escape' cs
  | escape' [] = []

fun escape s = implode (escape' (explode s))

fun printExample (input, output) =
  print ("insert into examples (input, output) values ('" ^ escape input ^ "','" ^ escape output ^ "');\n")

fun printDataset (trainInputs, trainOutputs, testInputs, testOutputs) =
  (List.map printExample (ListPair.zipEq (trainInputs, trainOutputs));
   List.map printExample (ListPair.zipEq (testInputs, testOutputs)))

val _ = List.map printDataset datasets
