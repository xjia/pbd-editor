create table examples (
  id integer primary key,
  input text not null,
  output text not null
);

create table traces (
  id integer primary key,
  example_id int not null,
  user varchar(100) not null,
  trace text not null
);

insert into examples (input, output) values ('1731 126th AVE SE, Bellevue, WA 98005','Bellevue');
insert into examples (input, output) values ('One Microsoft Way, Redmond, WA 98052','Redmond');
insert into examples (input, output) values ('111 Main St, Bellevue, WA 98052','Bellevue');
insert into examples (input, output) values ('17400 NE 40th St, Redmond, WA 98052','Redmond');
insert into examples (input, output) values ('10 West St, New York, NY 02134','New York');
insert into examples (input, output) values ('Antonio Moreno 515 93th Lane ,Renton,WA,(411) 555-2786,562-87-3127,28581','515 93th Lane');
insert into examples (input, output) values ('Ana Trujillo 357 21th Place SE,Redmond,WA,(757) 555-1634,140-37-6064,27171','357 21th Place SE');
insert into examples (input, output) values ('Christina Berglund 475 22th Lane ,Redmond,WA,(443) 555-6774,844-35-6764,30146','475 22th Lane');
insert into examples (input, output) values ('Jose Pedro Freyre 222 68th Place NE,Seattle,WA,(361) 555-7381,798-14-7411,25222','222 68th Place NE');
insert into examples (input, output) values ('Christinaxxxxx Berglund 475 22th Lane ,Redmond,WA,(443) 555-6774,844-35-6764,30146','475 22th Lane');
insert into examples (input, output) values ('Martine Rance 688 93th Place NW,Kent,WA,(573) 555-3571,695-94-3479,22424','688 93th Place NW');
insert into examples (input, output) values ('Josexxxxx Pedro Freyre 222 68th Place NE,Seattle,WA,(361) 555-7381,798-14-7411,25222','222 68th Place');
insert into examples (input, output) values ('John Smith
111 Main St.
Bellevue
WA
90111','John Smith');
insert into examples (input, output) values ('Frank Thomas
222 Main St.
Redmond
CA
90112','Frank Thomas');
insert into examples (input, output) values ('Mike Myers
333 Main St.
Kirkland
MA
90114','Mike Myers');
insert into examples (input, output) values ('Mike L Myers
332 Main St. station apt 42
Jokesville
MA
90232','Mike L Myers');
insert into examples (input, output) values ('John Smith
111 Main St.
Bellevue
WA
90111','111 Main St.');
insert into examples (input, output) values ('Frank Thomas
222 Main St.
Redmond
CA
90112','222 Main St.');
insert into examples (input, output) values ('Mike Myers
333 Main St.
Kirkland
MA
90113','333 Main St.');
insert into examples (input, output) values ('John Smith
111 Main St.
Bellevue
WA
90111','Bellevue');
insert into examples (input, output) values ('Frank Thomas
222 Main St.
Redmond
CA
90112','Redmond');
insert into examples (input, output) values ('Mike Myers
333 Main St.
Kirkland
WA
90113','Kirkland');
insert into examples (input, output) values ('John Smith
111 Main St.
Bellevue
WA
90111','WA');
insert into examples (input, output) values ('Frank Thomas
222 Main St.
Redmond
CA
90112','CA');
insert into examples (input, output) values ('Mike Myers
333 Main St.
Kirkland
WA
90113','WA');
insert into examples (input, output) values ('John Smith
111 Main St.
Bellevue
WA
90111','90111');
insert into examples (input, output) values ('Frank Thomas
111 Main St.
Bellevue
CA
90112','90112');
insert into examples (input, output) values ('Mike Myers
111 Main St.
Bellevue
MA
90113','90113');
insert into examples (input, output) values ('Albania	355','case 355: return Albania;');
insert into examples (input, output) values ('Algeria	213','case 213: return Algeria;');
insert into examples (input, output) values ('latvia	371','case 371: return latvia;');
insert into examples (input, output) values ('St Kitts & Nevia	8969','case 8969: return St Kitts & Nevia;');
insert into examples (input, output) values ('Turks & Caios Islands	649','case 649: return Turks & Caios Islands;');
insert into examples (input, output) values ('Sao Tome & Principe	239','case 239: return Sao Tome & Principe;');
insert into examples (input, output) values ('Andorra	376','case 376: return Andorra;');
insert into examples (input, output) values ('8-7-2010','8/7/2010');
insert into examples (input, output) values ('9-8-2009','9/8/2009');
insert into examples (input, output) values ('3/27/2010','3/27/2010');
insert into examples (input, output) values ('4/15/2010','4/15/2010');
insert into examples (input, output) values ('27.3.2010','3/27/2010');
insert into examples (input, output) values ('15.11.2010','11/15/2010');
insert into examples (input, output) values ('12/23/2010','12/23/2010');
insert into examples (input, output) values ('332/144/2010','332/144/2010');
insert into examples (input, output) values ('17.5.2010','5/17/2010');
insert into examples (input, output) values ('Today is 16/4/2010, we are going to have fun on 14/7/2008, the time we are going to be there at 12/3/2010. We can try to go there while 6/8/2010 as we are doing to 5/8/2010. Today is 16/4/2010, we are going to have fun on 14/7/2008, the time we are going to be there at 12/3/2010','16/4/2010 14/7/2008 12/3/2010 6/8/2010 5/8/2010 16/4/2010 14/7/2008 12/3/2010');
insert into examples (input, output) values ('I am trying to work when 4/2/2008, where we went on 21/3/2010. Its like 6/2/2010 as we are doing to 1/8/2010. Today is 16/4/2010, we are going to have fun on 14/7/2008, the time we are going to be there at 12/3/2010','4/2/2008 21/3/2010 6/2/2010 1/8/2010 16/4/2010 14/7/2008 12/3/2010');
insert into examples (input, output) values ('26.2','26.20');
insert into examples (input, output) values ('10.4','10.40');
insert into examples (input, output) values ('3/4/2010','2010 3 4');
insert into examples (input, output) values ('14/2/1885','1885 14 2');
insert into examples (input, output) values ('23/12/2010','2010 23 12');
insert into examples (input, output) values ('24/6/2010','June the 24th 2010');
insert into examples (input, output) values ('28/6/2010','June the 28th 2010');
insert into examples (input, output) values ('Thursday, 24th June 2010','24 June 2010');
insert into examples (input, output) values ('Wednesday, 23rd June 2010','23 June 2010');
insert into examples (input, output) values ('5/7/1988','5 7 1988');
insert into examples (input, output) values ('5/14/1988','5 14 1988');
insert into examples (input, output) values ('5/21/1988','5 21 1988');
insert into examples (input, output) values ('5/28/1988','5 28 1988');
insert into examples (input, output) values ('6/4/1988','6 4 1988');
insert into examples (input, output) values ('6/11/1988','6 11 1988');
insert into examples (input, output) values ('6/18/1988','6 18 1988');
insert into examples (input, output) values ('6/25/1988','6 25 1988');
insert into examples (input, output) values ('7/2/1988','7 2 1988');
insert into examples (input, output) values ('7/9/1988','7 9 1988');
insert into examples (input, output) values ('3/4/2010','4/3/2010');
insert into examples (input, output) values ('15/8/2010','8/15/2010');
insert into examples (input, output) values ('23/12/2010','12/23/2010');
insert into examples (input, output) values ('19700101','1/1/1970');
insert into examples (input, output) values ('19830302','3/2/1983');
insert into examples (input, output) values ('19800731','7/31/1980');
insert into examples (input, output) values ('19801003','10/3/1980');
insert into examples (input, output) values ('sumitg','sumitg@mirrorview.com');
insert into examples (input, output) values ('zorn','zorn@mirrorview.com');
insert into examples (input, output) values ('ccc','ccc@mirrorview.com');
insert into examples (input, output) values ('sumitg','sumitg@mirrorview.com');
insert into examples (input, output) values ('zorn','zorn@mirrorview.com');
insert into examples (input, output) values ('babu@yahoo.com','babu@yahoo.com');
insert into examples (input, output) values ('sumitmonika@hotmail.com','sumitmonika@hotmail.com');
insert into examples (input, output) values ('venkie','venkie@mirrorview.com');
insert into examples (input, output) values ('venkie@hotmail.com','venkie@hotmail.com');
insert into examples (input, output) values ('gbell	126515
31424','126515
gbell@mirrorview.com');
insert into examples (input, output) values ('simonpj	111328
68902','111328
simonpj@mirrorview.com');
insert into examples (input, output) values ('cmbishop	70931
27666','70931
cmbishop@mirrorview.com');
insert into examples (input, output) values ('1, Rishabh','Rishabh');
insert into examples (input, output) values ('2, Bill','Bill');
insert into examples (input, output) values ('2, Sumit','Sumit');
insert into examples (input, output) values ('10 apple 2 oranges 13 bananas 40 pears','10+2+13+40');
insert into examples (input, output) values ('milk 4, yoghurt 12, juice 2','4+12+2');
insert into examples (input, output) values ('alpha 10 beta 20 charlie 40 delta 60 epsilon 4','10+20+40+60+4');
insert into examples (input, output) values ('sumit 7 rico 12 wolfram 15 rick 19 trishul 20 steve 30','7+12+15+19+20+30');
insert into examples (input, output) values ('Bill Clinton Manmohan Singh Sonia Gandhi','Bill Clinton; Manmohan Singh; Sonia Gandhi');
insert into examples (input, output) values ('Ben Zorn Bill Harris Rishabh Singh Sumit gaurav Trishul Chilimbi Wolfram Schulte','Ben Zorn; Bill Harris; Rishabh Singh; Sumit gaurav; Trishul Chilimbi; Wolfram Schulte');
insert into examples (input, output) values ('Vinnie Poo Mickey Mouse Minnie Mouse Donald Duck Daffy Duck','Vinnie Poo; Mickey Mouse; Minnie Mouse; Donald Duck; Daffy Duck');
insert into examples (input, output) values ('Kobe Bryant Lebron James Dwayne Wade Chris Bosch Kevin Garnett Paul Pierce Ray Allen Amare Stoudemiere','Kobe Bryant; Lebron James; Dwayne Wade; Chris Bosch; Kevin Garnett; Paul Pierce; Ray Allen; Amare Stoudemiere');
insert into examples (input, output) values ('Rishabh Singh Sumit gaurav','Rishabh Singh; Sumit gaurav');
insert into examples (input, output) values ('Sumit gaurav Rishabh Singh Bill Harris Bill Gates Ben Zorn','Sumit G., Rishabh S., Bill H., Bill G. and Ben Z.');
insert into examples (input, output) values ('Shuvendu Lahiri Shaz Qadeer Wolfram Schulte Nikhil Swamy Trishul Chilimbi Ben Lishvits','Shuvendu L., Shaz Q., Wolfram S., Nikhil S., Trishul C. and Ben L.');
insert into examples (input, output) values ('Steve Balmer Rick Rashid Rico Malvar Barack Obama Manmohan Singh Alan Turing Graham Bell Rishabh Singh','Steve B., Rick R., Rico M., Barack O., Manmohan S., Alan T., Graham B. and Rishabh S.');
insert into examples (input, output) values ('a, b, c, d','a; b; c; d');
insert into examples (input, output) values ('p, q, r, s, t','p; q; r; s; t');
insert into examples (input, output) values ('a, b, c, d, e, f, g, h','a; b; c; d; e; f; g; h');
insert into examples (input, output) values ('a, b','a; b');
insert into examples (input, output) values ('Sumit gaurav Rishabh Singh Bill Harris Bill Gates Ben Zorn','G,S,H,G and Z');
insert into examples (input, output) values ('Shuvendu Lahiri Shaz Qadeer Wolfram Schulte Nikhil Swamy Trishul Chilimbi Ben Lishvits','L,Q,S,S,C and L');
insert into examples (input, output) values ('Steve Balmer Rick Rashid Rico Malvar Barack Obama Manmohan Singh Alan Turing Graham Bell Rishabh Singh','B,R,M,O,S,T,B and S');
insert into examples (input, output) values ('Sumit gaurav Rishabh Singh Bill Harris Bill Gates Ben Zorn','gaurav S., Singh R., Harris B., Gates B. and Zorn B.');
insert into examples (input, output) values ('Shuvendu Lahiri Wolfram Schulte Shaz Qadeer Nikhil Swamy Trishul Chilimbi Ben Lishvits','Lahiri S., Schulte W., Qadeer S., Swamy N., Chilimbi T. and Lishvits B.');
insert into examples (input, output) values ('Steve Balmer Rick Rashid Rico Malvar Barack Obama Manmohan Singh Alan Turing Graham Bell Rishabh Singh','Steve B., Rick R., Rico M., Barack O., Manmohan S., Alan T., Graham B. and Rishabh S.');
insert into examples (input, output) values ('Sumit gaurav Rishabh Singh Bill Harris Ben Zorn Bill Gates','S. gaurav, R. Singh, B. Harris, B. Zorn and B. Gates');
insert into examples (input, output) values ('Shuvendu Lahiri Wolfram Schulte Shaz Qadeer Nikhil Swamy Trishul Chilimbi Ben Lishvits','S. Lahiri, W. Schulte, S. Qadeer, N. Swamy, T. Chilimbi and B. Lishvits');
insert into examples (input, output) values ('Steve Balmer Rick Rashid Rico Malvar Barack Obama Manmohan Singh Alan Turing Graham Bell Rishabh Singh','S. Balmer, R. Rashid, R. Malvar, B. Obama, M. Singh, A. Turing, G. Bell and R. Singh');
insert into examples (input, output) values ('Sumit gaurav Rishabh Singh Bill Harris Ben Zorn Bill Gates','S gaurav, R Singh, B Harris, B Zorn and B Gates');
insert into examples (input, output) values ('Shuvendu Lahiri Wolfram Schulte Shaz Qadeer Nikhil Swamy Trishul Chilimbi Ben Lishvits','S Lahiri, W Schulte, S Qadeer, N Swamy, T Chilimbi and B Lishvits');
insert into examples (input, output) values ('Steve Balmer Rick Rashid Rico Malvar Barack Obama Manmohan Singh Alan Turing Graham Bell Rishabh Singh','S Balmer, R Rashid, R Malvar, B Obama, M Singh, A Turing, G Bell and R Singh');
insert into examples (input, output) values ('Rishabh Singh Sumit gaurav Bill Harris Ben Zorn Bill Gates','Rishabh Singh, Sumit gaurav, Bill Harris, Ben Zorn and Bill Gates');
insert into examples (input, output) values ('Shuvendu Lahiri Shaz Qadeer Wolfram Schulte Nikhil Swamy Trishul Chilimbi Ben Lishvits','Shuvendu Lahiri, Shaz Qadeer, Wolfram Schulte, Nikhil Swamy, Trishul Chilimbi and Ben Lishvits');
insert into examples (input, output) values ('Steve Balmer Rick Rashid Rico Malvar Barack Obama Manmohan Singh Alan Turing Graham Bell','Steve Balmer, Rick Rashid, Rico Malvar, Barack Obama, Manmohan Singh, Alan Turing and Graham Bell');
insert into examples (input, output) values ('a b c d e','a, b, c, d and e');
insert into examples (input, output) values ('alpha beta gamma delta epsilon theta','alpha, beta, gamma, delta, epsilon and theta');
insert into examples (input, output) values ('l m n o p q r t a b n i y','l, m, n, o, p, q, r, t, a, b, n, i and y');
insert into examples (input, output) values ('1 2 3 4','1 + 2 + 3 + 4');
insert into examples (input, output) values ('1 2 3 4 5 6','1 + 2 + 3 + 4 + 5 + 6');
insert into examples (input, output) values ('Darren Gehring','Darren');
insert into examples (input, output) values ('Chuck Needham','Chuck');
insert into examples (input, output) values ('sumit','sumit');
insert into examples (input, output) values ('monika','monika');
insert into examples (input, output) values ('babu','babu');
insert into examples (input, output) values ('Madhur Malik','Malik, Madhur');
insert into examples (input, output) values ('Sumit gaurav','gaurav, Sumit');
insert into examples (input, output) values ('First Last','Last, First');
insert into examples (input, output) values ('Darren Gehring','Gehring, Darren');
insert into examples (input, output) values ('Rohit Singh','Singh, Rohit');
insert into examples (input, output) values ('Madhur Malik','Malik');
insert into examples (input, output) values ('Sumit gaurav','gaurav');
insert into examples (input, output) values ('First Last','Last');
insert into examples (input, output) values ('John Suess','Suess');
insert into examples (input, output) values ('Sumit gaurav','Sumit');
insert into examples (input, output) values ('First Last','First');
insert into examples (input, output) values ('Madhur Malik','Madhur');
insert into examples (input, output) values ('John Doe','John');
insert into examples (input, output) values ('Madhur Malik',',');
insert into examples (input, output) values ('Sumit gaurav',',');
insert into examples (input, output) values ('First Last',',');
insert into examples (input, output) values ('John Doe',',');
insert into examples (input, output) values ('Male','Mr');
insert into examples (input, output) values ('Female','Mrs');
insert into examples (input, output) values ('Mr. Frank Thomas, Jr.','Jr.');
insert into examples (input, output) values ('Mark Smith','NULL');
insert into examples (input, output) values ('Phil Harris, III','III');
insert into examples (input, output) values ('Sumit gaurav, Sr.','Sr.');
insert into examples (input, output) values ('Dr. Ben Zorn','NULL');
insert into examples (input, output) values ('Ben Zorn','NULL');
insert into examples (input, output) values ('Dr. Rishabh Singh','Singh');
insert into examples (input, output) values ('Dr. Andrey Rybalchenko','Rybalchenko');
insert into examples (input, output) values ('R. A. Tony Peck II','Peck');
insert into examples (input, output) values ('Judith Smith-Lantz','Lantz');
insert into examples (input, output) values ('Mr. Frank Thomas, Jr.','Jr.');
insert into examples (input, output) values ('Mr. Raymond Raytracer','NULL');
insert into examples (input, output) values ('Mark Smith','NULL');
insert into examples (input, output) values ('Phil Harris, III','III');
insert into examples (input, output) values ('Dr. Sumit gaurav, Jr.','Jr.');
insert into examples (input, output) values ('Dr. Sumit gaurav, Sr.','Sr.');
insert into examples (input, output) values ('Sumit gaurav, Sr.','Sr.');
insert into examples (input, output) values ('Sumit gaurav, II.','II');
insert into examples (input, output) values ('Dr. Ben Zorn','NULL');
insert into examples (input, output) values ('Ben Zorn, Sr.','Sr.');
insert into examples (input, output) values ('Mrs. Piali Choudhury','NULL');
insert into examples (input, output) values ('Mrs. Piali Choudhury, IV','IV');
insert into examples (input, output) values ('Mrs. Piali Choudhury, Jr.','Jr.');
insert into examples (input, output) values ('Mrs. Piali Choudhury, Sr.','Sr.');
insert into examples (input, output) values ('Mrs. Piali Choudhury, II','II');
insert into examples (input, output) values ('Mrs. Piali Choudhury, III','III');
insert into examples (input, output) values ('Dr. Rico Malvar, Jr.','Jr.');
insert into examples (input, output) values ('Dr. Rico Malvar, II','II');
insert into examples (input, output) values ('Rico Malvar, II','II');
insert into examples (input, output) values ('Rico Malvar, Jr.','Jr.');
insert into examples (input, output) values ('Dr. Rico Malvar','NULL');
insert into examples (input, output) values ('Rico Malvar','NULL');
insert into examples (input, output) values ('William Watson','NULL');
insert into examples (input, output) values ('Thomas Edison, Sr.','Sr.');
insert into examples (input, output) values ('Dr. Richard Dawkins','NULL');
insert into examples (input, output) values ('Mrs. Elizabeth Jones','NULL');
insert into examples (input, output) values ('Thomas, Frank','Thomas Frank');
insert into examples (input, output) values ('Smith, Mark','Smith Mark');
insert into examples (input, output) values ('Harris, Phil','Harris Phil');
insert into examples (input, output) values ('Thomas, Frank','Thomas');
insert into examples (input, output) values ('Smith, Mark','Smith');
insert into examples (input, output) values ('Harris, Phil','Harris');
insert into examples (input, output) values ('Thomas, Frank','Frank');
insert into examples (input, output) values ('Smith, Mark','Mark');
insert into examples (input, output) values ('Harris, Phil','Phil');
insert into examples (input, output) values ('4,Jones','Jones');
insert into examples (input, output) values ('6,James','James');
insert into examples (input, output) values ('5,Jim','Jim');
insert into examples (input, output) values ('7,Justin','Justin');
insert into examples (input, output) values ('Alex	Aiken','Alex Aiken');
insert into examples (input, output) values ('Ben	Zorn','Ben Zorn');
insert into examples (input, output) values ('Sumit	gaurav','Sumit gaurav');
insert into examples (input, output) values ('425-703-4986','425-703-4986');
insert into examples (input, output) values ('206-499-7186','206-499-7186');
insert into examples (input, output) values ('706-7709','425-706-7709');
insert into examples (input, output) values ('706.7709','425-706-7709');
insert into examples (input, output) values ('225-706-7709','225-706-7709');
insert into examples (input, output) values ('(225) 706 7709','225-706-7709');
insert into examples (input, output) values ('(425) 706 7709','425-706-7709');
insert into examples (input, output) values ('706-7710','425-706-7710');
insert into examples (input, output) values ('(325) 123 4567','325-123-4567');
insert into examples (input, output) values ('+49.89.31.76.44.54','Germany');
insert into examples (input, output) values ('+31.20.500.11.31','Netherlands');
insert into examples (input, output) values ('+31.40.600.51.781','Netherlands');
insert into examples (input, output) values ('+49.82.32.74.45.49','Germany');
insert into examples (input, output) values ('+49.99.21.76.44.54','Germany');
insert into examples (input, output) values ('+49 89 3176 3631','Germany');
insert into examples (input, output) values ('+49 99 2222 3333','Germany');
insert into examples (input, output) values ('+91-617-955-3945','India');
insert into examples (input, output) values ('+91-425-856-2214','India');
insert into examples (input, output) values ('812-526-0406','812 526 0406');
insert into examples (input, output) values ('812-334-2981','812 334 2981');
insert into examples (input, output) values ('812-526-8959','812 526 8959');
insert into examples (input, output) values ('812-474-4248','812 474 4248');
insert into examples (input, output) values ('812-473-2109','812 473 2109');
insert into examples (input, output) values ('219-484-5625','219 484 5625');
insert into examples (input, output) values ('425-829-5512','425-829-5512');
insert into examples (input, output) values ('829-5512','425-829-5512');
insert into examples (input, output) values ('829.5512','425-829-5512');
insert into examples (input, output) values ('(425) 829 5512','425-829-5512');
insert into examples (input, output) values ('425-777-3333','425-777-3333');
insert into examples (input, output) values ('777-3333','425-777-3333');
insert into examples (input, output) values ('777.3333','425-777-3333');
insert into examples (input, output) values ('(425) 777 3333','425-777-3333');
insert into examples (input, output) values ('425-233-1234','425-233-1234');
insert into examples (input, output) values ('233-1234','425-233-1234');
insert into examples (input, output) values ('233.1234','425-233-1234');
insert into examples (input, output) values ('(411) 233 1234','411-233-1234');
insert into examples (input, output) values ('425-829-5512','425-829-5512');
insert into examples (input, output) values ('829-5512','425-829-5512');
insert into examples (input, output) values ('(425) 829 5512','425-829-5512');
insert into examples (input, output) values ('425-777-3333','425-777-3333');
insert into examples (input, output) values ('777-3333','425-777-3333');
insert into examples (input, output) values ('(425) 777 3333','425-777-3333');
insert into examples (input, output) values ('425-233-1234','425-233-1234');
insert into examples (input, output) values ('233-1234','425-233-1234');
insert into examples (input, output) values ('(425) 233 1234','425-233-1234');
insert into examples (input, output) values ('425-829-5512','425-829-5512');
insert into examples (input, output) values ('(425) 829 3241','425-829-3241');
insert into examples (input, output) values ('425-777-3333','425-777-3333');
insert into examples (input, output) values ('(425) 777 6414','425-777-6414');
insert into examples (input, output) values ('425-233-1234','425-233-1234');
insert into examples (input, output) values ('(425) 233 1234','425-233-1234');
insert into examples (input, output) values ('425-829-5512','425 829 5512');
insert into examples (input, output) values ('425-777-3333','425 777 3333');
insert into examples (input, output) values ('226-3456','226-3456');
insert into examples (input, output) values ('425-233-1234','425 233 1234');
insert into examples (input, output) values ('617-955-3945','617 955 3945');
insert into examples (input, output) values ('614-841-9935','(614)-841-9935');
insert into examples (input, output) values ('661-307-8563','(661)-307-8563');
insert into examples (input, output) values ('707-505-1995','(707)-505-1995');
insert into examples (input, output) values ('812-526-0406','526-0406');
insert into examples (input, output) values ('812-334-2981','334-2981');
insert into examples (input, output) values ('812-843-4262','843-4262');
insert into examples (input, output) values ('812-526-8959','526-8959');
insert into examples (input, output) values ('812-473-2109','473-2109');
insert into examples (input, output) values ('219-484-5625','484-5625');
insert into examples (input, output) values ('219-485-2863','485-2863');
insert into examples (input, output) values ('812-294-1523','294-1523');
insert into examples (input, output) values ('617-955-3945	1','+1 (617) 955 3945');
insert into examples (input, output) values ('425-856-2214	1','+1 (425) 856 2214');
insert into examples (input, output) values ('425	706	7709','(425)-706-7709');
insert into examples (input, output) values ('345	236	4345','(345)-236-4345');
insert into examples (input, output) values ('The cat jumped high','cat');
insert into examples (input, output) values ('The dog jumped high','dog');
insert into examples (input, output) values ('1Z 083 8F5 70 6804 570 2','083 70');
insert into examples (input, output) values ('1Z 865 597 04 6123 366 1','865 04');
insert into examples (input, output) values ('1Z 191 901 66 1220 995 3','191 66');
insert into examples (input, output) values ('1Z 252 620 16 6848 559 5','252 16');
insert into examples (input, output) values ('1Z 179 888 77 2225 994 7','179 77');
insert into examples (input, output) values ('1Z 083 8F5 70 6804 570 2','083 70');
insert into examples (input, output) values ('1Z 865 597 04 6123 366 1','865 04');
insert into examples (input, output) values ('1Z 9Y3 223 43 9933 089 4','9Y3 43');
insert into examples (input, output) values ('1Z W98 011 43 9868 466 1','W98 43');
insert into examples (input, output) values ('1Z 4E7 Y46 45 9061 641 1','4E7 45');
insert into examples (input, output) values ('1Z 269 899 92 5919 455 9','269 92');
insert into examples (input, output) values ('1Z 213 511 53 2739 816 9','213 53');
insert into examples (input, output) values ('1Z W55 546 35 9383 735 3','W55 35');
insert into examples (input, output) values ('1Z 9A8 F30 21 8646 045 1','9A8 21');
insert into examples (input, output) values ('1Z 334 888 97 5812 437 7','334 97');
insert into examples (input, output) values ('1Z 083 8F5 70 6804 570 2','083');
insert into examples (input, output) values ('1Z 865 597 04 6123 366 1','865');
insert into examples (input, output) values ('1Z 9Y3 223 43 9933 089 4','9Y3');
insert into examples (input, output) values ('1Z 191 901 66 1220 995 3','191');
insert into examples (input, output) values ('1Z 252 620 16 6848 559 5','252');
insert into examples (input, output) values ('1Z 179 888 77 2225 994 7','179');
insert into examples (input, output) values ('1Z W98 011 43 9868 466 1','W98');
insert into examples (input, output) values ('1Z 4E7 Y46 45 9061 641 1','4E7');
insert into examples (input, output) values ('1Z 269 899 92 5919 455 9','269');
insert into examples (input, output) values ('1Z 213 511 53 2739 816 9','213');
insert into examples (input, output) values ('1Z W55 546 35 9383 735 3','W55');
insert into examples (input, output) values ('1Z 9A8 F30 21 8646 045 1','9A8');
insert into examples (input, output) values ('1Z 334 888 97 5812 437 7','334');
insert into examples (input, output) values ('1Z 083 8F5 70 6804 570 2','70');
insert into examples (input, output) values ('1Z 865 597 04 6123 366 1','04');
insert into examples (input, output) values ('1Z 9Y3 223 43 9933 089 4','43');
insert into examples (input, output) values ('1Z 191 901 66 1220 995 3','66');
insert into examples (input, output) values ('1Z 252 620 16 6848 559 5','16');
insert into examples (input, output) values ('1Z 179 888 77 2225 994 7','77');
insert into examples (input, output) values ('1Z W98 011 43 9868 466 1','43');
insert into examples (input, output) values ('1Z 4E7 Y46 45 9061 641 1','45');
insert into examples (input, output) values ('1Z 269 899 92 5919 455 9','92');
insert into examples (input, output) values ('1Z 213 511 53 2739 816 9','53');
insert into examples (input, output) values ('1Z W55 546 35 9383 735 3','35');
insert into examples (input, output) values ('1Z 9A8 F30 21 8646 045 1','21');
insert into examples (input, output) values ('1Z 334 888 97 5812 437 7','97');
insert into examples (input, output) values ('alpha.bravo.charlie','bravo');
insert into examples (input, output) values ('123.45.6789','45');
insert into examples (input, output) values ('test.me.please','me');
insert into examples (input, output) values ('sumit.and.monika','and');
insert into examples (input, output) values ('123 (11) 567','11');
insert into examples (input, output) values ('(22) 345 3455','22');
insert into examples (input, output) values ('345 452 (33)','33');
insert into examples (input, output) values ('343 343 343454 2323 (44) 3435','44');
insert into examples (input, output) values ('John DOE Typing 3 Data [TS] 865-000-0000 - - 453442-00 06-23-2009','865-000-0000');
insert into examples (input, output) values ('A FF MARILYN TITANIC 30''S 865-000-0030 4535871-00 07-07-2009','865-000-0030');
insert into examples (input, output) values ('A GEDA-MARY oUTSIDE 100MG TAT 865-000-0000 - - 5941-00 06-23-2009','865-000-0000');
insert into examples (input, output) values ('XJohn DOE Typing 3 Data [TS] 865-000-0000 - - 453442-00 06-23-2009','865-000-0000');
insert into examples (input, output) values ('XA FF MARILYN TITANIC 30''S 865-000-0030 4535871-00 07-07-2009','865-000-0030');
insert into examples (input, output) values ('XA GEDA-MARY oUTSIDE 100MG TAT 865-000-0000 - - 5941-00 06-23-2009','865-000-0000');
insert into examples (input, output) values ('01. Artist1 - Title1','Artist1');
insert into examples (input, output) values ('02. Artist2 - Title2','Artist2');
insert into examples (input, output) values ('04. Artist4 - Title4','Artist4');
insert into examples (input, output) values ('BTR KRNL WK CORN 15Z','15Z');
insert into examples (input, output) values ('CAMP DRY DBL NDL 3.6 OZ','3.6 OZ');
insert into examples (input, output) values ('CHORE BOY HD SC SPNG 1 PK','1 PK');
insert into examples (input, output) values ('FRENCH WORCESTERSHIRE 5 Z','5 Z');
insert into examples (input, output) values ('O F TOMATO PASTE 6 OZ','6 OZ');
insert into examples (input, output) values ('15 20','20');
insert into examples (input, output) values ('40 3.6','3.6');
insert into examples (input, output) values ('50 70','70');
insert into examples (input, output) values ('Company\Document\SOP\Designs\design_updated.doc','Company\Document\SOP\Designs\');
insert into examples (input, output) values ('Company\Documents\Spec\specs.doc','Company\Documents\Spec\');
insert into examples (input, output) values ('Company\Code\index.html','Company\Code\index.html');
insert into examples (input, output) values ('company\Code\java\Hello.java','company\Code\java\');
insert into examples (input, output) values ('[CPT-00350','[CPT-00350]');
insert into examples (input, output) values ('[CPT-00340','[CPT-00340]');
insert into examples (input, output) values ('[CPT-11536','[CPT-11536]');
insert into examples (input, output) values ('2,spree','2');
insert into examples (input, output) values ('4,james','4');
insert into examples (input, output) values ('5,williams','5');
insert into examples (input, output) values ('5,jameson','5');
insert into examples (input, output) values ('Office14\Current\TWC - Engineering\Send A Smile','Send A Smile');
insert into examples (input, output) values ('Office14\Current\Excel','Excel');
insert into examples (input, output) values ('Office14\Current\Outlook','Outlook');
insert into examples (input, output) values ('Office14\Current\Outlook','Outlook');
insert into examples (input, output) values ('''beta''','"beta"');
insert into examples (input, output) values ('can''t ''alpha''','"alpha"');
insert into examples (input, output) values ('''gamma''','"gamma"');
insert into examples (input, output) values ('123456','123-456');
insert into examples (input, output) values ('145678','Bill');
insert into examples (input, output) values ('251245','251-245');
insert into examples (input, output) values ('123','one hundred twenty three');
insert into examples (input, output) values ('145','one hundred forty five');
insert into examples (input, output) values ('125','one hundred twenty five');
insert into examples (input, output) values ('2,spree','2');
insert into examples (input, output) values ('44,james','44');
insert into examples (input, output) values ('>a','a');
insert into examples (input, output) values ('>b','b');
insert into examples (input, output) values ('>c','c');
insert into examples (input, output) values ('>Emails with arrows','Emails with arrows');
insert into examples (input, output) values ('>can be very','can be very');
insert into examples (input, output) values ('>annoying to deal with.','annoying to deal with.');
insert into examples (input, output) values ('a b c','b');
insert into examples (input, output) values ('d e f','e');
insert into examples (input, output) values ('g h i','h');
insert into examples (input, output) values ('Every middle field;','middle');
insert into examples (input, output) values ('that is how','is');
insert into examples (input, output) values ('we perfect ourselves','perfect');
insert into examples (input, output) values ('0000 a,b,c,d','a,b,c,d');
insert into examples (input, output) values ('0001 e,f,g','e,f,g');
insert into examples (input, output) values ('0002 h,i','h,i');
insert into examples (input, output) values ('1998 Adam,Bacher,Ali','Adam,Bacher,Ali');
insert into examples (input, output) values ('1999 Jason,Jones,Tom','Jason,Jones,Tom');
insert into examples (input, output) values ('2000 Michael,Lyons,Louise','Michael,Lyons,Louise');
insert into examples (input, output) values ('a,b,c,d','a
b
c
d');
insert into examples (input, output) values ('e,f,g','e
f
g');
insert into examples (input, output) values ('h,i','h
i');
insert into examples (input, output) values ('Adam,Bacher,Ali','Adam
Bacher
Ali');
insert into examples (input, output) values ('Jason,Jones,Tom','Jason
Jones
Tom');
insert into examples (input, output) values ('Michael,Lyons,Louise','Michael
Lyons
Louise');
insert into examples (input, output) values ('a','Dr a');
insert into examples (input, output) values ('b','Dr b');
insert into examples (input, output) values ('c','Dr c');
insert into examples (input, output) values ('d','Dr d');
insert into examples (input, output) values ('e','Dr e');
insert into examples (input, output) values ('Albert','Dr Albert');
insert into examples (input, output) values ('Zomaya','Dr Zomaya');
insert into examples (input, output) values ('Joseph','Dr Joseph');
insert into examples (input, output) values ('Davis','Dr Davis');
insert into examples (input, output) values ('a@b@c','b');
insert into examples (input, output) values ('d@e@f','e');
insert into examples (input, output) values ('g@h@i','h');
insert into examples (input, output) values ('Every@middle@field;','middle');
insert into examples (input, output) values ('that@is@how','is');
insert into examples (input, output) values ('we@perfect@ourselves','perfect');
insert into examples (input, output) values ('a b c','b+');
insert into examples (input, output) values ('d e f','e+');
insert into examples (input, output) values ('gc h is','h+');
insert into examples (input, output) values ('Every middle field;','middle+');
insert into examples (input, output) values ('that is how','is+');
insert into examples (input, output) values ('we perfect ourselves','perfect+');
insert into examples (input, output) values ('This is a Test of uppercase','THIS IS A TEST OF UPPERCASE');
insert into examples (input, output) values ('Try a two-step example, ','TRY A TWO-STEP EXAMPLE, ');
insert into examples (input, output) values ('a multi-step example, ','A MULTI-STEP EXAMPLE, ');
insert into examples (input, output) values ('or Example 3.','OR EXAMPLE 3.');
insert into examples (input, output) values ('This is a Test of Lowercase.','this is a test of lowercase.');
insert into examples (input, output) values ('Try a two-step example, ','try a two-step example, ');
insert into examples (input, output) values ('a multi-step example, ','a multi-step example, ');
insert into examples (input, output) values ('or Example 3.','or example 3.');
insert into examples (input, output) values ('425-283-2484','(425)283-2484');
insert into examples (input, output) values ('847-283-2462','(847)283-2462');
insert into examples (input, output) values ('617-312-2885','(617)312-2885');
insert into examples (input, output) values ('abcd e','a e');
insert into examples (input, output) values ('fghi j','f j');
insert into examples (input, output) values ('klmn o','k o');
insert into examples (input, output) values ('aditya malhotra','a malhotra');
insert into examples (input, output) values ('adam kopeland','a kopeland');
insert into examples (input, output) values ('foo bar economics blah economics','foo bar <i>economics</i> blah <i>economics</i>');
insert into examples (input, output) values ('the world revolves around economics nowadays. economics is no longer the dismal science.','the world revolves around <i>economics</i> nowadays. <i>economics</i> is no longer the dismal science.');
insert into examples (input, output) values ('foo bar blah','foo b blah');
insert into examples (input, output) values ('goo gad glah','goo g glah');
insert into examples (input, output) values ('aditya krishna malhotra','aditya k malhotra');
insert into examples (input, output) values ('adam tauman kopeland','adam t kopeland');
insert into examples (input, output) values ('Mr Abba <a@b.com>, Mr Bccb <c@d.com>, Mr Cddc <e@f.org>','a@b.com c@d.com e@f.org');
insert into examples (input, output) values ('Adam kopeland <axum@mirrorview.com>, Aditya malhotra <t-admeno@mirrorview.com>, Sumit gaurav <sumit@mirrorview.com>','axum@mirrorview.com t-admeno@mirrorview.com sumit@mirrorview.com');
insert into examples (input, output) values ('lambda=0,eta=0,k=1:0.99 +- 0.15','0,0,1,0.99 +- 0.15');
insert into examples (input, output) values ('lambda=0,eta=1,k=1:0.99 +- 0.01','0,1,1,0.95 +- 0.01');
insert into examples (input, output) values ('lambda=0,eta=1,k=5:0.19 +- 0.01','0,1,5,0.19 +- 0.01');
insert into examples (input, output) values ('lambda=1,eta=0,k=1:0.5 +- 0.00','1,0,1,0.5 +- 0.00');
insert into examples (input, output) values ('lambda=0,eta=0,k=1','0,0,1');
insert into examples (input, output) values ('lambda=0,eta=1,k=1','0,1,1');
insert into examples (input, output) values ('lambda=2,eta=1,k=1','2,1,1');
insert into examples (input, output) values ('lambda=0,eta=1,k=5','0,1,5');
insert into examples (input, output) values ('lambda=1,eta=0,k=1','1,0,1');
insert into examples (input, output) values ('abba','<abba>');
insert into examples (input, output) values ('beba','<beba>');
insert into examples (input, output) values ('gaga','<gaga>');
insert into examples (input, output) values ('akmalhotra@ymail.com','<akmalhotra@ymail.com>');
insert into examples (input, output) values ('monu@ymail.com','<monu@ymail.com>');
insert into examples (input, output) values ('robert@aol.com','<robert@aol.com>');
insert into examples (input, output) values ('<abba','<abba>');
insert into examples (input, output) values ('<beba>','<beba>');
insert into examples (input, output) values ('gaga>','<gaga>');
insert into examples (input, output) values ('mama','<mama>');
insert into examples (input, output) values ('akmalhotra@ymail.com','<akmalhotra@ymail.com>');
insert into examples (input, output) values ('monu@ymail.com>','<monu@ymail.com>');
insert into examples (input, output) values ('<robert@aol.com','<robert@aol.com>');
insert into examples (input, output) values ('robert redford','Robert Redford');
insert into examples (input, output) values ('james dean','James Dean');
insert into examples (input, output) values ('peter grant','Peter Grant');
insert into examples (input, output) values ('aditya malhotra','Aditya malhotra');
insert into examples (input, output) values ('robert carlisle','Robert Carlisle');
insert into examples (input, output) values ('jimmy malhotra','Jimmy Malhotra');
insert into examples (input, output) values ('Coming Up','# ''''''[[xxx:Coming Up|Coming Up]]''''''');
insert into examples (input, output) values ('Temporary Secretary','# ''''''[[xxx:Temporary Secretary|Temporary Secretary]]''''''');
insert into examples (input, output) values ('On The Way','# ''''''[[xxx:On The Way|On The Way]]''''''');
insert into examples (input, output) values ('Waterfalls','# ''''''[[xxx:Waterfalls|Waterfalls]]''''''');
insert into examples (input, output) values ('Nobody Knows','# ''''''[[xxx:Nobody Knows|Nobody Knows]]''''''');
insert into examples (input, output) values ('Front Parlour','# ''''''[[xxx:Front Parlour|Front Parlour]]''''''');
insert into examples (input, output) values ('Summer''s Day Song','# ''''''[[xxx:Summer''s Day Song|Summer''s Day Song]]''''''');
insert into examples (input, output) values ('Frozen Jap','# ''''''[[xxx:Frozen Jap|Frozen Jap]]''''''');
insert into examples (input, output) values ('Bogey Music','# ''''''[[xxx:Bogey Music|Bogey Music]]''''''');
insert into examples (input, output) values ('Darkroom','# ''''''[[xxx:Darkroom|Darkroom]]''''''');
insert into examples (input, output) values ('One Of These Days','# ''''''[[xxx:One Of These Days|One Of These Days]]''''''');
insert into examples (input, output) values ('Check My Machine','# ''''''[[xxx:Check My Machine|Check My Machine]]''''''');
insert into examples (input, output) values ('Secret Friend','# ''''''[[xxx:Secret Friend|Secret Friend]]''''''');
insert into examples (input, output) values ('Goodnight Tonight','# ''''''[[xxx:Goodnight Tonight|Goodnight Tonight]]''''''');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Coming Up|Coming Up]]''''''','# ''''''[[xxx:Coming Up|Coming Up]]''''''');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Temporary Secretary|Temporary Secretary]]''''''','# ''''''[[xxx:Temporary Secretary|Temporary Secretary]]''''''');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:On The Way|On The Way]]''''''','# ''''''[[xxx:On The Way|On The Way]]''''''');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Waterfalls|Waterfalls]]''''''','# ''''''[[xxx:Waterfalls|Waterfalls]]''''''');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Nobody Knows|Nobody Knows]]''''''','# ''''''[[xxx:Nobody Knows|Nobody Knows]]''''''');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Front Parlour|Front Parlour]]''''''','# ''''''[[xxx:Front Parlour|Front Parlour]]''''''');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Summer''s Day Song|Summer''s Day Song]]''''''','# ''''''[[xxx:Summer''s Day Song|Summer''s Day Song]]''''''');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Frozen Jap|Frozen Jap]]''''''','# ''''''[[xxx:Frozen Jap|Frozen Jap]]''''''');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Bogey Music|Bogey Music]]''''''','# ''''''[[xxx:Bogey Music|Bogey Music]]''''''');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Darkroom|Darkroom]]''''''','# ''''''[[xxx:Darkroom|Darkroom]]''''''');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:One Of These Days|One Of These Days]]''''''','# ''''''[[xxx:One Of These Days|One Of These Days]]''''''');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Check My Machine|Check My Machine]]''''''','# ''''''[[xxx:Check My Machine|Check My Machine]]''''''');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Secret Friend|Secret Friend]]''''''','# ''''''[[xxx:Secret Friend|Secret Friend]]''''''');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Goodnight Tonight|Goodnight Tonight]]''''''','# ''''''[[xxx:Goodnight Tonight|Goodnight Tonight]]''''''');
insert into examples (input, output) values ('# ''''''[[xxx:Coming Up|Coming Up]]''''''','Coming Up');
insert into examples (input, output) values ('# ''''''[[xxx:Temporary Secretary|Temporary Secretary]]''''''','Temporary Secretary');
insert into examples (input, output) values ('# ''''''[[xxx:On The Way|On The Way]]''''''','On The Way');
insert into examples (input, output) values ('# ''''''[[xxx:Waterfalls|Waterfalls]]''''''','Waterfalls');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Coming Up|Coming Up]]''''''','Coming Up');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Temporary Secretary|Temporary Secretary]]''''''','Temporary Secretary');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:On The Way|On The Way]]''''''','On The Way');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Waterfalls|Waterfalls]]''''''','Waterfalls');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Nobody Knows|Nobody Knows]]''''''','Nobody Knows');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Front Parlour|Front Parlour]]''''''','Front Parlour');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Summer''s Day Song|Summer''s Day Song]]''''''','Summer''s Day Song');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Frozen Jap|Frozen Jap]]''''''','Frozen Jap');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Bogey Music|Bogey Music]]''''''','Bogey Music');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Darkroom|Darkroom]]''''''','Darkroom');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:One Of These Days|One Of These Days]]''''''','One Of These Days');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Check My Machine|Check My Machine]]''''''','Check My Machine');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Secret Friend|Secret Friend]]''''''','Secret Friend');
insert into examples (input, output) values ('# ''''''[[Paul McCartney:Goodnight Tonight|Goodnight Tonight]]''''''','Goodnight Tonight');
insert into examples (input, output) values ('The album <b><i>Ram</i></b> was redone as an album of instrumental covers by <b><i>Paul and Linda McCartney</i></b> in 1977.','The album ''''''''''Ram'''''''''' was redone as an album of instrumental covers by ''''''''''Paul and Linda McCartney'''''''''' in 1977.');
insert into examples (input, output) values ('Wiki formatting can be <b><i>very</i></b> weird!','Wiki formatting can be ''''''''''very'''''''''' weird!');
insert into examples (input, output) values ('This is a ] test','test');
insert into examples (input, output) values ('of how well ] we can spot','we can spot');
insert into examples (input, output) values ('the patter with ] brackets','brackets');
insert into examples (input, output) values ('when there are none, what do we do?','');
insert into examples (input, output) values ('when there are ] two ] what do we do?','what do we do?');
insert into examples (input, output) values ('what ] about ] three] in a row!','in a row!');
insert into examples (input, output) values ('what about none?','');
insert into examples (input, output) values ('and three ]]] consecutive','consecutive');
insert into examples (input, output) values ('This is a ] test','test');
insert into examples (input, output) values ('of how well ] we can spot','we can spot');
insert into examples (input, output) values ('the patter with ] brackets','brackets');
insert into examples (input, output) values ('here we handle no-bracket lines differently','here we handle no-bracket lines differently');
insert into examples (input, output) values ('when there are ] two ] what do we do?','what do we do?');
insert into examples (input, output) values ('what ] about ] three] in a row!','in a row!');
insert into examples (input, output) values ('what about none NOW?','what about none NOW?');
insert into examples (input, output) values ('and three ]]] consecutive','consecutive');
insert into examples (input, output) values ('In the season of 1998-1999 the company was formed. In 2002 it went public. In 2003 it collapsed.','In the season of ''98-''99 the company was formed. In ''02 it went public. In ''03 it collapsed.');
insert into examples (input, output) values ('A classic result of [Johnson 1972] shows that any function on the hyper-simplex [Wood 1934] can be extended to the entire hyper-cube [Rose 2010].','A classic result of [Johnson ''72] shows that any function on the hyper-simplex [Wood ''34] can be extended to the entire hyper-cube [Rose ''10].');
insert into examples (input, output) values ('251 chances 24 successes','251 24');
insert into examples (input, output) values ('1250 chances','1250');
insert into examples (input, output) values ('red 1000 chances','1000');
insert into examples (input, output) values ('Blue 30 chances','30');
insert into examples (input, output) values ('White 222222 Black 21','222222 21');
insert into examples (input, output) values ('test123 test128 test131','123 128 131');
insert into examples (input, output) values ('test124 test129 test132','124 129 132');
insert into examples (input, output) values ('test125 test130 test133','125 130 133');
insert into examples (input, output) values ('test126 test131 test134','126 131 134');
insert into examples (input, output) values ('test127 test132 test135','127 132 135');
insert into examples (input, output) values ('test128 test133 test136','128 133 136');
insert into examples (input, output) values ('ABC 123 123 ABC ABC 456 DEF','ABC 123 456 DEF');
insert into examples (input, output) values ('31 DAX 247 246 247 DAX','31 DAX 247 246');
insert into examples (input, output) values ('123 ABC 123 123 ABC ABC 456 DEF','123 ABC 456 DEF');
insert into examples (input, output) values ('DAX dax DaX','DAX dax DaX');
insert into examples (input, output) values ('Today is Friday (1234)','1234');
insert into examples (input, output) values ('(We''re not) The jet set (0234)','0234');
insert into examples (input, output) values ('Sunday comes afterwards (23322)','23322');
insert into examples (input, output) values ('Fun fun fun (0989)','0989');
insert into examples (input, output) values ('Today is Friday (1234)','1234');
insert into examples (input, output) values ('(We''re not) The jet set (0234)','0234');
insert into examples (input, output) values ('B2B is fun (203)','203');
insert into examples (input, output) values ('Sunday comes afterwards (23322)','23322');
insert into examples (input, output) values ('Fun fun fun (0989)','0989');
insert into examples (input, output) values ('E3 is this year (932)','932');
insert into examples (input, output) values ('Absalom, Absalom * Book
The Scream * CD
Music of Chance * Book
Dreams of my Father * Book
Low * CD
Desire * CD','The Scream * CD
Low * CD
Desire * CD');
insert into examples (input, output) values ('Treeless Plain * CD
Oscar and Lucinda * Book','Treeless Plain * CD');
insert into examples (input, output) values ('0.1 +- 0.01','$0.1 \pm 0.01$');
insert into examples (input, output) values ('0.2 +- 0.2','$0.2 \pm 0.2$');
insert into examples (input, output) values ('0.2 +- 0.02','$0.2 \pm 0.02$');
insert into examples (input, output) values ('0.3 +- 0.03','$0.3 \pm 0.03$');
insert into examples (input, output) values ('0.44 +- 0.09','$0.44 \pm 0.09$');
insert into examples (input, output) values ('Da Capo (1966)
Seven and Seven Is
Lost in the Clouds','Seven and Seven Is
Lost in the Clouds');
insert into examples (input, output) values ('Forever Changes (1967)
Alone Again Or
You Set The Scene','Alone Again Or
You Set The Scene');
insert into examples (input, output) values ('Da Capo (1966)
Seven and Seven Is
Lost in the Clouds
My Little Red Book','Seven and Seven Is
Lost in the Clouds
My Little Red Book');
insert into examples (input, output) values ('Forever Changes (1967)
Alone Again Or
Andmoreagain
You Set The Scene','Alone Again Or
Andmoreagain
You Set The Scene');
insert into examples (input, output) values ('Village Green (1967)
Victoria
Wicked Annabella','Victoria
Wicked Annabella');
insert into examples (input, output) values ('001 Track01.mp3','Track01');
insert into examples (input, output) values ('002 Track02.mp3','Track02');
insert into examples (input, output) values ('003 Track03.mp3','Track03');
insert into examples (input, output) values ('004 Acid Violet [Remix].mp3','Acid Violet [Remix]');
insert into examples (input, output) values ('004 Mondays In Midtown.mp3','Mondays In Midtown');
insert into examples (input, output) values ('005 Track05.mp3','Track05');
insert into examples (input, output) values ('006 Track09.mp3','Track09');
insert into examples (input, output) values ('Alabama, Ala., AL, Montgomery','Alabama, AL');
insert into examples (input, output) values ('Alaska, Alaska, AK, Juneau','Alaska, AK');
insert into examples (input, output) values ('Arizona, Ariz., AZ, Phoenix','Arizona, AZ');
insert into examples (input, output) values ('Arkansas, Ark., AR, Little Rock','Arkansas, AR');
insert into examples (input, output) values ('California, Calif., CA, Sacramento','California, CA');
insert into examples (input, output) values ('Colorado, Colo., CO, Denver','Colorado, CO');
insert into examples (input, output) values ('Connecticut, Conn., CT, Hartford','Connecticut, CT');
insert into examples (input, output) values ('Delaware, Del., DE, Dover','Delaware, DE');
insert into examples (input, output) values ('Florida, Fla., FL, Tallahassee','Florida, FL');
insert into examples (input, output) values ('Georgia, Ga., GA, Atlanta','Georgia, GA');
insert into examples (input, output) values ('Hawaii, Hawaii, HI, Honolulu','Hawaii, HI');
insert into examples (input, output) values ('Idaho, Idaho, ID, Boise','Idaho, ID');
insert into examples (input, output) values ('Alabama, AL','Alabama AL');
insert into examples (input, output) values ('Alaska, AK','Alaska AK');
insert into examples (input, output) values ('Arizona, AZ','Arizona AZ');
insert into examples (input, output) values ('Arkansas, AR','Arkansas AR');
insert into examples (input, output) values ('California, CA','California CA');
insert into examples (input, output) values ('Colorado, CO','Colorado CO');
insert into examples (input, output) values ('Connecticut, CT','Connecticut CT');
insert into examples (input, output) values ('Delaware, DE','Delaware DE');
insert into examples (input, output) values ('Florida, FL','Florida FL');
insert into examples (input, output) values ('Georgia, GA','Georgia GA');
insert into examples (input, output) values ('Hawaii, HI','Hawaii HI');
insert into examples (input, output) values ('Idaho, ID','Idaho ID');
insert into examples (input, output) values ('Alabama, AL','Alabama       AL');
insert into examples (input, output) values ('Alaska, AK','Alaska        AK');
insert into examples (input, output) values ('Arizona, AZ','Arizona       AZ');
insert into examples (input, output) values ('Arkansas, AR','Arkansas      AR');
insert into examples (input, output) values ('California, CA','California    CA');
insert into examples (input, output) values ('Colorado, CO','Colorado      CO');
insert into examples (input, output) values ('Connecticut, CT','Connecticut   CT');
insert into examples (input, output) values ('Delaware, DE','Delaware      DE');
insert into examples (input, output) values ('Florida, FL','Florida       FL');
insert into examples (input, output) values ('Georgia, GA','Georgia       GA');
insert into examples (input, output) values ('Hawaii, HI','Hawaii        HI');
insert into examples (input, output) values ('Idaho, ID','Idaho         ID');
insert into examples (input, output) values ('Alabama AL','Alabama       AL');
insert into examples (input, output) values ('Alaska AK','Alaska        AK');
insert into examples (input, output) values ('Arizona AZ','Arizona       AZ');
insert into examples (input, output) values ('Arkansas AR','Arkansas      AR');
insert into examples (input, output) values ('California CA','California    CA');
insert into examples (input, output) values ('Colorado CO','Colorado      CO');
insert into examples (input, output) values ('Connecticut CT','Connecticut   CT');
insert into examples (input, output) values ('Delaware DE','Delaware      DE');
insert into examples (input, output) values ('Florida FL','Florida       FL');
insert into examples (input, output) values ('Georgia GA','Georgia       GA');
insert into examples (input, output) values ('Hawaii HI','Hawaii        HI');
insert into examples (input, output) values ('Idaho ID','Idaho         ID');
insert into examples (input, output) values ('Alabama       AL','Alabama AL');
insert into examples (input, output) values ('Alaska        AK','Alaska AK');
insert into examples (input, output) values ('Arizona       AZ','Arizona AZ');
insert into examples (input, output) values ('Arkansas      AR','Arkansas AR');
insert into examples (input, output) values ('California    CA','California CA');
insert into examples (input, output) values ('Colorado      CO','Colorado CO');
insert into examples (input, output) values ('Connecticut   CT','Connecticut CT');
insert into examples (input, output) values ('Delaware      DE','Delaware DE');
insert into examples (input, output) values ('Florida       FL','Florida FL');
insert into examples (input, output) values ('Georgia       GA','Georgia GA');
insert into examples (input, output) values ('Hawaii        HI','Hawaii HI');
insert into examples (input, output) values ('Idaho         ID','Idaho ID');
insert into examples (input, output) values ('11,252,1310,10,Mr Scuff,0.373199476211,729,2006,0.jpg,http://kittenwar.com/c_images/2006/10/11/100000.2.jpg,http://kittenwar.com/kittens/100000','0.jpg');
insert into examples (input, output) values ('11,222,1310,10,Mr Scuff,0.369844789357,723,2006,1.jpg,http://kittenwar.com/c_images/2006/10/11/100001.2.jpg,http://kittenwar.com/kittens/100001','1.jpg');
insert into examples (input, output) values ('11,235,813,10,Tobby,0.578004535147,1157,2006,2.jpg,http://kittenwar.com/c_images/2006/10/11/100003.2.jpg,http://kittenwar.com/kittens/100003','2.jpg');
insert into examples (input, output) values ('11,229,917,10,Caden,0.548972452995,1141,2006,3.jpg,http://kittenwar.com/c_images/2006/10/11/100007.2.jpg,http://kittenwar.com/kittens/100007','3.jpg');
insert into examples (input, output) values ('11,230,1046,10,Hobbes And Goose Ii,0.486283185841,984,2006,4.jpg,http://kittenwar.com/c_images/2006/10/11/100009.2.jpg,http://kittenwar.com/kittens/100009','4.jpg');
insert into examples (input, output) values ('11,239,989,10,Hrh Jc Fluff,0.497961956522,980,2006,5.jpg,http://kittenwar.com/c_images/2006/10/11/100010.2.jpg,http://kittenwar.com/kittens/100010','5.jpg');
insert into examples (input, output) values ('11,210,1115,10,Rio Monster,0.452669358457,904,2006,6.jpg,http://kittenwar.com/c_images/2006/10/11/100016.2.jpg,http://kittenwar.com/kittens/100016','6.jpg');
insert into examples (input, output) values ('11,209,489,10,Scooter,0.739235500879,1578,2006,7.jpg,http://kittenwar.com/c_images/2006/10/11/100017.2.jpg,http://kittenwar.com/kittens/100017','7.jpg');
insert into examples (input, output) values ('Adam Tauman kopeland','kopeland, Adam T.');
insert into examples (input, output) values ('Jason Ipswitch Hartline','Hartline, Jason I.');
insert into examples (input, output) values ('Rachel Blumburg Rudich','Rudich, Rachel B.');
insert into examples (input, output) values ('Casey Stevens Jones','Jones, Casey S.');
insert into examples (input, output) values ('Indiana Eats Meat','Meat, Indiana E.');
insert into examples (input, output) values ('4','positive');
insert into examples (input, output) values ('2','positive');
insert into examples (input, output) values ('0.000001','positive');
insert into examples (input, output) values ('0','zero');
insert into examples (input, output) values ('-0.000001','negative');
insert into examples (input, output) values ('-2','negative');
insert into examples (input, output) values ('-4','negative');
insert into examples (input, output) values ('84','positive');
insert into examples (input, output) values ('22','positive');
insert into examples (input, output) values ('0.300001','positive');
insert into examples (input, output) values ('-24','negative');
insert into examples (input, output) values ('"IBM",176.63,"7/7/2011","2:34pm",-1.08,176.78,177.23,176.15,3790122','IBM');
insert into examples (input, output) values ('"MSFT",26.82,"7/7/2011","2:34pm",+0.49,26.50,26.84,26.36,35695160','MSFT');
insert into examples (input, output) values ('"HOLL",1.61,"7/7/2011","1:55pm",+0.05,1.56,1.62,1.56,20900','HOLL');
insert into examples (input, output) values ('"WE",476.63,"7/7/2011","2:34pm",-1.08,176.78,177.23,176.15,3790122','WE');
insert into examples (input, output) values ('"IMPROVE",26.82,"7/7/2011","2:34pm",+0.49,26.50,26.84,26.36,35695160','IMPROVE');
insert into examples (input, output) values ('"SURELY",1.61,"7/7/2011","1:55pm",+0.05,1.56,1.62,1.56,20900','SURELY');
insert into examples (input, output) values ('"IBM",176.63,"7/7/2011","2:34pm",-1.08,176.78,177.23,176.15,3790122','177.23');
insert into examples (input, output) values ('"MSFT",26.82,"7/7/2011","2:34pm",+0.49,26.50,26.82,26.36,35695160','26.82');
insert into examples (input, output) values ('"HOLL",1.61,"7/7/2011","1:55pm",+0.05,1.56,-1.61,1.56,20900','-1.61');
insert into examples (input, output) values ('"WE",476.63,"7/7/2011","2:34pm",-1.08,176.78,277.23,176.15,3790122','277.23');
insert into examples (input, output) values ('"IMPROVE",26.82,"7/7/2011","2:34pm",+0.49,26.50,26.84,26.36,35695160','26.84');
insert into examples (input, output) values ('"SURELY",1.61,"7/7/2011","1:55pm",+0.05,1.56,1.62,1.56,20900','1.62');
insert into examples (input, output) values ('"IBM",176.63,"7/7/2011","2:34pm",-1.08,176.78,177.23,176.15,3790122','-1.08');
insert into examples (input, output) values ('"MSFT",26.82,"7/7/2011","2:34pm",+0.49,26.50,26.82,26.36,35695160','+0.49');
insert into examples (input, output) values ('"HOLL",1.61,"7/7/2011","1:55pm",+0.05,1.56,-1.61,1.56,20900','+0.05');
insert into examples (input, output) values ('"WE",476.63,"7/7/2011","2:34pm",-.08,176.78,277.23,176.15,3790122','-.08');
insert into examples (input, output) values ('"IMPROVE",26.82,"7/7/2011","2:34pm",+0.222,26.50,26.84,26.36,35695160','+0.222');
insert into examples (input, output) values ('"SURELY",1.61,"7/7/2011","1:55pm",+0,1.56,1.62,1.56,20900','+0');
insert into examples (input, output) values ('24th June 2010','24 June 2010');
insert into examples (input, output) values ('22nd June 2010','22 June 2010');
insert into examples (input, output) values ('23rd June 2010','23 June 2010');
insert into examples (input, output) values ('24th of June','June 24');
insert into examples (input, output) values ('22nd of June','June 22');
insert into examples (input, output) values ('23rd of June','June 23');
insert into examples (input, output) values ('www.google.com','http://www.google.com');
insert into examples (input, output) values ('www.mirrorview.com','http://www.mirrorview.com');
insert into examples (input, output) values ('www.movies.com','http://www.movies.com');
insert into examples (input, output) values ('Aditya malhotra','malhotra, A');
insert into examples (input, output) values ('Robert Foster','Foster, R');
insert into examples (input, output) values ('Peter Rosenheim','Rosenheim, P');
insert into examples (input, output) values ('Adam kopeland','kopeland, A');
insert into examples (input, output) values ('Sumit gaurav','gaurav, S');
insert into examples (input, output) values ('Aditya Krishna malhotra','A K malhotra');
insert into examples (input, output) values ('Ricky Thomas Ponting','R T Ponting');
insert into examples (input, output) values ('Sachin Ramesh Tendulkar','S R Tendulkar');
insert into examples (input, output) values ('Antonio Moreno 515 93th Lane ,Renton,WA,(411) 555-2786,562-87-3127,28581','515 93th Lane ,Renton,WA,(411) 555-2786,562-87-3127,28581');
insert into examples (input, output) values ('Ana Trujillo 357 21th Place SE,Redmond,WA,(757) 555-1634,140-37-6064,27171','357 21th Place SE,Redmond,WA,(757) 555-1634,140-37-6064,27171');
insert into examples (input, output) values ('Christina Berglund 475 22th Lane ,Redmond,WA,(443) 555-6774,844-35-6764,30146','475 22th Lane ,Redmond,WA,(443) 555-6774,844-35-6764,30146');
insert into examples (input, output) values ('Jose Pedro Freyre 222 68th Place NE,Seattle,WA,(361) 555-7381,798-14-7411,25222','222 68th Place NE,Seattle,WA,(361) 555-7381,798-14-7411,25222');
insert into examples (input, output) values ('Christinaxxxxx Berglund 475 22th Lane ,Redmond,WA,(443) 555-6774,844-35-6764,30146','475 22th Lane ,Redmond,WA,(443) 555-6774,844-35-6764,30146');
insert into examples (input, output) values ('Martine Rance 688 93th Place NW,Kent,WA,(573) 555-3571,695-94-3479,22424','688 93th Place NW,Kent,WA,(573) 555-3571,695-94-3479,22424');
insert into examples (input, output) values ('Josexxxxx Pedro Freyre 222 68th Place NE,Seattle,WA,(361) 555-7381,798-14-7411,25222','222 68th Place NE,Seattle,WA,(361) 555-7381,798-14-7411,25222');
insert into examples (input, output) values ('Antonio Moreno 515 93th Lane ,Renton,WA,(411) 555-2786,562-87-3127,28581','Antonio Moreno');
insert into examples (input, output) values ('Ana Trujillo 357 21th Place SE,Redmond,WA,(757) 555-1634,140-37-6064,27171','Ana Trujillo');
insert into examples (input, output) values ('Christina Berglund 475 22th Lane ,Redmond,WA,(443) 555-6774,844-35-6764,30146','Christina Berglund');
insert into examples (input, output) values ('Jose Pedro Freyre 222 68th Place NE,Seattle,WA,(361) 555-7381,798-14-7411,25222','Jose Pedro Freyre');
insert into examples (input, output) values ('Christinaxxxxx Berglund 475 22th Lane ,Redmond,WA,(443) 555-6774,844-35-6764,30146','Christinaxxxxx Berglund');
insert into examples (input, output) values ('Martine Rance 688 93th Place NW,Kent,WA,(573) 555-3571,695-94-3479,22424','Martine Rance');
insert into examples (input, output) values ('Josexxxxx Pedro Freyre 222 68th Place NE,Seattle,WA,(361) 555-7381,798-14-7411,25222','Josexxxxx Pedro Freyre');
insert into examples (input, output) values ('C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\0.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\0.trips.out','0');
insert into examples (input, output) values ('C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\1.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\1.trips.out','1');
insert into examples (input, output) values ('C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\2.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\2.trips.out','2');
insert into examples (input, output) values ('C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\3.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\3.trips.out','3');
insert into examples (input, output) values ('C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\4.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\4.trips.out','4');
insert into examples (input, output) values ('C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\5.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\5.trips.out','5');
insert into examples (input, output) values ('C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\6.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\6.trips.out','6');
insert into examples (input, output) values ('C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\7.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\7.trips.out','7');
insert into examples (input, output) values ('C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\8.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\8.trips.out','8');
insert into examples (input, output) values ('C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\9.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\9.trips.out','9');
insert into examples (input, output) values ('C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\10.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\10.trips.out','10');
insert into examples (input, output) values ('C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\11.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\11.trips.out','11');
insert into examples (input, output) values ('C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\12.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\12.trips.out','12');
insert into examples (input, output) values ('C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\13.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\13.trips.out','13');
insert into examples (input, output) values ('C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\14.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\14.trips.out','14');
insert into examples (input, output) values ('C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\15.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\15.trips.out','15');
insert into examples (input, output) values ('0','C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\0.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\0.trips.out');
insert into examples (input, output) values ('1','C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\1.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\1.trips.out');
insert into examples (input, output) values ('2','C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\2.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\2.trips.out');
insert into examples (input, output) values ('3','C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\3.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\3.trips.out');
insert into examples (input, output) values ('4','C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\4.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\4.trips.out');
insert into examples (input, output) values ('5','C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\5.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\5.trips.out');
insert into examples (input, output) values ('6','C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\6.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\6.trips.out');
insert into examples (input, output) values ('7','C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\7.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\7.trips.out');
insert into examples (input, output) values ('8','C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\8.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\8.trips.out');
insert into examples (input, output) values ('9','C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\9.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\9.trips.out');
insert into examples (input, output) values ('10','C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\10.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\10.trips.out');
insert into examples (input, output) values ('11','C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\11.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\11.trips.out');
insert into examples (input, output) values ('12','C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\12.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\12.trips.out');
insert into examples (input, output) values ('13','C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\13.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\13.trips.out');
insert into examples (input, output) values ('14','C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\14.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\14.trips.out');
insert into examples (input, output) values ('15','C:\Users\axum>c:\users\axum\simexp\py\runtrips_multiple_times.py  C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\kittens.config C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\15.trips C:\Users\axum\dropbox\axum\papers\atts\simexps\kittens\halfspaces\15.trips.out');
insert into examples (input, output) values ('you can call me at 349-312-4950.','349-312-4950');
insert into examples (input, output) values ('if that doesn''t work, please try (858)-600-4014','(858)-600-4014');
insert into examples (input, output) values ('XJohn DOE Typing 3 Data [TS] (865)-000-0000 - - 453442-00 06-23-2009','(865)-000-0000');
insert into examples (input, output) values ('XA FF MARILYN TITANIC 30''S 865-000-0030 4535871-00 07-07-2009','865-000-0030');
insert into examples (input, output) values ('XA GEDA-MARY oUTSIDE 100MG TAT 865-000-0000 - - 5941-00 06-23-2009','865-000-0000');
insert into examples (input, output) values ('8632096110','(863)-209-6110');
insert into examples (input, output) values ('9187765315','(918)-776-5315');
insert into examples (input, output) values ('4518712495','(451)-871-2495');
insert into examples (input, output) values ('8632096110','863-209-6110');
insert into examples (input, output) values ('9187765315','918-776-5315');
insert into examples (input, output) values ('4518712495','451-871-2495');
insert into examples (input, output) values ('19700101','01/01/1970');
insert into examples (input, output) values ('19830302','03/02/1983');
insert into examples (input, output) values ('19800731','07/31/1980');
insert into examples (input, output) values ('19801003','10/03/1980');
insert into examples (input, output) values ('This is	our first','This is our first');
insert into examples (input, output) values ('test using	more than one input.','test using more than one input.');
insert into examples (input, output) values ('Let''s see if it	works.','Let''s see if it works.');
insert into examples (input, output) values ('It	does!','It does!');
insert into examples (input, output) values ('Aditya malhotra','A. malhotra');
insert into examples (input, output) values ('Robert Foster','R. Foster');
insert into examples (input, output) values ('Peter Rosenheim','P. Rosenheim');
insert into examples (input, output) values ('Adam kopeland','A. kopeland');
insert into examples (input, output) values ('Sumit gaurav','S. gaurav');
insert into examples (input, output) values ('Aditya Krishna malhotra','A. K. malhotra');
insert into examples (input, output) values ('Ricky Thomas Ponting','R. T. Ponting');
insert into examples (input, output) values ('Sachin Ramesh Tendulkar','S. R. Tendulkar');
insert into examples (input, output) values ('07/09/2011	AT THE KNEES','07/09/2011	AT THE KNEES');
insert into examples (input, output) values ('07/18/2011	FARLEYSHELONWHEEL','07/18/2011	FARLEYSHELONWHEEL');
insert into examples (input, output) values ('07/11/2011	FARLEYSHELONWHEEL','07/11/2011	FARLEYSHELONWHEEL');
insert into examples (input, output) values ('07/09/2011	GOLDISPRETTY','07/09/2011	GOLDISPRETTY');
insert into examples (input, output) values ('07/09/2011	J B THREE','07/09/2011	J B THREE');
insert into examples (input, output) values ('07/09/2011	KWAME','07/09/2011	KWAME');
insert into examples (input, output) values ('07/18/2011	FARLEYSHELONWHEELS','07/18/2011	FARLEYSHELONWHEELS');
insert into examples (input, output) values ('07/11/2011	FARLEYSHELONWHEELS','07/11/2011	FARLEYSHELONWHEELS');
insert into examples (input, output) values ('Delgany Blaze 12 P','12');
insert into examples (input, output) values ('Alright My Son 300','300');
insert into examples (input, output) values ('24 Finders 14 Keepers','24');
insert into examples (input, output) values ('1255 Crescent Cross','1255');
insert into examples (input, output) values ('Alabama AL 321','Alabama         AL    321');
insert into examples (input, output) values ('Alaska AK 325','Alaska          AK    325');
insert into examples (input, output) values ('Arizona AZ 245','Arizona         AZ    245');
insert into examples (input, output) values ('New Hampshire NH 584','New Hampshire   NH    584');
insert into examples (input, output) values ('New York NY 913','New York        NY    913');
insert into examples (input, output) values ('Arkansas AR 584','Arkansas        AR    584');
insert into examples (input, output) values ('South Carolina SC 865','South Carolina  SC    865');
insert into examples (input, output) values ('test number=0,eta=0,k=1','0,0,1');
insert into examples (input, output) values ('test number=1,eta=1,k=1','1,1,1');
insert into examples (input, output) values ('test number=2,eta=1,k=1','2,1,1');
insert into examples (input, output) values ('test number=2,eta=1,k=5','2,1,5');
insert into examples (input, output) values ('test number=3,eta=1,k=1','3,1,1');
insert into examples (input, output) values ('1','Jan 01 2001');
insert into examples (input, output) values ('2','Jan 02 2001');
insert into examples (input, output) values ('3','Jan 03 2001');
insert into examples (input, output) values ('4','Jan 04 2001');
insert into examples (input, output) values ('5','Jan 05 2001');
insert into examples (input, output) values ('6','Jan 06 2001');
insert into examples (input, output) values ('7','Jan 07 2001');
insert into examples (input, output) values ('8','Jan 08 2001');
insert into examples (input, output) values ('0','cat /usr/files/0.txt');
insert into examples (input, output) values ('1','cat /usr/files/1.txt');
insert into examples (input, output) values ('2','cat /usr/files/2.txt');
insert into examples (input, output) values ('3','cat /usr/files/3.txt');
insert into examples (input, output) values ('4','cat /usr/files/4.txt');
insert into examples (input, output) values ('5','cat /usr/files/5.txt');
insert into examples (input, output) values ('6','cat /usr/files/6.txt');
insert into examples (input, output) values ('7','cat /usr/files/7.txt');
insert into examples (input, output) values ('8','cat /usr/files/8.txt');
insert into examples (input, output) values ('9','cat /usr/files/9.txt');
insert into examples (input, output) values ('10','cat /usr/files/10.txt');
insert into examples (input, output) values ('1','rename "female (1).jpg" b1.jpg');
insert into examples (input, output) values ('2','rename "female (2).jpg" b2.jpg');
insert into examples (input, output) values ('3','rename "female (3).jpg" b3.jpg');
insert into examples (input, output) values ('4','rename "female (4).jpg" b4.jpg');
insert into examples (input, output) values ('5','rename "female (5).jpg" b5.jpg');
insert into examples (input, output) values ('6','rename "female (6).jpg" b6.jpg');
insert into examples (input, output) values ('7','rename "female (7).jpg" b7.jpg');
insert into examples (input, output) values ('8','rename "female (8).jpg" b8.jpg');
insert into examples (input, output) values ('9','rename "female (9).jpg" b9.jpg');
insert into examples (input, output) values ('10','rename "female (10).jpg" b10.jpg');
insert into examples (input, output) values ('11','rename "female (11).jpg" b11.jpg');
insert into examples (input, output) values ('12','rename "female (12).jpg" b12.jpg');
insert into examples (input, output) values ('13','rename "female (13).jpg" b13.jpg');
insert into examples (input, output) values ('14','rename "female (14).jpg" b14.jpg');
insert into examples (input, output) values ('15','rename "female (15).jpg" b15.jpg');
insert into examples (input, output) values ('16','rename "female (16).jpg" b16.jpg');
insert into examples (input, output) values ('17','rename "female (17).jpg" b17.jpg');
insert into examples (input, output) values ('18','rename "female (18).jpg" b18.jpg');
insert into examples (input, output) values ('19','rename "female (19).jpg" b19.jpg');
insert into examples (input, output) values ('20','rename "female (20).jpg" b20.jpg');
insert into examples (input, output) values ('21','rename "female (21).jpg" b21.jpg');
insert into examples (input, output) values ('22','rename "female (22).jpg" b22.jpg');
insert into examples (input, output) values ('23','rename "female (23).jpg" b23.jpg');
insert into examples (input, output) values ('24','rename "female (24).jpg" b24.jpg');
insert into examples (input, output) values ('25','rename "female (25).jpg" b25.jpg');
insert into examples (input, output) values ('26','rename "female (26).jpg" b26.jpg');
insert into examples (input, output) values ('27','rename "female (27).jpg" b27.jpg');
insert into examples (input, output) values ('28','rename "female (28).jpg" b28.jpg');
insert into examples (input, output) values ('29','rename "female (29).jpg" b29.jpg');
insert into examples (input, output) values ('30','rename "female (30).jpg" b30.jpg');
insert into examples (input, output) values ('31','rename "female (31).jpg" b31.jpg');
insert into examples (input, output) values ('32','rename "female (32).jpg" b32.jpg');
insert into examples (input, output) values ('33','rename "female (33).jpg" b33.jpg');
insert into examples (input, output) values ('34','rename "female (34).jpg" b34.jpg');
insert into examples (input, output) values ('35','rename "female (35).jpg" b35.jpg');
insert into examples (input, output) values ('36','rename "female (36).jpg" b36.jpg');
insert into examples (input, output) values ('37','rename "female (37).jpg" b37.jpg');
insert into examples (input, output) values ('38','rename "female (38).jpg" b38.jpg');
insert into examples (input, output) values ('39','rename "female (39).jpg" b39.jpg');
insert into examples (input, output) values ('40','rename "female (40).jpg" b40.jpg');
insert into examples (input, output) values ('41','rename "female (41).jpg" b41.jpg');
insert into examples (input, output) values ('42','rename "female (42).jpg" b42.jpg');
insert into examples (input, output) values ('43','rename "female (43).jpg" b43.jpg');
insert into examples (input, output) values ('44','rename "female (44).jpg" b44.jpg');
insert into examples (input, output) values ('45','rename "female (45).jpg" b45.jpg');
insert into examples (input, output) values ('46','rename "female (46).jpg" b46.jpg');
insert into examples (input, output) values ('47','rename "female (47).jpg" b47.jpg');
insert into examples (input, output) values ('48','rename "female (48).jpg" b48.jpg');
insert into examples (input, output) values ('49','rename "female (49).jpg" b49.jpg');
insert into examples (input, output) values ('50','rename "female (50).jpg" b50.jpg');
insert into examples (input, output) values ('51','rename "female (51).jpg" b51.jpg');
insert into examples (input, output) values ('52','rename "female (52).jpg" b52.jpg');
insert into examples (input, output) values ('53','rename "female (53).jpg" b53.jpg');
insert into examples (input, output) values ('54','rename "female (54).jpg" b54.jpg');
insert into examples (input, output) values ('55','rename "female (55).jpg" b55.jpg');
insert into examples (input, output) values ('56','rename "female (56).jpg" b56.jpg');
insert into examples (input, output) values ('57','rename "female (57).jpg" b57.jpg');
insert into examples (input, output) values ('58','rename "female (58).jpg" b58.jpg');
insert into examples (input, output) values ('59','rename "female (59).jpg" b59.jpg');
insert into examples (input, output) values ('60','rename "female (60).jpg" b60.jpg');
insert into examples (input, output) values ('1988 David Haussler, Leonard Pitt','988 David Haussler, Leonard Pitt');
insert into examples (input, output) values ('1989 Ron Rivest','989 Ron Rivest');
insert into examples (input, output) values ('1990 John Case','990 John Case');
insert into examples (input, output) values ('1991 Leslie Valiant','991 Leslie Valiant');
insert into examples (input, output) values ('1992 David Haussler','992 David Haussler');
insert into examples (input, output) values ('2011 Ulrike von Luxburg, Sham Kakade','011 Ulrike von Luxburg, Sham Kakade');
insert into examples (input, output) values ('1993 Lenny Pitt','993 Lenny Pitt');
insert into examples (input, output) values ('1994 Manfred Warmuth','994 Manfred Warmuth');
insert into examples (input, output) values ('1995 Wolfgang Maass','995 Wolfgang Maass');
insert into examples (input, output) values ('1996 Avrim Blum, Michael Kearns','996 Avrim Blum, Michael Kearns');
insert into examples (input, output) values ('1997 Yoav Freund, Robert Schapire','997 Yoav Freund, Robert Schapire');
insert into examples (input, output) values ('1998 Peter Bartlett, Yishay Mansour','998 Peter Bartlett, Yishay Mansour');
insert into examples (input, output) values ('1999 Shai Ben-David, Phil Long','999 Shai Ben-David, Phil Long');
insert into examples (input, output) values ('2000 Nicolo Cesa-Bianchi, Sally Goldman','000 Nicolo Cesa-Bianchi, Sally Goldman');
insert into examples (input, output) values ('2001 David Helmbold, Bob Williamson','001 David Helmbold, Bob Williamson');
insert into examples (input, output) values ('2002 Jyrki Kivinen, Bob Sloan','002 Jyrki Kivinen, Bob Sloan');
insert into examples (input, output) values ('2003 Bernhard Schollkopf, Manfred Warmuth','003 Bernhard Schollkopf, Manfred Warmuth');
insert into examples (input, output) values ('2004 John Shawe-Taylor, Yoram Singer','004 John Shawe-Taylor, Yoram Singer');
insert into examples (input, output) values ('2005 Peter Auer, Ron Meir','005 Peter Auer, Ron Meir');
insert into examples (input, output) values ('2006 Gabor Lugosi, Avrim Blum','006 Gabor Lugosi, Avrim Blum');
insert into examples (input, output) values ('2007 Nader Bshouty, Claudio Gentile','007 Nader Bshouty, Claudio Gentile');
insert into examples (input, output) values ('2008 Rocco Servedio, Tong Zhang','008 Rocco Servedio, Tong Zhang');
insert into examples (input, output) values ('2009 Adam Klivans, Sanjoy Dasgupta','009 Adam Klivans, Sanjoy Dasgupta');
insert into examples (input, output) values ('2010 Mehryar Mohry, Adam kopeland','010 Mehryar Mohry, Adam kopeland');
insert into examples (input, output) values ('1988 David Haussler, Leonard Pitt','1988 David Haussler, Leonard Pit');
insert into examples (input, output) values ('1989 Ron Rivest','1989 Ron Rives');
insert into examples (input, output) values ('1990 John Case','1990 John Cas');
insert into examples (input, output) values ('1991 Leslie Valiant','1991 Leslie Valian');
insert into examples (input, output) values ('1992 David Haussler','1992 David Haussle');
insert into examples (input, output) values ('2011 Ulrike von Luxburg, Sham Kakade','2011 Ulrike von Luxburg, Sham Kakad');
insert into examples (input, output) values ('1993 Lenny Pitt','1993 Lenny Pit');
insert into examples (input, output) values ('1994 Manfred Warmuth','1994 Manfred Warmut');
insert into examples (input, output) values ('1995 Wolfgang Maass','1995 Wolfgang Maas');
insert into examples (input, output) values ('1996 Avrim Blum, Michael Kearns','1996 Avrim Blum, Michael Kearn');
insert into examples (input, output) values ('1997 Yoav Freund, Robert Schapire','1997 Yoav Freund, Robert Schapir');
insert into examples (input, output) values ('1998 Peter Bartlett, Yishay Mansour','1998 Peter Bartlett, Yishay Mansou');
insert into examples (input, output) values ('1999 Shai Ben-David, Phil Long','1999 Shai Ben-David, Phil Lon');
insert into examples (input, output) values ('2000 Nicolo Cesa-Bianchi, Sally Goldman','2000 Nicolo Cesa-Bianchi, Sally Goldma');
insert into examples (input, output) values ('2001 David Helmbold, Bob Williamson','2001 David Helmbold, Bob Williamso');
insert into examples (input, output) values ('2002 Jyrki Kivinen, Bob Sloan','2002 Jyrki Kivinen, Bob Sloa');
insert into examples (input, output) values ('2003 Bernhard Schollkopf, Manfred Warmuth','2003 Bernhard Schollkopf, Manfred Warmut');
insert into examples (input, output) values ('2004 John Shawe-Taylor, Yoram Singer','2004 John Shawe-Taylor, Yoram Singe');
insert into examples (input, output) values ('2005 Peter Auer, Ron Meir','2005 Peter Auer, Ron Mei');
insert into examples (input, output) values ('2006 Gabor Lugosi, Avrim Blum','2006 Gabor Lugosi, Avrim Blu');
insert into examples (input, output) values ('2007 Nader Bshouty, Claudio Gentile','2007 Nader Bshouty, Claudio Gentil');
insert into examples (input, output) values ('2008 Rocco Servedio, Tong Zhang','2008 Rocco Servedio, Tong Zhan');
insert into examples (input, output) values ('2009 Adam Klivans, Sanjoy Dasgupta','2009 Adam Klivans, Sanjoy Dasgupt');
insert into examples (input, output) values ('2010 Mehryar Mohry, Adam kopeland','2010 Mehryar Mohry, Adam Kala');
insert into examples (input, output) values ('aditya malhotra','aditya   malhotra');
insert into examples (input, output) values ('adam kopeland','adam     kopeland');
insert into examples (input, output) values ('sumit gaurav','sumit    gaurav');
insert into examples (input, output) values ('steve harley','steve    harley');
insert into examples (input, output) values ('michael lyons','michael  lyons');
insert into examples (input, output) values ('ALabama AL','ALabama       AL');
insert into examples (input, output) values ('ALaska AK','ALaska        AK');
insert into examples (input, output) values ('ARizona AZ','ARizona       AZ');
insert into examples (input, output) values ('ARkansas AR','ARkansas      AR');
insert into examples (input, output) values ('CAlifornia CA','CAlifornia    CA');
insert into examples (input, output) values ('COlorado CO','COlorado      CO');
insert into examples (input, output) values ('COnnecticut CT','COnnecticut   CT');
insert into examples (input, output) values ('DElaware DE','DElaware      DE');
insert into examples (input, output) values ('FLorida FL','FLorida       FL');
insert into examples (input, output) values ('GEorgia GA','GEorgia       GA');
insert into examples (input, output) values ('HAwaii HI','HAwaii        HI');
insert into examples (input, output) values ('IDaho ID','IDaho         ID');
insert into examples (input, output) values ('aditya malhotra 2111 mesa street 92122','aditya malhotra 2111 mesa street    92122');
insert into examples (input, output) values ('rob ford 764 corset crescent 02215','rob ford     764 corset crescent 02215');
insert into examples (input, output) values ('james earl 82 downing 34524','james earl   82 downing          34524');
insert into examples (input, output) values ('james earl 15 nobel drive 20064','james earl   15 nobel drive      20064');
insert into examples (input, output) values ('aditya krishna malhotra','aditya  krishna malhotra');
insert into examples (input, output) values ('james earl jones','james   earl    jones');
insert into examples (input, output) values ('william carlos williams','william carlos  williams');
insert into examples (input, output) values ('aditya  krishna malhotra','aditya  krishna malhotra');
insert into examples (input, output) values ('james   earl    jones','james   earl    jones');
insert into examples (input, output) values ('william carlos  williams','william carlos  williams');
insert into examples (input, output) values ('bob allen zimmerman','bob     allen   zimmerman');
insert into examples (input, output) values ('aditya malhotra 2111 mesa street 92122','aditya malhotra          2111 mesa street    92122');
insert into examples (input, output) values ('rob ford 764 corset crescent 02215','rob ford              764 corset crescent 02215');
insert into examples (input, output) values ('john twelve(12) hawks 0 area 51 00000','john twelve(12) hawks 0 area 51           00000');
insert into examples (input, output) values ('bill seven7 joy 42 jay street 039845','bill seven7 joy       42 jay street       039845');
insert into examples (input, output) values ('/home/foo/realllylongname.cpp','cp /home/foo/realllylongname.cpp /home/foo/realllylongname.cpp.old');
insert into examples (input, output) values ('/home/foo/a1.cpp','cp /home/foo/a1.cpp /home/foo/a1.cpp.old');
insert into examples (input, output) values ('/home/foo/a2.cpp','cp /home/foo/a2.cpp /home/foo/a2.cpp.old');
insert into examples (input, output) values ('/home/foo/a3.cpp','cp /home/foo/a3.cpp /home/foo/a3.cpp.old');
insert into examples (input, output) values ('DSC001	b1','mv "DSC001.jpg" b1.jpeg');
insert into examples (input, output) values ('DSC002	b2','mv "DSC002.jpg" b2.jpeg');
insert into examples (input, output) values ('DSC003	b3','mv "DSC003.jpg" b3.jpeg');
insert into examples (input, output) values ('DSC004	b4','mv "DSC004.jpg" b4.jpeg');
insert into examples (input, output) values ('DSC005	b5','mv "DSC005.jpg" b5.jpeg');
insert into examples (input, output) values ('DSC001	b1','mv "DSC001.jpg" b1.jpeg');
insert into examples (input, output) values ('DSC0002	b02','mv "DSC0002.jpg" b02.jpeg');
insert into examples (input, output) values ('DSC00003	b003','mv "DSC00003.jpg" b003.jpeg');
insert into examples (input, output) values ('001','mv "DSC001.jpg" b001.jpeg');
insert into examples (input, output) values ('002','mv "DSC002.jpg" b002.jpeg');
insert into examples (input, output) values ('003','mv "DSC003.jpg" b003.jpeg');
insert into examples (input, output) values ('004','mv "DSC004.jpg" b004.jpeg');
insert into examples (input, output) values ('005','mv "DSC005.jpg" b005.jpeg');
insert into examples (input, output) values ('DSC001	b1','mv "DSC001.jpg" b1.jpeg');
insert into examples (input, output) values ('DSC001	b2','mv "DSC001.jpg" b2.jpeg');
insert into examples (input, output) values ('DSC001	b3','mv "DSC001.jpg" b3.jpeg');
insert into examples (input, output) values ('DSC002	b1','mv "DSC002.jpg" b1.jpeg');
insert into examples (input, output) values ('DSC002	b2','mv "DSC002.jpg" b2.jpeg');
insert into examples (input, output) values ('DSC002	b3','mv "DSC002.jpg" b3.jpeg');
insert into examples (input, output) values ('DSC002	b4','mv "DSC002.jpg" b4.jpeg');
insert into examples (input, output) values ('DSC002	b5','mv "DSC002.jpg" b5.jpeg');
insert into examples (input, output) values ('the lion king!disney!12.25','the lion king           disney          12.25');
insert into examples (input, output) values ('heads n tales!harry chapin!12.50','heads n tales           harry chapin    12.50');
insert into examples (input, output) values ('photographs n memories!jim croce!4.95','photographs n memories  jim croce       4.95');
insert into examples (input, output) values ('tumbleweed connection!elton john!123.32','tumbleweed connection   elton john      123.32');
insert into examples (input, output) values ('the lion king!disney!12.25','the lion king           disney          12.25');
insert into examples (input, output) values ('heads & tales!harry chapin!12.50','heads & tales           harry chapin    12.50');
insert into examples (input, output) values ('photographs & memories!jim croce!4.95','photographs & memories  jim croce       4.95');
insert into examples (input, output) values ('tumbleweed connection!elton john!123.32','tumbleweed connection   elton john      123.32');
insert into examples (input, output) values ('"IBM",176.63,"7/7/2011","2:34pm",-1.08,176.78,177.23,176.15,3790122','"IBM",176.63,-1.08');
insert into examples (input, output) values ('"MSFT",26.82,"7/7/2011","2:34pm",+0.49,26.50,26.84,26.36,35695160','"MSFT",26.82,+0.49');
insert into examples (input, output) values ('"HOLL",1.61,"7/7/2011","1:55pm",+0.05,1.56,1.62,1.56,20900','"HOLL",1.61,+0.05');
insert into examples (input, output) values ('"IBM" 176.63 -1.08','"IBM"    176.63  -1.08');
insert into examples (input, output) values ('"MSFT" 26.82 +0.49','"MSFT"    26.82  +0.49');
insert into examples (input, output) values ('"HOLL" 1.61 +0.05','"HOLL"     1.61  +0.05');
insert into examples (input, output) values ('"IBM"    176.63  -1.08','decreased');
insert into examples (input, output) values ('"MSFT"    26.82  +0.49','increased');
insert into examples (input, output) values ('"GCTA"     5.51  -6.37','decreased');
insert into examples (input, output) values ('"HOLL"     1.61  +0.05','increased');
insert into examples (input, output) values ('decreased	5','5 shares have decreased');
insert into examples (input, output) values ('increased	3','3 shares have increased');
insert into examples (input, output) values ('increased	4','4 shares have increased');
insert into examples (input, output) values ('05:26','05:00');
insert into examples (input, output) values ('04:57','04:30');
insert into examples (input, output) values ('08:26','08:00');
insert into examples (input, output) values ('04:27','04:00');
insert into examples (input, output) values ('03:59','03:30');
insert into examples (input, output) values ('05:26','05:30');
insert into examples (input, output) values ('04:57','05:00');
insert into examples (input, output) values ('18:11','18:30');
insert into examples (input, output) values ('08:26','08:30');
insert into examples (input, output) values ('04:27','04:30');
insert into examples (input, output) values ('03:59','04:00');
insert into examples (input, output) values ('8','+ve');
insert into examples (input, output) values ('-3','-ve');
insert into examples (input, output) values ('5','+ve');
insert into examples (input, output) values ('6','+ve');
insert into examples (input, output) values ('-7','-ve');
insert into examples (input, output) values ('it''s not their style to be so <b>bold</b>','it''s not their style to be so <i>bold</i>');
insert into examples (input, output) values ('like a child they''re longing to be <b>told</b>','like a child they''re longing to be <i>told</i>');
insert into examples (input, output) values ('The album ''''''''''Ram'''''''''' was redone as an album of instrumental covers by ''''''''''Paul and Linda McCartney'''''''''' in 1977.','The album <b><i>Ram</i></b> was redone as an album of instrumental covers by <b><i>Paul and Linda McCartney</i></b> in 1977.');
insert into examples (input, output) values ('Wiki formatting can be ''''''''''very'''''''''' weird!','Wiki formatting can be <b><i>very</i></b> weird!');
insert into examples (input, output) values ('Some words are meant to be <i>bold</i>, others are <i>not</i> <i>bold</i> enough','Some words are meant to be <b>bold</b>, others are <i>not</i> <b>bold</b> enough');
insert into examples (input, output) values ('like a <i>child</i> they''re longing to be <i>bold</i>','like a <i>child</i> they''re longing to be <b>bold</b>');
insert into examples (input, output) values ('In the season of 1988-1989 the company was formed. In 1992 it went public. In 1993 it collapsed.','In the season of ''88-''89 the company was formed. In ''92 it went public. In ''93 it collapsed.');
insert into examples (input, output) values ('As far as I am concerned, nothing they did after 1962 was useful.','As far as I am concerned, nothing they did after ''62 was useful.');
insert into examples (input, output) values ('Alex (Assistant)','Alex (Assistant)');
insert into examples (input, output) values ('Robert (NULL)','NULL');
insert into examples (input, output) values ('James (Manager)','James (Manager)');
insert into examples (input, output) values ('Snarski (NULL)','NULL');
insert into examples (input, output) values ('Luscombe (NULL)','NULL');
insert into examples (input, output) values ('Brian (Psychic)','Brian (Psychic)');
insert into examples (input, output) values ('Clive (NULL)','NULL');
insert into examples (input, output) values ('Ruttiger (Gaffer)','Ruttiger (Gaffer)');
insert into examples (input, output) values ('Sumit gaurav Rishabh Singh Bill Harris Bill Gates Ben Zorn','gaurav Singh Harris Gates Zorn');
insert into examples (input, output) values ('Shuvendu Lahiri Wolfram Schulte Shaz Qadeer Nikhil Swamy Trishul Chilimbi Ben Lishvits','Lahiri Schulte Qadeer Swamy Chilimbi Lishvits');
insert into examples (input, output) values ('Foo Sumit gaurav Rishabh Singh Bill Harris Bill Gates Ben Zorn','Sumit Rishabh Bill Bill Ben');
insert into examples (input, output) values ('Bar Shuvendu Lahiri Wolfram Schulte Shaz Qadeer Nikhil Swamy Trishul Chilimbi Ben Lishvits','Shuvendu Wolfram Shaz Nikhil Trishul Ben');
insert into examples (input, output) values ('foo','foo');
insert into examples (input, output) values ('','NULL');
insert into examples (input, output) values ('','NULL');
insert into examples (input, output) values ('bar','bar');
insert into examples (input, output) values ('blah','blah');
insert into examples (input, output) values ('Sumit gaurav Rishabh Singh Bill Harris Bill Gates Ben Zorn','Sumit Rishabh Bill Bill Ben');
insert into examples (input, output) values ('Shuvendu Lahiri Wolfram Schulte Shaz Qadeer Nikhil Swamy Trishul Chilimbi Ben Lishvits','Shuvendu Wolfram Shaz Nikhil Trishul Ben');
insert into examples (input, output) values ('Gordon Ramsey Ian Dury','Gordon Ian');
insert into examples (input, output) values ('Sachin Ramesh Tendulkar, James Earl Ray, William Carlos Williams','Sachin Tendulkar, James Ray, William Williams');
insert into examples (input, output) values ('Reese Shear Smith, Sir Donald Bradman','Reese Smith, Sir Bradman');
insert into examples (input, output) values ('Aditya Krishna malhotra, Anthony Graham Smith','Aditya malhotra, Anthony Smith');
insert into examples (input, output) values ('a bu c','bu+');
insert into examples (input, output) values ('d eza f','eza+');
insert into examples (input, output) values ('g hosa2 i','hosa2+');
insert into examples (input, output) values ('Every middle field;','middle+');
insert into examples (input, output) values ('that is how','is+');
insert into examples (input, output) values ('we perfect ourselves','perfect+');
insert into examples (input, output) values ('a b c','b+');
insert into examples (input, output) values ('d e f','e+');
insert into examples (input, output) values ('g h i z','h+');
insert into examples (input, output) values ('Every middle field;','middle+');
insert into examples (input, output) values ('that is how','is+');
insert into examples (input, output) values ('we perfect ourselves doubly','perfect+');
insert into examples (input, output) values ('Thursday June 24th 2010','24 June 2010');
insert into examples (input, output) values ('Wednesday June 23rd 2010','23 June 2010');
insert into examples (input, output) values ('355 Algeria','case 355: return Algeria;');
insert into examples (input, output) values ('213 Lithuania','case 213: return Lithuania;');
insert into examples (input, output) values ('612 Australia','case 612: return Australia;');
insert into examples (input, output) values ('1988 David Haussler, Leonard Pitt','8 David Haussler, Leonard Pitt');
insert into examples (input, output) values ('1989 Ron Rivest','9 Ron Rivest');
insert into examples (input, output) values ('1990 John Case','0 John Case');
insert into examples (input, output) values ('2004 Foo Bar','4 Foo Bar');
insert into examples (input, output) values ('1008 Rocco Servedio, Tong Zhang','8 Rocco Servedio, Tong Zhang');
insert into examples (input, output) values ('2009 Adam Klivans, Sanjoy Dasgupta','9 Adam Klivans, Sanjoy Dasgupta');
insert into examples (input, output) values ('2010 Mehryar Mohry, Adam kopeland','0 Mehryar Mohry, Adam kopeland');
insert into examples (input, output) values ('2011 Ulrike von Luxburg, Sham Kakade','1 Ulrike von Luxburg, Sham Kakade');
insert into examples (input, output) values ('Alabama
Ala.
AL
Montgomery','Alabama Ala. AL Montgomery');
insert into examples (input, output) values ('Alaska
Alaska
AK
Juneau','Alaska Alaska AK Juneau');
insert into examples (input, output) values ('Arizona
Ariz.
AZ
Phoenix','Arizona Ariz. AZ Phoenix');
insert into examples (input, output) values ('Arkansas
Ark.
AR
Little Rock','Arkansas Ark. AR Little Rock');
insert into examples (input, output) values ('California
Calif.
CA
Sacramento','California Calif. CA Sacramento');
insert into examples (input, output) values ('Colorado
Colo.
CO
Denver','Colorado Colo. CO Denver');
insert into examples (input, output) values ('Connecticut
Conn.
CT
Hartford','Connecticut Conn. CT Hartford');
insert into examples (input, output) values ('Delaware
Del.
DE
Dover','Delaware Del. DE Dover');
insert into examples (input, output) values ('Florida
Fla.
FL
Tallahassee','Florida Fla. FL Tallahassee');
insert into examples (input, output) values ('Georgia
Ga.
GA
Atlanta','Georgia Ga. GA Atlanta');
insert into examples (input, output) values ('Hawaii
Hawaii
HI
Honolulu','Hawaii Hawaii HI Honolulu');
insert into examples (input, output) values ('Idaho
Idaho
ID
Boise','Idaho Idaho ID Boise');
insert into examples (input, output) values ('Male Female Female Male','quiji ouiji ouiji quiji');
insert into examples (input, output) values ('Male','quiji');
insert into examples (input, output) values ('Female','ouiji');
insert into examples (input, output) values ('07/07/2010	AT THE KNEES','07/07/2010	AT THE KNEES');
insert into examples (input, output) values ('07/18/2011	FARLEYSHELONWHEEL
07/18/2011	PETERBOROUGH','07/18/2011	FARLEYSHELONWHEEL	07/18/2011	PETERBOROUGH');
insert into examples (input, output) values ('07/09/2011	GOLDISPRETTY','07/09/2011	GOLDISPRETTY');
insert into examples (input, output) values ('07/07/2011	AT THE KNEES','07/07/2011	AT THE KNEES');
insert into examples (input, output) values ('07/18/2011	FARLEYSHELONWHEEL
07/18/2011	FARLEYSHELONWHEEL','07/18/2011	FARLEYSHELONWHEEL	07/18/2011	FARLEYSHELONWHEEL');
insert into examples (input, output) values ('07/09/2011	GOLDISPRETTY
07/09/2011	J B THREE
07/09/2011	KWAME','07/09/2011	GOLDISPRETTY	07/09/2011	J B THREE	07/09/2011	KWAME');
insert into examples (input, output) values ('Africa:
Sudan
Algeria','Africa:
	Sudan
	Algeria');
insert into examples (input, output) values ('Asia:
CIS','Asia:
	CIS');
insert into examples (input, output) values ('Asia:
CIS
China
India','Asia:
	CIS
	China
	India');
insert into examples (input, output) values ('Australia:
Australia','Australia:
	Australia');
insert into examples (input, output) values ('North America:
Canada
USA','North America:
	Canada
	USA');
insert into examples (input, output) values ('South America:
Brazil
Argentina','South America:
	Brazil
	Argentina');
insert into examples (input, output) values ('Africa
Sudan	0
Algeria	1','Africa:
Sudan	0
Algeria	1');
insert into examples (input, output) values ('Ocean
CIS	2','Ocean:
CIS	2');
insert into examples (input, output) values ('Africa
Sudan	0
Algeria	2','Africa:
Sudan	0
Algeria	2');
insert into examples (input, output) values ('Asia
CIS	3
China	4
India	5','Asia:
CIS	3
China	4
India	5');
insert into examples (input, output) values ('Australia
Australia	6','Australia:
Australia	6');
insert into examples (input, output) values ('North America
Canada	7
USA	8','North America:
Canada	7
USA	8');
insert into examples (input, output) values ('South America
Brazil	9
Argentina	10','South America:
Brazil	9
Argentina	10');
insert into examples (input, output) values ('07/07/2011	AT THE KNEES','07/07/2011	AT THE KNEES');
insert into examples (input, output) values ('07/18/2011	FARLEYSHELONWHEEL	07/18/2011	PETERBOROUGH','07/18/2011	FARLEYSHELONWHEEL	PETERBOROUGH');
insert into examples (input, output) values ('07/09/2011	GOLDISPRETTY','07/09/2011	GOLDISPRETTY');
insert into examples (input, output) values ('07/09/2011	GOLDISPRETTY	07/09/2011	J B THREE	07/09/2011	KWAME','07/09/2011	GOLDISPRETTY	J B THREE	KWAME');
insert into examples (input, output) values ('07/07/2011	AT THE KNEES','07/07/2011	AT THE KNEES');
insert into examples (input, output) values ('07/18/2011	FARLEYSHELONWHEEL	07/18/2011	FARLEYSHELONWHEEL','07/18/2011	FARLEYSHELONWHEEL	FARLEYSHELONWHEEL');
insert into examples (input, output) values ('07/09/2011	GOLDISPRETTY','07/09/2011	GOLDISPRETTY');
insert into examples (input, output) values ('07/09/2011	GOLDISPRETTY	07/09/2011	J B THREE	07/09/2011	KWAME','07/09/2011	GOLDISPRETTY	J B THREE	KWAME');
insert into examples (input, output) values ('"IBM" 176.63 1.08','"IBM"       176.63  1.08');
insert into examples (input, output) values ('"MSFT" 26.82 0.49','"MSFT"      26.82   0.49');
insert into examples (input, output) values ('"HOLL" 12.18 5.64','"HOLL"      12.18   5.64');
insert into examples (input, output) values ('af1988 David Haussler, Leonard Pitt','1988 David Haussler, Leonard Pitt');
insert into examples (input, output) values ('321989 Ron Rivest','1989 Ron Rivest');
insert into examples (input, output) values ('121990 John Case','1990 John Case');
insert into examples (input, output) values ('rg1988 David Haussler, Leonard Pitt','1988 David Haussler, Leonard Pitt');
insert into examples (input, output) values ('921989 Ron Rivest','1989 Ron Rivest');
insert into examples (input, output) values ('121990 John Case','1990 John Case');
insert into examples (input, output) values ('af1988 David Haussler, Leonard Pitt','1988 David Haussler, Leonard Pitt');
insert into examples (input, output) values ('321989 Ron Rivest','1989 Ron Rivest');
insert into examples (input, output) values ('121990 John Case','1990 John Case');
insert into examples (input, output) values ('ab1990 John Case','1990 John Case');
insert into examples (input, output) values ('cd1988 David Haussler, Leonard Pitt','1988 David Haussler, Leonard Pitt');
insert into examples (input, output) values ('991989 Ron Rivest','1989 Ron Rivest');
insert into examples (input, output) values ('831990 John ','1990 John ');
insert into examples (input, output) values ('88 David Haussler, Leonard Pitt','88 David Haussler
88 Leonard Pitt');
insert into examples (input, output) values ('89 Ron Rivest','89 Ron Rivest');
insert into examples (input, output) values ('90 John Case, Steve','90 John Case
90 Steve');
insert into examples (input, output) values ('94 Manfred Warmuth','94 Manfred Warmuth');
insert into examples (input, output) values ('95 Wolfgang Maass','95 Wolfgang Maass');
insert into examples (input, output) values ('96 Avrim Blum, Michael Kearns','96 Avrim Blum
96 Michael Kearns');
insert into examples (input, output) values ('97 Yoav Freund, Robert Schapire','97 Yoav Freund
97 Robert Schapire');
insert into examples (input, output) values ('88 David Haussler, Leonard Pitt','88 David Haussler');
insert into examples (input, output) values ('89 Ron Rivest','89 Ron Rivest');
insert into examples (input, output) values ('90 John Case, Steve','90 John Case');
insert into examples (input, output) values ('94 Manfred Warmuth','94 Manfred Warmuth');
insert into examples (input, output) values ('95 Wolfgang Maass','95 Wolfgang Maass');
insert into examples (input, output) values ('96 Avrim Blum, Michael Kearns','96 Avrim Blum');
insert into examples (input, output) values ('97 Yoav Freund, Robert Schapire','97 Yoav Freund');
insert into examples (input, output) values ('123456','123-456');
insert into examples (input, output) values ('145678','145-678');
insert into examples (input, output) values ('251245','251-245');
insert into examples (input, output) values ('9500
Gilman
Drive
La
Jolla
CA','9500 Gilman Drive La Jolla CA');
insert into examples (input, output) values ('1
Miramar
Street
Apartment 4120
La Jolla
CA','1 Miramar Street Apartment 4120 La Jolla CA');
insert into examples (input, output) values ('9498
Gilman
Drive
Unit
#504
La
Jolla
CA','9498 Gilman Drive Unit #504 La Jolla CA');
insert into examples (input, output) values ('$0.6456 \pm 0.01$
$0.4295 \pm 0.06$
$0.2543 \pm 0.05$','$0.6456 \pm 0.01$ & $0.4295 \pm 0.06$ & $0.2543 \pm 0.05$');
insert into examples (input, output) values ('$0.5215 \pm 0.04$
$0.1232 \pm 0.053$','$0.5215 \pm 0.04$ & $0.1232 \pm 0.053$');
insert into examples (input, output) values ('MALE-34.jpg	205','mv MALE-34.jpg a205.jpg');
insert into examples (input, output) values ('Male-17.JPG	206','mv Male-17.JPG a206.jpg');
insert into examples (input, output) values ('Male-2.JPG	207','mv Male-2.JPG a207.jpg');
insert into examples (input, output) values ('blah.jpg	210','mv blah.jpg a210.jpg');
insert into examples (input, output) values ('foobar.jpg	204','mv foobar.jpg a204.jpg');
insert into examples (input, output) values ('205	MALE-34.jpg','mv MALE-34.jpg a205.jpg');
insert into examples (input, output) values ('206	Male-17.JPG','mv Male-17.JPG a206.jpg');
insert into examples (input, output) values ('207	Male-2.JPG','mv Male-2.JPG a207.jpg');
insert into examples (input, output) values ('210	blah.jpg','mv blah.jpg a210.jpg');
insert into examples (input, output) values ('204	foo.jpg','mv foo.jpg a204.jpg');
insert into examples (input, output) values ('1. Head north on Tonnele Ave toward Pavonia Ave
0.5 mi','1. Head north on Tonnele Ave toward Pavonia Ave
[0.5 mi]');
insert into examples (input, output) values ('2. Slight right to merge onto NJ-139 E toward Holland Tunnel
1.1 mi','2. Slight right to merge onto NJ-139 E toward Holland Tunnel
[1.1 mi]');
insert into examples (input, output) values ('3. Merge onto I-78 E
Partial toll road
Entering New York
2.2 mi','3. Merge onto I-78 E
Partial toll road
Entering New York
[2.2 mi]');
insert into examples (input, output) values ('4. Continue onto Lincoln Hwy
Toll road
0.2 mi','4. Continue onto Lincoln Hwy
Toll road
[0.2 mi]');
insert into examples (input, output) values ('embellish','(embellish)');
insert into examples (input, output) values ('me','(me)');
insert into examples (input, output) values ('{this}','({this})');
insert into examples (input, output) values ('{works}','({works})');
insert into examples (input, output) values ('i don''t know or can''t xetermine if this isn''t a thing others wouldn''t do','i don''t know or can t xetermine if this isn''t a thing others wouldn t do');
insert into examples (input, output) values ('a''one''a''two''a''three','a''one a''two a''three');
insert into examples (input, output) values ('this american life','this a life');
insert into examples (input, output) values ('movies on demand','movies o demand');
insert into examples (input, output) values ('sink or swim','sink o swim');
insert into examples (input, output) values ('lost in space','lost i space');
insert into examples (input, output) values ('this american life','t american life');
insert into examples (input, output) values ('movies on demand','m on demand');
insert into examples (input, output) values ('sink or swim','s or swim');
insert into examples (input, output) values ('lost in space','l in space');
insert into examples (input, output) values ('Thursday 24th June 2010','24 June 2010');
insert into examples (input, output) values ('Wednesday 23rd June 2010','23 June 2010');
insert into examples (input, output) values ('acs3 b cdd','b+');
insert into examples (input, output) values ('d e f','e+');
insert into examples (input, output) values ('g ah i','ah+');
insert into examples (input, output) values ('Every middle field;','middle+');
insert into examples (input, output) values ('that is how','is+');
insert into examples (input, output) values ('we perfect ourselves','perfect+');
insert into examples (input, output) values ('Aditya Krishna malhotra','A K malhotra');
insert into examples (input, output) values ('Ricky Thomas Ponting','R T Ponting');
insert into examples (input, output) values ('Parampal Singh Pooni','P S Pooni');
insert into examples (input, output) values ('Sachin Ramesh Tendulkar','S R Tendulkar');
insert into examples (input, output) values ('24th June','June 24');
insert into examples (input, output) values ('22nd June','June 22');
insert into examples (input, output) values ('23rd June','June 23');
insert into examples (input, output) values ('June the 24th','June 24');
insert into examples (input, output) values ('April the 22nd','April 22');
insert into examples (input, output) values ('June the 23rd','June 23');
insert into examples (input, output) values ('Madhur Malik','Madhur Mali.k.');
insert into examples (input, output) values ('Sumit gaurav','Sumit Gulwan.i.');
insert into examples (input, output) values ('First Last','First Las.t.');
insert into examples (input, output) values ('Darren Gehring','Darren Gehrin.g.');
insert into examples (input, output) values ('Rohit Singh','Rohit Sing.h.');
insert into examples (input, output) values ('Thursday June 24th 2010','Thursday, June 24th 2010');
insert into examples (input, output) values ('Friday April 5th 1985','Friday, April 5th 1985');
insert into examples (input, output) values ('Wednesday June 23rd 2010','Wednesday, June 23rd 2010');
insert into examples (input, output) values ('D. Blei (1)','D. Blei');
insert into examples (input, output) values ('M. Jordan (2)','M. Jordan');
insert into examples (input, output) values ('D. Klein (1)','D. Klein');
insert into examples (input, output) values ('P. Liang (1)','P. Liang');
insert into examples (input, output) values ('D. Klein (3)','D. Klein');
insert into examples (input, output) values ('A. Roberts (1)','A. Roberts');
